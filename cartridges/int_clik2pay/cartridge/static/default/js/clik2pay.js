'use strict';

/* global jQuery */

(function () {
    var clik2payPaymentButton;
    var hintMessageElement;
    var errorMessageElement;
    var errorMessageContainer;

    /**
     * Display the given error message
     *
     * @param {string?} [errorMessage] Error message
     */
    function displayError(errorMessage) {
        if (!errorMessage) {
            // eslint-disable-next-line no-param-reassign
            errorMessage = errorMessageElement.dataset.genericErrorMessage;
        }
        errorMessageElement.textContent = errorMessage;
        errorMessageContainer.classList.remove('visually-hidden');
        jQuery(errorMessageContainer).show();
    }

    /**
     * Redirect to the given URL using the POST method
     *
     * @param {string} redirectUrl Redirect URL
     * @param {string} orderNo Order number
     * @param {string} orderToken Order token
     */
    function postRedirect(redirectUrl, orderNo, orderToken) {
        var redirect = jQuery('<form>')
            .appendTo(document.body)
            .attr({
                method: 'POST',
                action: redirectUrl
            });

        jQuery('<input>')
            .appendTo(redirect)
            .attr({
                name: 'orderID',
                value: orderNo
            });

        jQuery('<input>')
            .appendTo(redirect)
            .attr({
                name: 'orderToken',
                value: orderToken
            });

        redirect.submit();
    }

    /**
     * Check if payment was successful and try to place the order.
     * Keep polling the endpoint until either timeout or error.
     */
    function tryContinuePlaceOrder() {
        jQuery.ajax({
            url: clik2payPaymentButton.dataset.continueUrl,
            type: 'GET',
            success: function (data) {
                if (!data.isSuccess) {
                    displayError(data.customerErrorMessage);
                    return;
                }
                if (data.keepPolling) {
                    setTimeout(tryContinuePlaceOrder, data.pollingInterval);
                    return;
                }
                if (data.redirectUrl) {
                    if (data.redirectMethod === 'POST') {
                        postRedirect(data.redirectUrl, data.orderNo, data.orderToken);
                    } else {
                        window.location = data.redirectUrl;
                    }
                    return;
                }
                displayError();
            },
            error: function () {
                displayError();
            }
        });
    }

    /**
     * Handle the payment button click
     *
     * @param {Object} event Event
     */
    function clik2PayButtonClick(event) {
        event.preventDefault();

        var paymentPageUrl = clik2payPaymentButton.dataset.paymentPageUrl;
        var windowName = 'Clik2pay via interac payment page';
        window.open(paymentPageUrl, windowName, 'location=0,height=800,width=600').focus();

        hintMessageElement.classList.remove('visually-hidden');

        tryContinuePlaceOrder();
    }

    /**
     * Initialize DOM element variables and event handler
     *
     * Note that jQuery is not yet loaded here.
     */
    function initialize() {
        hintMessageElement = document.querySelector('.js-click2pay-manual-hint');

        errorMessageElement = document.querySelector('.js-click2pay-error-message-text');
        errorMessageContainer = document.querySelector('.js-click2pay-error-message');

        clik2payPaymentButton = document.querySelector('.js-clik2pay-payment-button');
        clik2payPaymentButton.addEventListener('click', clik2PayButtonClick);
    }

    initialize();
}());
