'use strict';

/*
 * The Interac service does not support a redirect from the external payment page back to the shop.
 *
 * Instead, after submitting the order, processor.authorize() fetches the URL to the external payment page and
 * then redirects to Clik2pay-ShowPaymentPage. This renders the Clik2pay button.
 *
 * Clicking the button opens the external payment page and starts polling the Clik2pay-TryContinuePlaceOrder endpoint.
 * Once the customer completed payment in the popup window, Clik2pay sends the Payment Request Status to
 * Clik2pay-Notify which updates the SFCC payment transaction.
 *
 * Once Clik2pay-TryContinuePlaceOrder detects that payment has either succeeded or failed it either
 * places the order and shows the confirmation page or fails the order and displays an error message.
 *
 *
 * IMPORTANT: This cartridge does currently not support split payment between this method and a e.g. a gift certificate.
 *            That's the reason why we just remove all payment instruments and add the Clik2pay method.
 *            However there should be no problem in customizing it to do so, because at the moment Clik2pay does not
 *            require you to send additional order details, but only the amount to pay.
 *            Please check the Clik2pay API and reach out to Clik2pay to verify.
 */

/**
 * Remove all current payment instruments from the basket and
 * then create a Clik2pay payment instrument.
 *
 * @param {dw.order.Basket} basket Basket
 * @param {string} paymentMethodID Payment method ID
 * @returns {Object} Success or error flag
 */
function handle(basket, paymentMethodID) {
    var Logger = require('dw/system/Logger');
    var Transaction = require('dw/system/Transaction');
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');
    var log = Logger.getLogger('clik2pay.processorHelpers.handle');

    if (paymentMethodID !== constants.PaymentMethodClik2payInterac) {
        log.error('Unhandled payment method: ' + paymentMethodID);
        return { error: true };
    }

    if (!basket) {
        log.error('Basket parameter is null or undefined');
        return { error: true };
    }

    Transaction.wrap(function () {
        basket.removeAllPaymentInstruments();
        basket.createPaymentInstrument(constants.PaymentMethodClik2payInterac, basket.totalGrossPrice);
    });

    return { success: true };
}

/**
 * Imports type definition(S) which are used in multiple files
 * @typedef {import('*\/cartridge/scripts/helpers/clik2pay/types')} processorAuthorizationResult
 */

/**
 * Perform a service call to Clik2pay which creates a new Payment Request.
 * Return the URL to the external payment page and update the order and payment transaction.
 *
 * @param {dw.order.Order} order Order
 * @param {dw.order.OrderPaymentInstrument} paymentInstrument The Clik2pay payment instrument
 * @returns {processorAuthorizationResult} Error flag and redirect information
 */
function authorize(order, paymentInstrument) {
    var Logger = require('dw/system/Logger');
    var Money = require('dw/value/Money');
    var Order = require('dw/order/Order');
    var Transaction = require('dw/system/Transaction');
    var StringUtils = require('dw/util/StringUtils');
    var URLUtils = require('dw/web/URLUtils');
    var ProblemReporter = require('*/cartridge/scripts/helpers/clik2pay/problemReporter');

    var log = Logger.getLogger('clik2pay.processorHelpers.authorize');
    var problemReporter;
    var orderNotePrefix = 'Clik2pay: Authorize ';

    var trackOrderChange = function (orderNote) {
        Transaction.wrap(function () {
            order.trackOrderChange(orderNote);
        });
    };

    try {
        // check pre-conditions to authorize
        if (!order) {
            // no order so we can't have the orderNo as part of the report format
            problemReporter = new ProblemReporter('clik2pay payment processor', log, '{0}-Authorize encountered problems for unknown order: {1}');
            problemReporter.addErrorMessage('no order to authorize');
        } else {
            // order exists. So let's have the orderNo within each error report to make it more helpful
            problemReporter = new ProblemReporter('clik2pay payment processor', log, '{0}-Authorize encountered problems authorizing ' + order.orderNo + ': {1}');

            if (order.status.value !== Order.ORDER_STATUS_CREATED) {
                var wrongStatusMessage = StringUtils.format('order {0} has to be in status created to be authorized, but it is in status {1}', order.orderNo, order.status.displayValue);
                problemReporter.addErrorMessage(wrongStatusMessage);
            }
        }
        if (!paymentInstrument) {
            problemReporter.addErrorMessage('no payment instrument passed - can not create a payment request without a payment instrument to base it on');
        }
        if (problemReporter.hasErrors()) {
            // We got a problem, so we'll report it
            problemReporter.logStatus();
            if (order) {
                trackOrderChange(orderNotePrefix + 'failed.');
            }
            return {
                error: true,
                problemsToReport: problemReporter.getStatusReport()
            };
        }

        // everything we need seems to be there. So call clik2pay for a payment request link
        var serviceAPI = require('*/cartridge/scripts/payment/clik2pay/serviceAPI');
        var createPaymentRequestResult = serviceAPI.createPaymentRequest(order, paymentInstrument);

        if (!createPaymentRequestResult.success) {
            // a problem occurred. The service Result is already correctly formatted, problems have already been logged. So just log a reminder and return the result
            problemReporter.addErrorMessage(createPaymentRequestResult.problemsToReport);
            problemReporter.logStatus();
            trackOrderChange(orderNotePrefix + 'failed. Error during create Payment Request service call. ' + (createPaymentRequestResult.problemsToReport || ''));
            return {
                error: !createPaymentRequestResult.success,
                problemsToReport: createPaymentRequestResult.problemsToReport
            };
        }

        var paymentTransaction = paymentInstrument.getPaymentTransaction();
        if (!paymentTransaction) {
            problemReporter.addErrorMessage('no payment transaction could be found on the clik2pay payment instrument');
            problemReporter.logStatus();
            trackOrderChange(orderNotePrefix + 'failed. Payment transaction is empty.');
            return {
                error: true,
                problemsToReport: problemReporter.getStatusReport()
            };
        }

        var confirmedAmount = new Money(createPaymentRequestResult.confirmedAmount, order.totalGrossPrice.currencyCode);
        var requestedAmount = paymentTransaction.getAmount();
        var statusMessage = ' Payment Request status is ' + createPaymentRequestResult.status;

        if (!confirmedAmount.equals(requestedAmount)) {
            problemReporter.addErrorMessage('The confirmed amount of {0} does not equal the requested amount of {1}, please use the resource link in the notes section of the order and/or contact clik2pay for details why');
            problemReporter.logStatus();

            Transaction.wrap(function () {
                paymentTransaction.setTransactionID(createPaymentRequestResult.merchantTransactionId);
            });

            trackOrderChange(orderNotePrefix + 'failed.'
                + ' Confirmed amount (' + confirmedAmount.toFormattedString()
                + ') does not equal requested amount (' + requestedAmount.toFormattedString() + '.'
                + statusMessage);
            return {
                error: true,
                problemsToReport: problemReporter.getStatusReport()
            };
        }

        Transaction.wrap(function () {
            paymentTransaction.setTransactionID(createPaymentRequestResult.merchantTransactionId);
            paymentTransaction.custom.c2pPaymentRequestID = createPaymentRequestResult.id;
            paymentTransaction.custom.c2pPaymentRequestPaymentLink = createPaymentRequestResult.paymentRequestLink;
            paymentTransaction.custom.c2pPaymentRequestQrCodeLink = createPaymentRequestResult.qrCodeLink;
            paymentTransaction.custom.c2pPaymentRequestShortCode = createPaymentRequestResult.shortCode;
        });
        trackOrderChange(orderNotePrefix + 'successful. Confirmed amount is '
            + confirmedAmount.toFormattedString() + '.' + statusMessage);

        return {
            error: false,
            redirectRequired: true,
            redirectUrl: URLUtils.url('Clik2pay-ShowPaymentPage', 'orderNo', order.orderNo, 'orderToken', order.orderToken).toString()
        };
    } catch (exception) {
        log.error('Order {0}: Exception occurred. {1}', JSON.stringify(exception, null, 2), order.orderNo);
        trackOrderChange(orderNotePrefix + 'failed. Exception occurred.');
        return {
            error: true,
            problemsToReport: exception.message
        };
    }
}

module.exports = {
    handle: handle,
    authorize: authorize
};
