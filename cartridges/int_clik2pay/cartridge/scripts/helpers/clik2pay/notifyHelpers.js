'use strict';

/**
 * Helper functions to handle incoming payment notifications
 */


/**
 * Compare a request's credentials to the credentials stored in site preference.
 *
 * @param {string?} authorizationHeader Value of the HTTP Authorization header
 * @returns {Object} Success flag and error information
 */
function authenticateRequest(authorizationHeader) {
    var Logger = require('dw/system/Logger');
    var Site = require('dw/system/Site');
    var StringUtils = require('dw/util/StringUtils');
    var System = require('dw/system/System');
    var credentialHelpers = require('*/cartridge/scripts/helpers/clik2pay/credentialHelpers');

    var log = Logger.getLogger('clik2pay.notifyHelpers.authenticateRequest');
    var errorResult = {
        isSuccess: false,
        errorStatusCode: 401
    };
    var successResult = {
        isSuccess: true
    };

    // parse request's credentials
    if (!authorizationHeader || typeof authorizationHeader !== 'string' || authorizationHeader.indexOf('Basic ') !== 0) {
        log.error('invalid Authorization header');
        errorResult.errorMessage = 'invalid Authorization header: ' + authorizationHeader;
        return errorResult;
    }

    // get stored credentials
    var storedCredentials = credentialHelpers.getNotifyEndpointCredentialString(Site.current.ID);
    if (!storedCredentials) {
        errorResult.errorMessage = 'credentials are not configured correctly';
        return errorResult;
    }

    var encodedCredentials = authorizationHeader.substring(6);
    var decodedCredentials = StringUtils.decodeBase64(encodedCredentials);

    // compare
    if (decodedCredentials !== storedCredentials) {
        errorResult.errorMessage = 'credentials don\'t match';
        if (System.instanceType !== System.PRODUCTION_SYSTEM) {
            errorResult.errorMessage += ': decodedCredentials (' + decodedCredentials + ') != storedCredentials (' + storedCredentials + ')';
        }
        log.error(errorResult.errorMessage);

        return errorResult;
    }

    return successResult;
}

/**
 * Parse and validate a notification request.
 *
 * @param {string?} requestBody Body of the notification request
 * @returns {Object} Success flag, error information, and parsed request body
 */
function parseNotificationRequest(requestBody) {
    var Logger = require('dw/system/Logger');
    var log = Logger.getLogger('clik2pay.notifyHelpers.parseNotificationRequest');

    var errorResult = {
        isSuccess: false,
        errorStatusCode: 400
    };
    var successResult = {
        isSuccess: true
    };

    var requestData;
    try {
        requestData = JSON.parse(requestBody);

        if (!requestData) {
            log.error('empty or invalid request body');
            errorResult.errorMessage = 'empty or invalid request body';
            return errorResult;
        }
        successResult.requestData = requestData;

        var orderNo = requestData.resource.invoiceNumber;
        var orderToken = requestData.resource.businessUnit;
        if (!orderNo || !orderToken) {
            log.error('empty order number or token. orderNo = {0}, orderToken = {1}', orderNo, orderToken);
            errorResult.errorMessage = 'empty order number or token';
            return errorResult;
        }
        successResult.orderNo = orderNo;
        successResult.orderToken = orderToken;

        return successResult;
    } catch (exception) {
        log.error(JSON.stringify(exception, null, 2));
        errorResult.errorMessage = 'error while parsing the request';
        return errorResult;
    }
}

/**
 * Update the payment transaction's payment request status.
 *
 * @param {dw.order.Order} order Order
 * @param {dw.order.OrderPaymentInstrument} paymentTransaction Payment transaction
 * @param {string} newTransactionStatus New payment transaction status
 * @returns {Object} Success flag, error information
 */
function updatePaymentTransactionStatus(order, paymentTransaction, newTransactionStatus) {
    var Logger = require('dw/system/Logger');
    var Order = require('dw/order/Order');
    var Transaction = require('dw/system/Transaction');
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');

    var log = Logger.getLogger('clik2pay.notifyHelpers.updatePaymentTransactionStatus');
    var logMessagePrefix = 'Order ' + order.orderNo + ': ';
    var orderNote = 'Clik2pay: received notification with Payment Request status ' + newTransactionStatus + '.';

    return Transaction.wrap(function () {
        paymentTransaction.custom.c2pPaymentRequestStatus = newTransactionStatus;
        orderNote += ' Payment transaction status updated.';

        var orderStatus = order.status.value;
        if (orderStatus !== Order.ORDER_STATUS_CREATED) {
            if (newTransactionStatus === constants.PaymentRequestStatus.PAID) {
                // allow customer service to find failed orders for which a successful payment notification was received
                order.custom.c2pPaymentSuccessAfterTimeout = true;
            }
            orderNote += ' Error: Expected CREATED but found ' + order.status.displayValue + '.';
            order.trackOrderChange(orderNote);
            log.error(logMessagePrefix + orderNote);
            return {
                isSuccess: false,
                errorMessage: orderNote
            };
        }

        order.trackOrderChange(orderNote);
        return {
            isSuccess: true
        };
    });
}

/**
 * Check if the given string is a valid Payment Request Status enum value.
 *
 * @param {string} status Payment request status
 * @returns {boolean} True if valid
 */
function validatePaymentRequestStatus(status) {
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');

    // get all enum values
    var values = Object.keys(constants.PaymentRequestStatus).map(function (key) {
        return constants.PaymentRequestStatus[key];
    });

    return values.indexOf(status) > -1;
}

/**
 * Updates order status according to the notification contents.
 *
 * @param {dw.order.Order} order Order
 * @param {Object} notification Parsed notification request body
 * @returns {Object} Success flag, error information
 */
function updateOrder(order, notification) {
    var Logger = require('dw/system/Logger');
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');
    var paymentInstrumentHelpers = require('*/cartridge/scripts/helpers/clik2pay/paymentInstrumentHelpers');

    var log = Logger.getLogger('clik2pay.notifyHelpers.updateOrder');
    var logMessagePrefix = 'Order ' + order.orderNo + ': ';
    var errorResult = {
        isSuccess: false
    };
    var successResult = {
        isSuccess: true
    };

    var paymentInstrument = paymentInstrumentHelpers.findClik2payPaymentInstrument(order);
    if (!paymentInstrument) {
        errorResult.errorMessage = 'Clik2pay payment instrument not found';
        log.error(logMessagePrefix + errorResult.errorMessage);
        return errorResult;
    }

    var paymentTransaction = paymentInstrument.paymentTransaction;
    if (!paymentTransaction) {
        errorResult.errorMessage = 'Payment transaction is null';
        log.error(logMessagePrefix + errorResult.errorMessage);
        return errorResult;
    }

    var newPaymentRequestStatus = notification.resource.status;
    var isValidPaymentRequestStatus = module.exports.validatePaymentRequestStatus(newPaymentRequestStatus);
    if (!isValidPaymentRequestStatus) {
        errorResult.errorMessage = 'Payment request status is invalid or unhandled: ' + newPaymentRequestStatus;
        log.error(logMessagePrefix + errorResult.errorMessage);
        return errorResult;
    }

    var currentPaymentRequestStatus = paymentTransaction.custom.c2pPaymentRequestStatus;
    var isStatusUpdateAllowed = newPaymentRequestStatus !== currentPaymentRequestStatus
        && currentPaymentRequestStatus !== constants.PaymentRequestStatus.PAID
        && currentPaymentRequestStatus !== constants.PaymentRequestStatus.FAILED;
    if (!isStatusUpdateAllowed) {
        // once the SFCC payment transaction is either PAID or FAILED
        // then the transaction must not be changed anymore
        log.debug(logMessagePrefix + 'Ignored notification with status ' + newPaymentRequestStatus);
        successResult.paymentTransactionStatus = currentPaymentRequestStatus;
        return successResult;
    }

    var updateTransactionResult = module.exports.updatePaymentTransactionStatus(order, paymentTransaction, newPaymentRequestStatus);
    if (updateTransactionResult.isSuccess) {
        successResult.paymentTransactionStatus = newPaymentRequestStatus;
        return successResult;
    }
    errorResult.errorMessage = updateTransactionResult.errorMessage;
    return errorResult;
}

/**
 * Process a notification for a given Payment Request.
 *
 * @param {string?} authorizationHeader Value of the HTTP Authorization header
 * @param {string} requestBody Request body
 * @param {boolean} [skipAuth=false] If true then request authentication is skipped (this parameter is ignored on Production)
 * @returns {Object} Success flag and error information
 */
function handleNotificationRequest(authorizationHeader, requestBody, skipAuth) {
    var Logger = require('dw/system/Logger');
    var OrderMgr = require('dw/order/OrderMgr');
    var System = require('dw/system/System');

    var log = Logger.getLogger('clik2pay.notifyHelpers.handleNotificationRequest');

    if (System.instanceType === System.PRODUCTION_SYSTEM || !skipAuth) {
        var authenticationResult = module.exports.authenticateRequest(authorizationHeader);
        if (!authenticationResult.isSuccess) {
            var errorMessage = System.instanceType === System.PRODUCTION_SYSTEM // do not reveal authentication error details on Production
                ? null : authenticationResult.errorMessage;
            return {
                isSuccess: false,
                errorMessage: errorMessage,
                errorStatusCode: 401
            };
        }
    }

    var parsingResult = module.exports.parseNotificationRequest(requestBody);
    if (!parsingResult.isSuccess) {
        return {
            isSuccess: false,
            errorMessage: parsingResult.errorMessage
        };
    }

    var order = OrderMgr.getOrder(parsingResult.orderNo, parsingResult.orderToken);
    if (!order) {
        log.error('Order {0}: order not found', parsingResult.orderNo);
        return {
            isSuccess: false,
            errorMessage: 'order not found'
        };
    }

    var updateOrderResult = module.exports.updateOrder(order, parsingResult.requestData);
    if (!updateOrderResult.isSuccess) {
        return {
            isSuccess: false,
            errorMessage: updateOrderResult.errorMessage
        };
    }

    return {
        isSuccess: true,
        paymentTransactionStatus: updateOrderResult.paymentTransactionStatus
    };
}

module.exports = {
    authenticateRequest: authenticateRequest,
    parseNotificationRequest: parseNotificationRequest,
    updatePaymentTransactionStatus: updatePaymentTransactionStatus,
    validatePaymentRequestStatus: validatePaymentRequestStatus,
    updateOrder: updateOrder,
    handleNotificationRequest: handleNotificationRequest
};
