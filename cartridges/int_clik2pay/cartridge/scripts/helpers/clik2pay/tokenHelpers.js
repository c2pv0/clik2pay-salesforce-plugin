'use strict';

/**
 * Helper functions to handle caching of authentication tokens.
 *
 * The custom cache contains a separate entry for each authentication service credential ID.
 */


/**
 * Get the custom cache used to store authentication tokens.
 *
 * @returns {dw.system.Cache?} Cache
 */
function getTokenCache() {
    var CacheMgr = require('dw/system/CacheMgr');
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');

    var log = require('dw/system/Logger').getLogger('clik2pay.tokenHelpers.getTokenCache');

    try {
        return CacheMgr.getCache(constants.AuthenticationTokenCacheID);
    } catch (exception) {
        var errorMessage = 'Error while accessing cache with ID ' + constants.AuthenticationTokenCacheID
            + ': ' + JSON.stringify(exception, null, 2);
        log.error(errorMessage);
        return null;
    }
}

/**
 * Stores a given authentication token in the custom authentication token cache.
 *
 * @param {dw.svc.HTTPFormService} service The service that made the authentication call
 * @param {string} token Authentication token
 * @param {number} milliSecondsBeforeExpiration the milliseconds until the token won't be valid anymore
 */
function storeToken(service, token, milliSecondsBeforeExpiration) {
    var Site = require('dw/system/Site');
    var credentialHelpers = require('*/cartridge/scripts/helpers/clik2pay/credentialHelpers');

    var tokenCache = module.exports.getTokenCache();
    if (!tokenCache) {
        return;
    }

    var now = new Date();
    // When the token was stored the system needed some time to cache it. This will be considerably lower than one second.
    // However just to be sure, we'll ignore the last second of validity to have some leeway.
    var expirationTime = now.getTime() + (milliSecondsBeforeExpiration - 1000);
    var validUntil = new Date(expirationTime).toISOString();

    var tokenEntry = {
        token: token,
        validUntil: validUntil
    };

    var cacheKey = credentialHelpers.getAuthenticationCredentialID(Site.current.ID);
    tokenCache.put(cacheKey, tokenEntry);
}

/**
 * Checks the custom authentication token cache for an existing token.
 *
 * @param {dw.svc.Service} service The instance of the authentication service which would make the call if the token is not retrievable
 * @returns {string?} Cached token
 */
function retrieveToken(service) {
    var Site = require('dw/system/Site');
    var credentialHelpers = require('*/cartridge/scripts/helpers/clik2pay/credentialHelpers');

    var tokenCache = module.exports.getTokenCache();
    if (!tokenCache) {
        return null;
    }

    var cacheKey = credentialHelpers.getAuthenticationCredentialID(Site.current.ID);
    var tokenEntry = tokenCache.get(cacheKey);
    if (!tokenEntry) {
        return null;
    }

    var now = new Date();
    var serviceTimeout = service.configuration.profile.timeoutMillis || 10000;
    var validUntil = new Date(tokenEntry.validUntil);

    // the token needs to be valid until the service call which uses the token has finished
    var leastValidityNeeded = new Date(now.getTime() + serviceTimeout);

    if (validUntil < leastValidityNeeded) {
        tokenCache.invalidate(cacheKey);
        return null;
    }

    return tokenEntry.token;
}

module.exports = {
    getTokenCache: getTokenCache,
    storeToken: storeToken,
    retrieveToken: retrieveToken
};
