'use strict';

/**
 * Enum of Payment Request Status values.
 * See https://docs.clik2pay.com/docs/payments
 *
 * @readonly
 * @enum {PaymentRequestStatus}
 */
var PaymentRequestStatusEnum = {
    CREATED: 'CREATED',
    SMS_SENT: 'SMS_SENT',
    EMAIL_SENT: 'EMAIL_SENT',
    ACTIVE: 'ACTIVE',
    PAID: 'PAID',
    SETTLED: 'SETTLED',
    FAILED: 'FAILED'
};

module.exports = {
    PaymentRequestStatus: PaymentRequestStatusEnum,
    PaymentMethodClik2payInterac: 'C2P_VIA_INTERAC',
    AuthenticationTokenCacheID: 'clik2pay.authentication.token', // must be the same as in caches.json
    ServiceIDs: {
        getAuthToken: 'int_clik2pay.httpForm.clik2pay.get_auth_token',
        createPaymentRequest: 'int_clik2pay.http.clik2pay.create_payment_request'
    },
    MinUserPasswordLength: 10,
    PaymentRequestTransactionType: 'ECOMM',
    PaymentRequestDeliveryMode: 'NONE'
};
