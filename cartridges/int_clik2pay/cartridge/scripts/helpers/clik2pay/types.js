'use strict';

/**
 * @typedef {object} clik2PayServiceResponse
 * @property {boolean} success all status codes that start with a 2 are deemed a success
 * @property {string} statusCode the status code of the response
 * @property {object} response the success or error JSON result from clik2pay
 */

/**
 * @typedef {Object} createPaymentRequestResult
 * @property {boolean} success true if the result is as expected
 * @property {string} paymentRequestLink the payment request link for usage in the clik2pay button
 * @property {string} [problemsToReport] if there were problems this report explains which there were
 * @property {Object} [parameters] if the problems occurred during parameters validation this helps with debugging
 * @property {Object} [result] if the problems occurred during the validation of the result it is added to help with debugging
 */

/**
 * @typedef processorAuthorizationResult
 * @property {boolean} success true if a payment request could be created and the order was updated accordingly
 * @property {string} [problemsToReport] (only present of success === false) a (hopefully helpful) report on encountered problems down the line
 */
