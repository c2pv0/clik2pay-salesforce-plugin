'use strict';

/**
 * Helper functions used by the endpoint Clik2pay-TryContinuePlaceOrder
 */

/**
 * Enum of Order Action values.
 * @readonly
 * @enum {OrderAction}
 */
var OrderAction = {
    ShowConfirmation: 'ShowConfirmation',
    PlaceOrder: 'PlaceOrder',
    WaitAndRetry: 'WaitAndRetry',
    FailOrder: 'FailOrder'
};

/**
 * Check if polling for payment request status has timed out.
 * Store the date and time of the first polling attempt in the payment transaction.
 *
 * @param {*} paymentTransaction Payment transaction
 * @returns {{isTimedOut: boolean, pollingInterval: number}} Timeout status and retry interval
 */
function checkPollingTimeout(paymentTransaction) {
    var Calendar = require('dw/util/Calendar');
    var Site = require('dw/system/Site');
    var Transaction = require('dw/system/Transaction');

    var pollingTimeout = Site.current.getCustomPreferenceValue('c2pPaymentStatusPollingTimeout') || 60 * 1000; // milliseconds until timeout
    var pollingInterval = Site.current.getCustomPreferenceValue('c2pPaymentStatusPollingInterval') || 1000; // milliseconds between attempts

    var pollingStart = paymentTransaction.custom.c2pPollingStartDateTime;
    if (!pollingStart) {
        // first time, not yet timed out
        Transaction.wrap(function () {
            paymentTransaction.custom.c2pPollingStartDateTime = new Date();
        });
        return {
            isTimedOut: false,
            pollingInterval: pollingInterval
        };
    }

    var pollingEndCalendar = new Calendar(pollingStart);
    pollingEndCalendar.add(Calendar.MILLISECOND, pollingTimeout);
    var pollingEnd = pollingEndCalendar.getTime();

    var now = new Date();
    if (now < pollingEnd) {
        return {
            isTimedOut: false,
            pollingInterval: pollingInterval
        };
    }

    return {
        isTimedOut: true,
        pollingInterval: -1
    };
}

/**
 * Determine whether the given order can be placed or must be failed,
 * or whether not enough information is available and polling should continue.
 *
 * @param {dw.order.Order} order Order
 * @param {dw.order.PaymentTransaction} paymentTransaction Payment transaction
 * @returns {Object} Success flag, error information
 */
function determineOrderAction(order, paymentTransaction) {
    var Order = require('dw/order/Order');
    var Resource = require('dw/web/Resource');
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');

    var result = {
        action: OrderAction.FailOrder
    };

    var orderStatus = order.status.value;
    var paymentRequestStatus = paymentTransaction.custom.c2pPaymentRequestStatus;

    if (orderStatus === Order.ORDER_STATUS_NEW || orderStatus === Order.ORDER_STATUS_OPEN) {
        result.action = OrderAction.ShowConfirmation;
        return result;
    }

    if (orderStatus !== Order.ORDER_STATUS_CREATED) {
        result.failOrderReason = 'Cannot place order. Expected order status CREATED, but found ' + order.status.displayValue;
        return result;
    }

    if (paymentRequestStatus === constants.PaymentRequestStatus.PAID) {
        result.action = OrderAction.PlaceOrder;
        return result;
    }

    if (paymentRequestStatus === constants.PaymentRequestStatus.FAILED) {
        result.failOrderReason = 'Payment failed.';
        return result;
    }

    var pollingTimeoutResult = module.exports.checkPollingTimeout(paymentTransaction);
    if (pollingTimeoutResult.isTimedOut) {
        result.failOrderReason = 'Polling for payment request status timed out.';
        result.customerErrorMessage = Resource.msg('checkout.payment.polling.timeout.error', 'clik2pay', null);
        return result;
    }

    result.action = OrderAction.WaitAndRetry;
    result.pollingInterval = pollingTimeoutResult.pollingInterval;

    return result;
}

/**
 * Fail the given order
 *
 * @param {dw.order.Order} order Order
 * @param {string?} [orderChangeNote] Order change note
 */
function failOrder(order, orderChangeNote) {
    var OrderMgr = require('dw/order/OrderMgr');
    var Transaction = require('dw/system/Transaction');

    Transaction.wrap(function () {
        OrderMgr.failOrder(order, true);
        if (orderChangeNote) {
            order.trackOrderChange(orderChangeNote);
        }
    });
}

/**
 * Attempt to place or fail the order if a payment status notification has already arrived for the given order.
 * Otherwise the return object indicates that polling should continue.
 *
 * @param {string} orderNo Order number
 * @param {string} orderToken Order token
 * @returns {Object} Success flag, error information
 */
function tryContinuePlaceOrder(orderNo, orderToken) {
    var Logger = require('dw/system/Logger');
    var OrderMgr = require('dw/order/OrderMgr');
    var paymentInstrumentHelpers = require('*/cartridge/scripts/helpers/clik2pay/paymentInstrumentHelpers');
    var checkoutHelperWrappers = require('*/cartridge/scripts/helpers/clik2pay/checkoutHelperWrappers');

    var log = Logger.getLogger('clik2pay.placeOrderHelpers.tryContinuePlaceOrder');
    var logMessagePrefix = 'Order ' + orderNo + ': ';
    var errorResult = {
        isSuccess: false
    };
    var successResult = {
        isSuccess: true
    };

    var order = OrderMgr.getOrder(orderNo, orderToken);
    if (!order) {
        errorResult.errorMessage = 'Order not found';
        log.error(logMessagePrefix + errorResult.errorMessage);
        return errorResult;
    }

    var paymentInstrument = paymentInstrumentHelpers.findClik2payPaymentInstrument(order);
    if (!paymentInstrument) {
        errorResult.errorMessage = 'Clik2pay payment instrument not found';
        log.error(logMessagePrefix + errorResult.errorMessage);
        return errorResult;
    }

    var paymentTransaction = paymentInstrument.paymentTransaction;
    if (!paymentTransaction) {
        errorResult.errorMessage = 'Payment transaction is null';
        log.error(logMessagePrefix + errorResult.errorMessage);
        return errorResult;
    }

    var orderActionResult = module.exports.determineOrderAction(order, paymentTransaction);

    if (orderActionResult.action === OrderAction.PlaceOrder) {
        var continuePlaceOrderResult = checkoutHelperWrappers.continuePlaceOrder(order);
        return continuePlaceOrderResult;
    }

    if (orderActionResult.action === OrderAction.WaitAndRetry) {
        successResult.keepPolling = true;
        successResult.pollingInterval = orderActionResult.pollingInterval;
        return successResult;
    }

    if (orderActionResult.action === OrderAction.ShowConfirmation) {
        var confirmationUrlResult = checkoutHelperWrappers.getOrderConfirmationUrl(order);
        return confirmationUrlResult;
    }

    var failOrderReason = 'Unhandled or empty order action: ' + orderActionResult.action;
    if (orderActionResult.action === OrderAction.FailOrder) {
        failOrderReason = orderActionResult.failOrderReason;
    }

    module.exports.failOrder(order, failOrderReason);

    log.error(logMessagePrefix + failOrderReason);
    errorResult.errorMessage = failOrderReason;
    errorResult.customerErrorMessage = orderActionResult.customerErrorMessage;
    return errorResult;
}

module.exports = {
    OrderAction: OrderAction,
    checkPollingTimeout: checkPollingTimeout,
    determineOrderAction: determineOrderAction,
    failOrder: failOrder,
    tryContinuePlaceOrder: tryContinuePlaceOrder
};
