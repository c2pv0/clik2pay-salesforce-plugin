'use strict';

/**
 * Creates a new problem Reporter, which is meant to unify reporting of errors and warnings about problems like e.g. missing configuration and parameters
 *
 * Errors are logged on error level and are expected to have to be solved before the reported on feature works
 * Warnings are logged on warning level and solving them might raise efficiency of the feature but are not mandatory to solve
 *
 * @param {string} id the id of the entity for which the missing configuration / parameters shall be reported
 * @param {dw.system.Log} log the log which problems are written to. (Should use an adequate logging category)
 * @param {string} [errorReportFormat] optional override for mandatory report format. Will default to '{0} is missing mandatory values:\n{1}' with {0} == id && {1} == <compiled report>
 * @param {string} [warningReportFormat] optional override for recommended report format. Will default to '{0} is missing recommended values: {1}' with {0} == id && {1} == <compiled report>
 * @constructor
 */
function ProblemReporter(id, log, errorReportFormat, warningReportFormat) {
    this.id = id;
    this.log = log || require('dw/system/Logger').getLogger('ProblemReporter');
    this.errorMessages = [];
    this.warningMessages = [];
    this.errorReportFormat = errorReportFormat || '{0} is missing mandatory values:\n{1}';
    this.warningReportFormat = warningReportFormat || '{0} is missing recommended values: {1}';
}

/**
 * Adds an error message to the list of things to report
 * @param {string} message the message to add to the report
 */
ProblemReporter.prototype.addErrorMessage = function (message) {
    // only add messages once
    if (this.errorMessages.indexOf(message) === -1) {
        this.errorMessages.push(message);
    }
};

/**
 * Adds a warning to the list of things to report
 * @param {string} message the message to add to the report
 */
ProblemReporter.prototype.addWarningMessage = function (message) {
    // only add messages once
    if (this.warningMessages.indexOf(message) === -1) {
        this.warningMessages.push(message);
    }
};

/**
 * Checks if there is a report on identified errors available
 * @returns {boolean} true if there are errors to report on
 */
ProblemReporter.prototype.hasErrors = function () {
    return this.errorMessages.length > 0;
};

/**
 * Checks if there is a report on warnings available
 * @returns {boolean} true if there are warnings to report on
 */
ProblemReporter.prototype.hasWarnings = function () {
    return this.warningMessages.length > 0;
};

/**
 * Creates a formatted report about identified errors
 * @returns {string} the formatted error report, an empty string if there is nothing to report
 */
ProblemReporter.prototype.getErrorReport = function () {
    if (!this.hasErrors()) {
        return '';
    }

    var StringUtils = require('dw/util/StringUtils');
    return StringUtils.format(this.errorReportFormat, this.id, this.errorMessages.join('\n'));
};

/**
 * Creates a formatted report about necessary warnings
 * @returns {string} the formatted report about warnings, an empty string if there is nothing to report
 */
ProblemReporter.prototype.getWarningReport = function () {
    if (!this.hasWarnings()) {
        return '';
    }

    var StringUtils = require('dw/util/StringUtils');
    return StringUtils.format(this.warningReportFormat, this.id, this.warningMessages.join(', '));
};

/**
 * Creates a full report on all available errors and warnings
 * @returns {string} the formatted report about the currently found problems, an empty string if there is nothing to report
 */
ProblemReporter.prototype.getStatusReport = function () {
    var StringUtils = require('dw/util/StringUtils');
    var hasErrors = this.hasErrors();
    var hasWarnings = this.hasWarnings();

    if (hasErrors && hasWarnings) {
        return StringUtils.format('{0} \n\n{1}', this.getErrorReport(), this.getWarningReport());
    } else if (hasErrors) {
        return this.getErrorReport();
    } else if (hasWarnings) {
        return this.getWarningReport();
    }
    return '';
};

/**
 * Logs the found errors and warnings as formatted reports on their appropriate logging levels
 */
ProblemReporter.prototype.logStatus = function () {
    if (this.hasErrors()) {
        this.log.error(this.errorReportFormat, this.id, this.errorMessages.join(', '));
    }

    if (this.hasWarnings()) {
        this.log.warn(this.warningReportFormat, this.id, this.warningMessages.join(', '));
    }
};

module.exports = ProblemReporter;
