'use strict';

/**
 * Helper functions related to payment instruments
 */


/**
 * Searches the line item container for a Clik2pay payment instrument.
 *
 * @param {dw.order.LineItemCtnr} lineItemContainer Basket or order
 * @returns {dw.order.OrderPaymentInstrument?} Order payment instrument
 */
function findClik2payPaymentInstrument(lineItemContainer) {
    var Logger = require('dw/system/Logger');
    var Order = require('dw/order/Order');
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');

    var log = Logger.getLogger('clik2pay.paymentInstrumentHelpers.findClik2payPaymentInstrument');

    var instruments = lineItemContainer.getPaymentInstruments(constants.PaymentMethodClik2payInterac).toArray();

    if (instruments.length <= 0) {
        return null;
    }

    if (instruments.length > 1) {
        var logMessagePrefix = lineItemContainer instanceof Order ? 'Order ' + lineItemContainer.orderNo + ': ' : '';
        log.error(logMessagePrefix + 'Found more than one Clik2pay payment instrument.');
    }

    return instruments[0];
}

module.exports = {
    findClik2payPaymentInstrument: findClik2payPaymentInstrument
};
