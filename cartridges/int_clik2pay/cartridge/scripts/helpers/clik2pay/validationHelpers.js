'use strict';

/**
 * Checks if all mandatory and recommended parameters are set and updates the reporter accordingly.
 * Logs validation errors and warnings.
 *
 * @param {Object} parameters the parameters object used in calling
 * @param {dw.system.Logger} serviceLog the logger this service is supposed to use for logging
 * @returns {ProblemReporter} the reporter with the problems to report if any
 */
function validateCreatePaymentRequestParameters(parameters, serviceLog) {
    var ProblemReporter = require('*/cartridge/scripts/helpers/clik2pay/problemReporter');
    var validationReporter = new ProblemReporter('createPaymentRequest - ParameterValidation', serviceLog, '{0} found those problems:\n{1}');

    if (!parameters) {
        validationReporter.addErrorMessage('Without parameters to validate there is no validation');
        validationReporter.logStatus();
        return validationReporter;
    }

    // Mandatory: Authentication Token
    if (!parameters.token) {
        validationReporter.addErrorMessage('Could not retrieve authentication Token');
    }

    // Mandatory: We need a apiKey to authorize our calls to the api
    if (!parameters.apiKey) {
        validationReporter.addErrorMessage('api key for clik2pay in service configuration');
    }

    // Mandatory: Amount to authorize. If <= 0 we should not make a call
    if (parameters.body.amount <= 0) {
        validationReporter.addErrorMessage('Can not authorize an amount of ' + parameters.body.amount);
    }

    // Mandatory: payer data
    if (!parameters.body.payer) {
        validationReporter.addErrorMessage('payer data is missing');
    } else if (!parameters.body.payer.name) {
        validationReporter.addErrorMessage('customer name is missing');
    }

    // Conditional mandatory (deliveryMode dependencies)
    if (parameters.body.deliveryMode !== 'NONE') {
        if (parameters.body.deliveryMode === 'EMAIL') {
            if (!parameters.body.payer.email) {
                validationReporter.addErrorMessage('deliveryMode is EMAIL and customer mail is missing in order.customerEmail');
            }
        } else if (parameters.body.deliveryMode === 'SMS' && !parameters.body.payer.mobileNumber) {
            validationReporter.addErrorMessage('deliveryMode is SMS and customer phone number is missing in order address data');
        }
    }

    validationReporter.logStatus();
    return validationReporter;
}

/**
 * Checks if the response is acknowledging the send data and everything is in order
 * @param {Object} parameters the parameters object used in calling
 * @param {Object} createPaymentRequestResponse what came back from the clik2pay service endpoint
 * @param {dw.system.Logger} serviceLog the logger this service is supposed to use for logging
 * @returns {string} the problems to report. An empty string of no problems were discovered
 */
function validateCreatePaymentRequestResult(parameters, createPaymentRequestResponse, serviceLog) {
    var ProblemReporter = require('*/cartridge/scripts/helpers/clik2pay/problemReporter');
    var validationReporter = new ProblemReporter('createPaymentRequest - ResponseValidation', serviceLog, '{0} found those problems:\n{1}');

    if (!parameters || !createPaymentRequestResponse) {
        if (!parameters) {
            validationReporter.addErrorMessage('could not start validation because no parameters were passed');
        }
        if (!createPaymentRequestResponse) {
            validationReporter.addErrorMessage('could not start validation because no service call result was passed');
        }

        // exit early because without parameters to compare there is no validation
        validationReporter.logStatus();
        return validationReporter.getStatusReport();
    }

    if (!createPaymentRequestResponse.paymentLink) {
        // the most important part of this whole request
        validationReporter.addErrorMessage('create payment request did not return link for clik2pay button');
    }

    // create and use format for this specific kind of problem
    var StringUtils = require('dw/util/StringUtils');
    var messageFormat = 'clik2pay responded with wrong {0} ({1}) instead of requested {0} ({2})';

    // compare the values sent in the service request
    // to the values echoed back by the service response
    [
        'amount', 'type', 'payer.name', 'deliveryMode', 'invoiceNumber',
        'merchantTransactionId', 'businessUnit'
    ].forEach(function (attributeName) {
        var requestValue = parameters.body[attributeName];
        var responseValue = createPaymentRequestResponse[attributeName];

        if (attributeName === 'payer.name') {
            requestValue = parameters.body.payer.name;
            responseValue = createPaymentRequestResponse.payer.name;
        }

        if (requestValue !== responseValue) {
            var errorMessage = StringUtils.format(messageFormat, attributeName, responseValue, requestValue);
            validationReporter.addErrorMessage(errorMessage);
        }
    });

    validationReporter.logStatus();

    return validationReporter.getStatusReport();
}

module.exports = {
    validateCreatePaymentRequestParameters: validateCreatePaymentRequestParameters,
    validateCreatePaymentRequestResult: validateCreatePaymentRequestResult
};
