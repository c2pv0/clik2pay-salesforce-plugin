'use strict';

/* global request: readonly, session: readonly */

/**
 * Helper functions for integration tests
 */

var testAddress = {
    firstName: 'depositSubmit',
    fullName: 'should not be used because of firstName',
    phone: '2555679876'
};

var testOrder = {
    billingAddress: testAddress,
    defaultShipment: {
        shippingAddress: testAddress
    },
    customerEmail: 'no.real.email@example.com',
    orderNo: 'intTestMockOrder1010',
    orderToken: 'Token1010',
    customerLocaleID: 'fr_CA',
    customerNo: 'customerNo123456',
    totalGrossPrice: {
        value: 123.45
    }
};


/* istanbul ignore next */
/**
 * Remove basket line items and payment instruments.
 *
 * @param {dw.order.Basket} basket Basket to be cleared
 */
function clearBasket(basket) {
    var Transaction = require('dw/system/Transaction');

    Transaction.wrap(function () {
        basket.removeAllPaymentInstruments();

        basket.shipments.toArray().forEach(function (shipment) {
            if (shipment !== basket.defaultShipment) { // the default shipment can not be removed
                basket.removeShipment(shipment);
            }
        });

        basket.allProductLineItems.toArray().forEach(function (pli) {
            basket.removeProductLineItem(pli);
        });

        basket.couponLineItems.toArray().forEach(function (cli) {
            basket.removeCouponLineItem(cli);
        });

        basket.giftCertificateLineItems.toArray().forEach(function (gli) {
            basket.removeGiftCertificateLineItem(gli);
        });
    });
}

/* istanbul ignore next */
/**
 * Set test address data.
 * @param {dw.order.OrderAddress} address Address
 */
function setTestAddress(address) {
    address.address1 = '485 Rte 222';
    address.city = 'St-Denis-de-Brompton';
    address.setCountryCode('CA');
    address.firstName = 'Aeryn';
    address.lastName = 'Sun';
    address.phone = '(819) 846-4449';
    address.postalCode = 'J0B 2P0';
    address.stateCode = 'QC';
}

/* istanbul ignore next */
/**
 * Attempt to set a shipping method if none is set.
 *
 * @param {dw.order.Shipment} shipment Shipment
 */
function ensureShipmentHasMethod(shipment) {
    var ShippingMgr = require('dw/order/ShippingMgr');

    if (shipment.shippingMethod) {
        return;
    }

    var defaultMethod = ShippingMgr.getDefaultShippingMethod();
    if (defaultMethod) {
        shipment.setShippingMethod(defaultMethod);
        return;
    }

    var applicableMethods = ShippingMgr.getShipmentShippingModel(shipment).applicableShippingMethods.toArray();
    var shippingMethod = applicableMethods.length > 0 ? applicableMethods[0] : null;
    if (shippingMethod) {
        shipment.setShippingMethod(shippingMethod);
    }
}

/* istanbul ignore next */
/**
 * Create a test basket.
 *
 * @param {boolean} simulatePaymentSuccess If true then payment success is simulated
 * @returns {dw.order.Basket?} Test basket
 */
function createTestBasket(simulatePaymentSuccess) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Currency = require('dw/util/Currency');
    var Site = require('dw/system/Site');
    var Transaction = require('dw/system/Transaction');
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');

    var productID = Site.current.ID === 'RefArch' ? '701642843627M' : '701644060657';

    request.setLocale('en_CA');
    session.setCurrency(Currency.getCurrency('CAD'));

    return Transaction.wrap(function () {
        var basket = BasketMgr.getCurrentOrNewBasket();
        if (!basket) {
            return null;
        }

        module.exports.clearBasket(basket);

        basket.updateCurrency();

        var shipment = basket.getDefaultShipment();
        var productLineItem = basket.createProductLineItem(productID, shipment);
        productLineItem.setQuantityValue(1);

        shipment.createShippingAddress();
        module.exports.setTestAddress(shipment.shippingAddress);

        basket.createBillingAddress();
        module.exports.setTestAddress(basket.billingAddress);

        module.exports.ensureShipmentHasMethod(shipment);

        if (simulatePaymentSuccess) {
            basket.billingAddress.firstName = 'depositSubmit';
            basket.defaultShipment.shippingAddress.firstName = 'depositSubmit';
        }

        basket.customerEmail = basket.billingAddress.firstName.toLowerCase() + '@example.com';
        basket.updateTotals();

        var HookMgr = require('dw/system/HookMgr');
        HookMgr.callHook('dw.order.calculate', 'calculate', basket);

        basket.createPaymentInstrument(constants.PaymentMethodClik2payInterac, basket.totalGrossPrice);

        return basket;
    });
}

/* istanbul ignore next */
/**
 * Create a test order.
 *
 * @param {boolean} simulatePaymentSuccess If true then payment success is simulated
 * @returns {Object} Order and success flag
 */
function createTestOrder(simulatePaymentSuccess) {
    var OrderMgr = require('dw/order/OrderMgr');
    var Site = require('dw/system/Site');
    var Transaction = require('dw/system/Transaction');
    var credentialHelpers = require('*/cartridge/scripts/helpers/clik2pay/credentialHelpers');

    var basket = module.exports.createTestBasket(simulatePaymentSuccess);
    var order = Transaction.wrap(function () {
        return OrderMgr.createOrder(basket);
    });
    var notifyCredentials = credentialHelpers.getNotifyEndpointCredentialString(Site.current.ID);

    return {
        isSuccess: true,
        orderNo: order.orderNo,
        orderToken: order.orderToken,
        notifyCredentials: notifyCredentials
    };
}

module.exports = {
    testAddress: testAddress,
    testOrder: testOrder,
    clearBasket: clearBasket,
    setTestAddress: setTestAddress,
    ensureShipmentHasMethod: ensureShipmentHasMethod,
    createTestBasket: createTestBasket,
    createTestOrder: createTestOrder
};
