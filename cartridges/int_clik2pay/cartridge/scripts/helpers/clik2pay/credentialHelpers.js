'use strict';

/**
 * Helper functions for service credentials
 */

/**
 * Retrieves a service credential object
 *
 * @param {string} serviceID Service ID. Arbitrary but must exist, used to access the credential object, no service call is made.
 * @param {string} credentialID ID of the credential object that is requested
 * @returns {dw.svc.ServiceCredential?} Credential object
 */
function getServiceCredential(serviceID, credentialID) {
    var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
    var Logger = require('dw/system/Logger');

    var log = Logger.getLogger('clik2pay.credentialHelpers.getServiceCredential');
    var service;

    // get a dummy service that can be used to get the credentials
    try {
        service = LocalServiceRegistry.createService(serviceID, {});
    } catch (error) {
        log.error(error.message);
        return null;
    }

    try {
        service.setCredentialID(credentialID);

        var serviceConfiguration = service.getConfiguration();
        var credential = serviceConfiguration.getCredential();
        if (!credential) {
            log.error('Service configuration {0} has no credential assigned', serviceConfiguration.ID);
            return null;
        }

        return credential;
    } catch (error) {
        log.error('Could not find service credential with ID {0}', credentialID);
        return null;
    }
}

/**
 * Get the credential ID for the authentication service.
 *
 * @param {string} siteID Site ID
 * @returns {string} Credential ID
 */
function getAuthenticationCredentialID(siteID) {
    var System = require('dw/system/System');

    var instanceType = System.instanceType === System.PRODUCTION_SYSTEM ? 'production' : 'sandbox';
    var credentialID = 'clik2pay.authentication.' + instanceType + '.' + siteID;

    return credentialID;
}

/**
 * Get the service credentials used to perform the authentication
 * service call that fetches the authentication token from Clik2pay.
 *
 * @param {string} siteID Site ID
 * @returns {dw.svc.ServiceCredential?} Service credentials
 */
function getAuthenticationCredentials(siteID) {
    var Logger = require('dw/system/Logger');
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');

    var log = Logger.getLogger('clik2pay.credentialHelpers.getAuthenticationCredentials');

    var serviceID = constants.ServiceIDs.getAuthToken;
    var credentialID = module.exports.getAuthenticationCredentialID(siteID);

    var credential = module.exports.getServiceCredential(serviceID, credentialID);
    if (!credential) {
        return null;
    }

    if (!credential.user || credential.user.length < constants.MinUserPasswordLength
        || !credential.password || credential.password.length < constants.MinUserPasswordLength) {
        log.error('Service credential {0} is configured incorrectly. Username and password each need to be at least {1} characters long.',
            credential.ID, constants.MinUserPasswordLength.toFixed(0));
        return null;
    }

    return credential;
}

/**
 * Get the credentials to authenticate an incoming notification request from Clik2pay.
 *
 * These are the same credentials used by the site to perform the authentication
 * service call that fetches the authentication token from Clik2pay.
 *
 * @param {string} siteID Site ID
 * @returns {string?} Username and password separated by ':'
 */
function getNotifyEndpointCredentialString(siteID) {
    var credentials = module.exports.getAuthenticationCredentials(siteID);
    if (!credentials) {
        return null;
    }

    return credentials.user + ':' + credentials.password;
}

module.exports = {
    getServiceCredential: getServiceCredential,
    getAuthenticationCredentialID: getAuthenticationCredentialID,
    getAuthenticationCredentials: getAuthenticationCredentials,
    getNotifyEndpointCredentialString: getNotifyEndpointCredentialString
};
