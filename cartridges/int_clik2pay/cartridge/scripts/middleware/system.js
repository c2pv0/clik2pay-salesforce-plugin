'use strict';

var System = require('dw/system/System');
var URLUtils = require('dw/web/URLUtils');

/**
 * Break the middleware chain and redirect to the home page
 * if the system instance type is Production.
 *
 * @param {Object} req Request object
 * @param {Object} res Response object
 * @param {Function} next Next call in the middleware chain
 */
function nonProduction(req, res, next) {
    if (System.getInstanceType() === System.PRODUCTION_SYSTEM) {
        res.redirect(URLUtils.url('Home-Show'));
    }
    next();
}

module.exports = {
    nonProduction: nonProduction
};
