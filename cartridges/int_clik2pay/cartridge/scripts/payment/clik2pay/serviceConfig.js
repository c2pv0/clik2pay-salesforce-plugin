'use strict';

/**
 * Imports type definitions which are used in multiple files
 * @typedef {import('*\/cartridge/scripts/helpers/clik2pay/types')} clik2PayServiceResponse
 */

var Bytes = require('dw/util/Bytes');
var Encoding = require('dw/crypto/Encoding');
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
 *
 * @param {dw.svc.HTTPFormService} service the service instance, that will make the authentication call
 * (Note: the service parameter is passed automatically when the service is called)
 * @param {Object} requestParameters the object holding all necessary parameters that is passed when the service is called
 * @returns {string} the
 */
function createAuthenticationRequest(service, requestParameters) {
    // Note: Parameter verification is done in serviceAPI, so we can expect that they are there
    var rawCredentials = requestParameters.username + ':' + requestParameters.password;

    // setup headers for authentication
    service.addHeader('Authorization', 'Basic ' + Encoding.toBase64(new Bytes(rawCredentials)));
    service.addHeader('Content-Type', 'application/x-www-form-urlencoded');

    return 'grant_type=client_credentials&scope=payment_request%2Fall';
}

/**
 * This creates a request to create a payment request, which might make the name look
 * a bit silly on the first look, but we have to differentiate this call from the
 * a request to the create payment endpoint.
 *
 * (The "create payment" call might be out of scope of the original link cartridge but
 * might be included later, so let's keep its existence in mind, just to make live for
 * later developers easier.)
 *
 * @param {dw.svc.HTTPService} service The service instance, that will make the create payment request call
 * @param {Object} parameters the object holding all necessary parameters that is passed when the service is called
 * @returns {string} The body for the request
 */
function createPaymentRequestRequest(service, parameters) {
    service.setAuthentication('NONE');
    service.addHeader('x-api-key', parameters.apiKey);
    service.addHeader('Authorization', 'Bearer ' + parameters.token);
    service.addHeader('Content-Type', 'application/json');

    service.setURL(service.getURL() + '/payment-requests');

    var body = parameters.body;
    return JSON.stringify(body);
}

/**
 * When the service is called this callback will parse the response
 *
 * @param {dw.svc.Service} service the service instance whose call lead the the response
 * @param {dw.net.HTTPClient} httpClient the client that has all the information about how the call went
 * @returns {clik2PayServiceResponse} the parsed result of the service call
 */
function parseResponse(service, httpClient) {
    var statusCode = httpClient.statusCode.toString();
    /** @type {clik2PayServiceResponse} */
    var result = {
        success: statusCode.indexOf('2') === 0,
        statusCode: statusCode,
        response: {}
    };

    try {
        if (result.success && httpClient.text) {
            result.response = JSON.parse(httpClient.text);
        } else if (!result.success && httpClient.errorText) {
            result.response = JSON.parse(httpClient.errorText);
        } else {
            // unexpected case, that should never happen
            result.success = false;
            result.response = {
                fatalErrorMessage: 'could not parse response from server because no text / errorText was available'
            };
        }
    } catch (exception) {
        result.success = false;
        result.response = {
            fatalErrorMessage: 'could not parse server response',
            serverResponse: httpClient.text ? httpClient.text : httpClient.errorText
        };
    }

    return JSON.stringify(result);
}

/**
 * Creates a local service instance to call the given clik2pay endpoint.
 *
 * @param {string} endpointName Just the last part of the service name either 'get_auth_token' or 'create_payment_request'
 * @returns {dw.svc.Service} The created service instance
 */
function createService(endpointName) {
    var System = require('dw/system/System');
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');

    // To keep the code better readable we only take the endpointName, so we'll map it to the actual service configuration
    var endpoint = endpointName === 'get_auth_token' ? constants.ServiceIDs.getAuthToken : constants.ServiceIDs.createPaymentRequest;

    var clik2PayService = null;
    if (endpointName === 'get_auth_token') {
        // get Auth Token -> HTTPForm
        clik2PayService = LocalServiceRegistry.createService(endpoint, {
            createRequest: createAuthenticationRequest,
            parseResponse: parseResponse
        });
    } else {
        // create Payment Request -> HTTP
        clik2PayService = LocalServiceRegistry.createService(endpoint, {
            createRequest: createPaymentRequestRequest,
            parseResponse: parseResponse,
            getRequestLogMessage: function (request) {
                if (System.instanceType !== System.PRODUCTION_SYSTEM) {
                    return JSON.stringify(request);
                }
                return '';
            }
        });
    }

    return clik2PayService;
}

module.exports = {
    createAuthenticationRequest: createAuthenticationRequest,
    createPaymentRequestRequest: createPaymentRequestRequest,
    parseResponse: parseResponse,
    createService: createService
};
