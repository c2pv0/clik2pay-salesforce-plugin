'use strict';

/**
 * Imports type definitions which are used in multiple files
 * @typedef {import('*\/cartridge/scripts/helpers/clik2pay/types')} clik2PayServiceResponse
 * @typedef {import('*\/cartridge/scripts/helpers/clik2pay/types')} createPaymentRequestResult
 */

/**
 * @typedef authenticationResult
 * @property {boolean} success true if a bearer token could be retrieved, false in case of problems
 * @property {string} [token] the bearer token if available
 * @property {clik2PayServiceResponse} [serviceCallResult] the result of the service call in case of failure (to ease debugging)
 */

/**
 * Creates, stores and retrieves the necessary Token to authenticate calls to the clik2Pay endpoints
 *
 * @returns {authenticationResult} Success flag and token
 */
function authenticate() {
    var Logger = require('dw/system/Logger');
    var Site = require('dw/system/Site');
    var serviceConfig = require('*/cartridge/scripts/payment/clik2pay/serviceConfig');
    var credentialHelpers = require('*/cartridge/scripts/helpers/clik2pay/credentialHelpers');

    var serviceLog = Logger.getLogger('clik2pay.serviceAPI.authenticate');
    var authenticationFailure = {
        success: false
    };

    // the service has to be created anyways to get access to the current configuration;
    var authenticationService = serviceConfig.createService('get_auth_token');
    if (!(authenticationService && authenticationService.configuration)) {
        serviceLog.error('could not initialize the authentication service for clik2pay, please check the configuration');
        return authenticationFailure;
    }

    // check if there is a cached and still valid authentication token
    var authenticationHelper = require('*/cartridge/scripts/helpers/clik2pay/tokenHelpers');
    var previousToken = authenticationHelper.retrieveToken(authenticationService);
    if (previousToken) {
        // We still have a valid token in the cache. So we can return it early
        return {
            success: true,
            token: previousToken
        };
    }

    // no token could be retrieved, so we have to create a new one
    var credential = credentialHelpers.getAuthenticationCredentials(Site.current.ID);
    if (!credential) {
        return authenticationFailure;
    }

    authenticationService.setCredentialID(credential.ID);
    var parameters = {
        username: credential.user,
        password: credential.password
    };

    var serviceCallResult = authenticationService.call(parameters);
    if (serviceCallResult.ok) {
        var serviceResponse = JSON.parse(serviceCallResult.object);
        if (serviceResponse.success) {
            var token = serviceResponse.response.access_token;
            var milliSecondsBeforeExpiration = serviceResponse.response.expires_in * 1000; // expires_in holds the token lifetime in seconds

            // cache the token
            authenticationHelper.storeToken(authenticationService, token, milliSecondsBeforeExpiration);

            return {
                success: true,
                token: token
            };
        }
    }

    if (serviceCallResult.errorMessage) {
        serviceLog.error(serviceCallResult.errorMessage);
    }
    return authenticationFailure;
}

/**
* @typedef {Object} payer
* @property {string} name The full name of the payer
* @property {string} [merchantAccountId] the customer number
* @property {string} [email] the customer email, only required in case deliveryMode is Email, optional otherwise
* @property {string} [mobileNumber] The mobile number to contact the payer. Only required when deliveryMode is SMS, optional otherwise
* @property {string} [preferredLanguage] The preferred language of correspondence. Should be a 2 character language code, as per the ISO 639-1 standard. Currently, only English (en) and French (fr) are supported.
*/

/**
 * Extracts the necessary information from the order and builds a payer object for service calls from it
 * @param {dw.order.Order} order the salesforce order created right before this call
 * @returns {payer} the payer object
 */
function buildPayerObject(order) {
    var billingAddress = order.billingAddress;
    var shippingAddress = order.defaultShipment.shippingAddress;
    if (!billingAddress && !shippingAddress) {
        throw new Error('Order ' + order.orderNo + ': no address found');
    }

    var address = billingAddress || shippingAddress;
    var fullName = address.firstName === 'depositSubmit' ? 'depositSubmit' : address.fullName;
    var phoneNumber = billingAddress && billingAddress.phone ? billingAddress.phone : shippingAddress.phone;

    var preferredLanguage = order.customerLocaleID.startsWith('fr') ? 'fr' : 'en';

    var payer = {
        name: fullName,
        email: order.customerEmail,
        merchantAccountId: order.customerNo,
        preferredLanguage: preferredLanguage
    };

    if (phoneNumber) {
        payer.mobileNumber = phoneNumber;
    }

    return payer;
}

/**
 * triggers call to clik2pay to create a payment request
 * @param {dw.order.Order} order the SFCC order the call is made for
 * @returns {createPaymentRequestResult} the result of the call
 */
function createPaymentRequest(order) {
    var Logger = require('dw/system/Logger');
    var Site = require('dw/system/Site');
    var constants = require('*/cartridge/scripts/helpers/clik2pay/constants');
    var serviceConfig = require('*/cartridge/scripts/payment/clik2pay/serviceConfig');
    var validationHelpers = require('*/cartridge/scripts/helpers/clik2pay/validationHelpers');

    var SERVICE_ID = 'create_payment_request';
    var currentSite = Site.current;
    var paymentRequestFailure = {
        success: false,
        problemsToReport: 'uninitialized'
    };

    var ProblemReporter = require('*/cartridge/scripts/helpers/clik2pay/problemReporter');
    var serviceLog = Logger.getLogger('clik2pay.serviceAPI.createPaymentRequest');
    var reporter = new ProblemReporter(SERVICE_ID, serviceLog);

    // Mandatory: no order no call - Also: no anonymous payments
    if (!order || !order.billingAddress || !order.defaultShipment.shippingAddress || !order.billingAddress.fullName) {
        reporter.addErrorMessage('Without an order with set address data no call to {0} at clik2pay is possible', SERVICE_ID);
        reporter.logStatus();
        paymentRequestFailure.problemsToReport = reporter.getStatusReport();
        return paymentRequestFailure;
    }

    // Mandatory: without a token no authorization for requests
    var tokenRequest = module.exports.authenticate();
    if (!tokenRequest.success) {
        reporter.addErrorMessage('Can not retrieve a authorization token to make call to {0} at clik2pay', SERVICE_ID);
        reporter.logStatus();
        paymentRequestFailure.problemsToReport = reporter.getStatusReport();
        return paymentRequestFailure;
    }

    var createPaymentRequestService = serviceConfig.createService(SERVICE_ID);
    var serviceConfiguration = createPaymentRequestService.configuration;

    // PARAMETERS INITIALIZATION
    var parameters = {
        body: {
            amount: order.totalGrossPrice.value,
            businessUnit: order.orderToken,
            deliveryMode: constants.PaymentRequestDeliveryMode,
            invoiceNumber: order.orderNo,
            merchantTransactionId: order.orderNo,
            payer: module.exports.buildPayerObject(order),
            type: constants.PaymentRequestTransactionType
        },
        apiKey: serviceConfiguration.credential.password,
        token: tokenRequest.token
    };

    // OPTIONAL PARAMETERS
    var emailTemplate = currentSite.getCustomPreferenceValue('c2pCustomerNotificationEmailTemplateID');
    if (emailTemplate) {
        parameters.body.emailTemplate = emailTemplate;
    }

    var displayMessage = currentSite.getCustomPreferenceValue('c2pPaymentRequestDisplayMessage');
    if (displayMessage) {
        parameters.body.displayMessage = displayMessage;
    }

    var merchantID = currentSite.getCustomPreferenceValue('c2pMerchantID');
    if (merchantID) {
        parameters.body.merchant = {
            id: merchantID
        };
    }

    // VALIDATION
    // Check if everything is there
    var parameterProblemReporter = validationHelpers.validateCreatePaymentRequestParameters(parameters, serviceLog);

    if (parameterProblemReporter.hasErrors()) {
        paymentRequestFailure.parameters = parameters;
        paymentRequestFailure.problemsToReport = parameterProblemReporter.getStatusReport();
        return paymentRequestFailure;
    }

    // All necessary data is available => MAKE THE CALL
    var serviceCallResult = createPaymentRequestService.call(parameters);
    // Note: serviceCallResult.object only exists in case of status OK
    var createPaymentRequestResult = JSON.parse(serviceCallResult.object);

    // VALIDATE RESPONSE
    var problemsToReport;
    if (createPaymentRequestResult && createPaymentRequestResult.success) {
        problemsToReport = validationHelpers.validateCreatePaymentRequestResult(parameters, createPaymentRequestResult.response, serviceLog);
        if (problemsToReport.length === 0) {
            var response = createPaymentRequestResult.response;
            var link2ResourceEntries = response.links.filter(function (entry) { return entry.rel === 'payment-request'; });
            var link2Resource = 'No link available';
            if (link2ResourceEntries.length) {
                // there should be only one anyway
                link2Resource = link2ResourceEntries[0].href;
            }

            return {
                success: true,
                clik2payResourceLink: link2Resource,
                confirmedAmount: response.amount,
                id: response.id,
                merchantTransactionId: response.merchantTransactionId,
                paymentRequestLink: response.paymentLink,
                qrCodeLink: response.qrCodeLink,
                shortCode: response.shortCode,
                status: response.status
            };
        }
    }

    // In case of error use serviceCallResult.msg instead of serviceCallResult.object (no parsing)
    problemsToReport = problemsToReport || serviceCallResult.msg;
    if (serviceCallResult.errorMessage) {
        serviceLog.error('Order {0}: {1}', order.orderNo, serviceCallResult.errorMessage);
        var customerErrorMessage = module.exports.getCustomerErrorMessage(serviceCallResult.errorMessage);
        if (customerErrorMessage) {
            problemsToReport = customerErrorMessage;
        }
    }

    return {
        success: false,
        paymentRequestLink: '',
        problemsToReport: problemsToReport,
        result: createPaymentRequestResult
    };
}

/**
 * Try to parse an API error response and read its error message.
 *
 * Example error response:
 * {"message":"Failed to parse resource: paymentRequestResource","errors":[{"property":"payer.mobileNumber","error":"A valid phone number is required, with 10 digits and no spaces or other characters"}]}
 *
 * @param {string?} errorResponseText Service response text in case of an error
 * @returns {string?} Customer error message
 */
function getCustomerErrorMessage(errorResponseText) {
    if (!errorResponseText) {
        return null;
    }

    var errorResponse;
    try {
        errorResponse = JSON.parse(errorResponseText);
        if (!errorResponse) {
            return null;
        }
    } catch (exception) {
        return null;
    }

    if (!errorResponse.errors || !errorResponse.errors.length) {
        return null;
    }

    // within the `errors` array get and concatenate all `error` messages
    var customerErrorMessage = errorResponse.errors.map(function (errorEntry) {
        return errorEntry.error;
    }).filter(function (errorMessage) {
        return !!errorMessage;
    }).join(' ').trim();

    return customerErrorMessage || null;
}

module.exports = {
    authenticate: authenticate,
    createPaymentRequest: createPaymentRequest,
    buildPayerObject: buildPayerObject,
    getCustomerErrorMessage: getCustomerErrorMessage
};
