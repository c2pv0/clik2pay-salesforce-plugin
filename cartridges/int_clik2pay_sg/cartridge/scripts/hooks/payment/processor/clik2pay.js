'use strict';

/*
 * See module processorHelpers for more information on the checkout flow.
 */

/**
 * Remove all current payment instruments from the basket and
 * then create a Clik2pay payment instrument.
 *
 * @param {Object} params Basket and payment method ID
 * @returns {Object} Success or error flag
 */
function handle(params) {
    var processorHelpers = require('*/cartridge/scripts/helpers/clik2pay/processorHelpers');

    var result = processorHelpers.handle(params.Basket, params.PaymentMethodID);
    return result;
}

/**
 * Perform a service call to Clik2pay which creates a new Payment Request.
 * Return the URL to the external payment page and update the order and payment transaction.
 *
 * @param {Object} params Order and payment instrument
 * @returns {processorAuthorizationResult} Error flag and redirect information
 */
function authorize(params) {
    var processorHelpers = require('*/cartridge/scripts/helpers/clik2pay/processorHelpers');

    var result = processorHelpers.authorize(params.Order, params.PaymentInstrument);
    return result;
}

module.exports = {
    Handle: handle,
    Authorize: authorize
};
