'use strict';

/* global request */

/**
 * Helper functions to call the storefront's checkout helpers or models.
 */

var controllerCartridge = require('~/package.json').controllerCartridge;
var app = require('/' + controllerCartridge + '/cartridge/scripts/app');

/**
 * Continue to place an order.
 *
 * @param {dw.order.Order} order Order
 * @returns {Object} Success flag, error information
 */
function continuePlaceOrder(order) {
    var Order = app.getModel('Order');

    var orderPlacementStatus = Order.submit(order);

    if (orderPlacementStatus.error) {
        /** @type {dw.system.Status} */
        var status = orderPlacementStatus.PlaceOrderError;
        return {
            isSuccess: false,
            customerErrorMessage: status.message
        };
    }

    var confirmationUrlResult = module.exports.getOrderConfirmationUrl(order);
    return confirmationUrlResult;
}

/**
 * Get the URL to display the given order's confirmation page.
 *
 * @param {dw.order.Order} order Order
 * @returns {Object} Redirect URL and HTTP method
 */
function getOrderConfirmationUrl(order) {
    var URLUtils = require('dw/web/URLUtils');

    var result = {
        isSuccess: true,
        redirectUrl: URLUtils.url('Clik2pay-ShowOrderConfirmation', 'orderNo', order.orderNo, 'orderToken', order.orderToken).toString(),
        redirectMethod: 'GET',
        orderNo: order.orderNo,
        orderToken: order.orderToken
    };
    return result;
}

module.exports = {
    continuePlaceOrder: continuePlaceOrder,
    getOrderConfirmationUrl: getOrderConfirmationUrl
};

