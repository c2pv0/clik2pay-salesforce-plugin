'use strict';

/* global request: readonly, response: readonly, customer: readonly */


var controllerCartridge = require('~/package.json').controllerCartridge;
var guard = require('/' + controllerCartridge + '/cartridge/scripts/guard');
var app = require('/' + controllerCartridge + '/cartridge/scripts/app');

/**
 * The Clik2pay payment processor redirects to this endpoint.
 * This renders the Clik2pay button.
 * Clicking the button opens the external payment page and starts polling the Clik2pay-TryContinuePlaceOrder endpoint.
 */
function showPaymentPage() {
    var OrderMgr = require('dw/order/OrderMgr');
    var URLUtils = require('dw/web/URLUtils');
    var paymentInstrumentHelpers = require('*/cartridge/scripts/helpers/clik2pay/paymentInstrumentHelpers');

    var orderNo = String(request.httpParameterMap.orderNo.stringValue);
    var orderToken = String(request.httpParameterMap.orderToken.stringValue);

    var order = OrderMgr.getOrder(orderNo, orderToken);
    var paymentInstrument = paymentInstrumentHelpers.findClik2payPaymentInstrument(order);

    app.getView({
        Order: order,
        clik2pay: {
            paymentPageUrl: paymentInstrument.paymentTransaction.custom.c2pPaymentRequestPaymentLink,
            continueUrl: URLUtils.url('Clik2pay-TryContinuePlaceOrder', 'orderNo', order.orderNo, 'orderToken', order.orderToken).toString()
        }
    }).render('checkout/clik2payPayment');
}

/**
 * This endpoint is called by Clik2pay with status updates about Payment Requests.
 */
function notify() {
    var Logger = require('dw/system/Logger');
    var System = require('dw/system/System');
    var responseUtil = require('*/cartridge/scripts/util/Response');
    var notifyHelpers = require('*/cartridge/scripts/helpers/clik2pay/notifyHelpers');
    var log = Logger.getLogger('clik2pay.Notify');

    var result;
    try {
        var authorizationHeader = request.httpHeaders.get('authorization');
        var requestBody = request.httpParameterMap.requestBodyAsString;

        // allow integration tests to skip authentication
        var skipAuth = System.instanceType !== System.PRODUCTION_SYSTEM && request.httpParameterMap.isParameterSubmitted('skipauth');

        result = notifyHelpers.handleNotificationRequest(authorizationHeader, requestBody, skipAuth);
    } catch (exception) {
        log.error(JSON.stringify(exception, null, 2));
        var errorMessage = System.instanceType === System.PRODUCTION_SYSTEM
            ? 'Exception occurred' : JSON.stringify(exception, null, 2).split('\n');
        result = {
            isSuccess: false,
            errorMessage: errorMessage
        };
    }

    responseUtil.renderJSON({
        isSuccess: result.isSuccess,
        errorMessage: result.errorMessage,
        paymentTransactionStatus: result.paymentTransactionStatus
    });
    if (!result.isSuccess) {
        response.setStatus(result.errorStatusCode || 500);
    }
}


/**
 * This endpoint gets polled from the checkout summary page.
 * It checks whether the Clik2pay-Notify has received a payment status update and attempts to place the order.
 * If no notification was received then this endpoint's response indicates that the endpoint needs to get polled again.
 */
function tryContinuePlaceOrder() {
    var Logger = require('dw/system/Logger');
    var responseUtil = require('*/cartridge/scripts/util/Response');
    var placeOrderHelpers = require('*/cartridge/scripts/helpers/clik2pay/placeOrderHelpers');
    var log = Logger.getLogger('clik2pay.TryContinuePlaceOrder');

    var orderNo = String(request.httpParameterMap.orderNo.stringValue);
    var orderToken = String(request.httpParameterMap.orderToken.stringValue);

    var result;
    try {
        result = placeOrderHelpers.tryContinuePlaceOrder(orderNo, orderToken);
    } catch (exception) {
        log.error('Exception: ' + JSON.stringify(exception, null, 2));
        result = {
            isSuccess: false,
            errorMessage: 'Exception occurred'
        };
    }

    responseUtil.renderJSON(result);
}

/**
 * Display the order confirmation page.
 *
 * SiteGenesis does not expose this as an endpoint.
 */
function showOrderConfirmation() {
    var OrderMgr = require('dw/order/OrderMgr');

    var orderNo = String(request.httpParameterMap.orderNo.stringValue);
    var orderToken = String(request.httpParameterMap.orderToken.stringValue);

    var order = OrderMgr.getOrder(orderNo, orderToken);

    // eslint-disable-next-line new-cap
    app.getController('COSummary').ShowConfirmation(order);
}

module.exports = {
    ShowPaymentPage: guard.ensure(['https', 'get'], showPaymentPage),
    Notify: guard.ensure(['https', 'post'], notify),
    TryContinuePlaceOrder: guard.ensure(['https', 'get'], tryContinuePlaceOrder),
    ShowOrderConfirmation: guard.ensure(['https', 'get'], showOrderConfirmation)
};
