'use strict';

/* global request: readonly */

var System = require('dw/system/System');
var controllerCartridge = require('~/package.json').controllerCartridge;
var guard = require('/' + controllerCartridge + '/cartridge/scripts/guard');

/**
 * Test the authentication service call.
 */
function testAuthentication() {
    var responseUtil = require('*/cartridge/scripts/util/Response');
    var clik2PayServiceAPI = require('*/cartridge/scripts/payment/clik2pay/serviceAPI');

    var authenticationResult = clik2PayServiceAPI.authenticate();

    responseUtil.renderJSON(authenticationResult);
}

/**
 * Test the Create Payment Request service call.
 */
function testCreatePaymentRequest() {
    var responseUtil = require('*/cartridge/scripts/util/Response');
    var clik2PayServiceAPI = require('*/cartridge/scripts/payment/clik2pay/serviceAPI');
    var testHelpers = require('*/cartridge/scripts/helpers/clik2pay/testHelpers');

    var createPaymentRequestResult = clik2PayServiceAPI.createPaymentRequest(testHelpers.testOrder);

    responseUtil.renderJSON(createPaymentRequestResult);
}

/**
 * Create a test order to be used for integration tests.
 */
function createTestOrder() {
    var Logger = require('dw/system/Logger');
    var responseUtil = require('*/cartridge/scripts/util/Response');
    var testHelpers = require('*/cartridge/scripts/helpers/clik2pay/testHelpers');

    var log = Logger.getLogger('clik2pay.CreateTestOrder');
    var result;

    try {
        var simulatePaymentSuccess = request.httpParameterMap.isParameterSubmitted('simulatePaymentSuccess');
        result = testHelpers.createTestOrder(simulatePaymentSuccess);
    } catch (exception) {
        var errorMessage = JSON.stringify(exception, null, 2);
        log.error(errorMessage);
        result = {
            isSuccess: false,
            errorMessage: errorMessage.split('\n')
        };
    }

    responseUtil.renderJSON(result);
}

if (System.instanceType !== System.PRODUCTION_SYSTEM) {
    module.exports = {
        TestAuthentication: guard.ensure(['https', 'get'], testAuthentication),
        TestCreatePaymentRequest: guard.ensure(['https', 'get'], testCreatePaymentRequest),
        CreateTestOrder: guard.ensure(['https', 'get'], createTestOrder)
    };
}
