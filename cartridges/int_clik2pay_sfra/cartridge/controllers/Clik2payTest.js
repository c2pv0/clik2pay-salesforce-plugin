'use strict';

var server = require('server');
var system = require('*/cartridge/scripts/middleware/system');

/**
 * Test the authentication service call.
 */
server.get('TestAuthentication', system.nonProduction, server.middleware.https, function (req, res, next) {
    var clik2PayServiceAPI = require('*/cartridge/scripts/payment/clik2pay/serviceAPI');

    var authenticationResult = clik2PayServiceAPI.authenticate();

    res.json(authenticationResult);
    return next();
});

/**
 * Test the Create Payment Request service call.
 */
server.get('TestCreatePaymentRequest', system.nonProduction, server.middleware.https, function (req, res, next) {
    var clik2PayServiceAPI = require('*/cartridge/scripts/payment/clik2pay/serviceAPI');
    var testHelpers = require('*/cartridge/scripts/helpers/clik2pay/testHelpers');

    var createPaymentRequestResult = clik2PayServiceAPI.createPaymentRequest(testHelpers.testOrder);

    res.json(createPaymentRequestResult);
    return next();
});

/**
 * Create a test order to be used for integration tests.
 */
server.get('CreateTestOrder', system.nonProduction, server.middleware.https, function (req, res, next) {
    var Logger = require('dw/system/Logger');
    var testHelpers = require('*/cartridge/scripts/helpers/clik2pay/testHelpers');
    var log = Logger.getLogger('clik2pay.CreateTestOrder');

    var result;
    try {
        var simulatePaymentSuccess = req.httpParameterMap.isParameterSubmitted('simulatePaymentSuccess');
        result = testHelpers.createTestOrder(simulatePaymentSuccess);
    } catch (exception) {
        var errorMessage = JSON.stringify(exception, null, 2);
        log.error(errorMessage);
        result = {
            isSuccess: false,
            errorMessage: errorMessage.split('\n')
        };
    }

    res.json(result);
    return next();
});

module.exports = server.exports();
