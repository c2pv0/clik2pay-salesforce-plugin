'use strict';

/* global request: readonly */

var server = require('server');

/**
 * The Clik2pay payment processor redirects to this endpoint.
 * This renders the Clik2pay button.
 * Clicking the button opens the external payment page and starts polling the Clik2pay-TryContinuePlaceOrder endpoint.
 */
server.post('ShowPaymentPage', server.middleware.https, function (req, res, next) {
    var OrderMgr = require('dw/order/OrderMgr');
    var URLUtils = require('dw/web/URLUtils');
    var AccountModel = require('*/cartridge/models/account');
    var OrderModel = require('*/cartridge/models/order');
    var paymentInstrumentHelpers = require('*/cartridge/scripts/helpers/clik2pay/paymentInstrumentHelpers');

    var accountModel = new AccountModel(req.currentCustomer);

    var orderNo = req.querystring.orderNo;
    var orderToken = req.querystring.orderToken;
    var order = OrderMgr.getOrder(orderNo, orderToken);
    var orderModel = new OrderModel(
        order,
        {
            customer: req.currentCustomer.raw,
            countryCode: (order.billingAddress || order.defaultShipment.shippingAddress).countryCode.value,
            usingMultiShipping: req.session.privacyCache.get('usingMultiShipping'),
            shippable: true,
            containerView: 'basket'
        }
    );

    var paymentInstrument = paymentInstrumentHelpers.findClik2payPaymentInstrument(order);

    res.render('checkout/clik2payPayment', {
        order: orderModel,
        customer: accountModel,
        clik2pay: {
            paymentPageUrl: paymentInstrument.paymentTransaction.custom.c2pPaymentRequestPaymentLink,
            continueUrl: URLUtils.url('Clik2pay-TryContinuePlaceOrder', 'orderNo', order.orderNo, 'orderToken', order.orderToken).toString()
        }
    });
    return next();
});

/**
 * This endpoint is called by Clik2pay with status updates about Payment Requests.
 */
server.post('Notify', server.middleware.https, function (req, res, next) {
    var Logger = require('dw/system/Logger');
    var System = require('dw/system/System');
    var notifyHelpers = require('*/cartridge/scripts/helpers/clik2pay/notifyHelpers');
    var log = Logger.getLogger('clik2pay.Notify');

    var result;
    try {
        var authorizationHeader = req.httpHeaders.get('authorization');
        var requestBody = req.httpParameterMap.requestBodyAsString;

        // allow integration tests to skip authentication
        var skipAuth = System.instanceType !== System.PRODUCTION_SYSTEM && req.httpParameterMap.isParameterSubmitted('skipauth');

        result = notifyHelpers.handleNotificationRequest(authorizationHeader, requestBody, skipAuth);
    } catch (exception) {
        log.error(JSON.stringify(exception, null, 2));
        var errorMessage = System.instanceType === System.PRODUCTION_SYSTEM
            ? 'Exception occurred' : JSON.stringify(exception, null, 2).split('\n');
        result = {
            isSuccess: false,
            errorMessage: errorMessage
        };
    }

    res.json({
        isSuccess: result.isSuccess,
        errorMessage: result.errorMessage,
        paymentTransactionStatus: result.paymentTransactionStatus
    });
    if (!result.isSuccess) {
        res.setStatusCode(result.errorStatusCode || 500);
    }
    return next();
});

/**
 * This endpoint gets polled from the checkout summary page.
 * It checks whether the Clik2pay-Notify has received a payment status update and attempts to place the order.
 * If no notification was received then this endpoint's response indicates that the endpoint needs to get polled again.
 */
server.get('TryContinuePlaceOrder', server.middleware.https, function (req, res, next) {
    var Logger = require('dw/system/Logger');
    var placeOrderHelpers = require('*/cartridge/scripts/helpers/clik2pay/placeOrderHelpers');
    var log = Logger.getLogger('clik2pay.TryContinuePlaceOrder');

    var orderNo = String(req.querystring.orderNo);
    var orderToken = String(req.querystring.orderToken);

    var result;
    try {
        result = placeOrderHelpers.tryContinuePlaceOrder(orderNo, orderToken);
    } catch (exception) {
        log.error('Exception: ' + JSON.stringify(exception, null, 2));
        result = {
            isSuccess: false,
            errorMessage: 'Exception occurred'
        };
    }

    res.json(result);
    return next();
});

module.exports = server.exports();
