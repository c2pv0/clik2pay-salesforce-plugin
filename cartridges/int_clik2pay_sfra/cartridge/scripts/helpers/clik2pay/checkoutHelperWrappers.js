'use strict';

/* global request */

/**
 * Helper functions to call the storefront's checkout helpers or models.
 */

/**
 * Continue to place an order.
 *
 * @param {dw.order.Order} order Order
 * @returns {Object} Success flag, error information
 */
function continuePlaceOrder(order) {
    var checkoutHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

    var placeOrderResult = checkoutHelpers.continuePlaceOrder(order, request);

    if (placeOrderResult.error) {
        return {
            isSuccess: false,
            cartError: placeOrderResult.cartError,
            redirectUrl: placeOrderResult.redirectUrl,
            customerErrorMessage: placeOrderResult.errorMessage
        };
    }

    var confirmationUrlResult = module.exports.getOrderConfirmationUrl(order);
    return confirmationUrlResult;
}

/**
 * Get the URL to display the given order's confirmation page.
 *
 * @param {dw.order.Order} order Order
 * @returns {Object} Redirect URL and HTTP method
 */
function getOrderConfirmationUrl(order) {
    var URLUtils = require('dw/web/URLUtils');

    var result = {
        isSuccess: true,
        redirectUrl: URLUtils.url('Order-Confirm').toString(),
        redirectMethod: 'POST',
        orderNo: order.orderNo,
        orderToken: order.orderToken
    };
    return result;
}

module.exports = {
    continuePlaceOrder: continuePlaceOrder,
    getOrderConfirmationUrl: getOrderConfirmationUrl
};
