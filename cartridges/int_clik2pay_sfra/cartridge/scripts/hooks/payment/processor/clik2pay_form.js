'use strict';


/**
 * Mandatory payment processor hook that verifies payment form input data.
 *
 * Clik2pay requires no input data so this hook function only returns the selected payment method.
 *
 * @param {Object} req Request object
 * @param {Object} paymentForm Payment form
 * @param {Object} viewFormData Object containing billing form data
 * @returns {Object} Error or payment information
 */
function processForm(req, paymentForm, viewFormData) {
    var viewData = viewFormData;

    viewData.paymentMethod = {
        value: paymentForm.paymentMethod.value,
        htmlName: paymentForm.paymentMethod.value
    };

    return {
        error: false,
        viewData: viewData
    };
}

/**
 * Mandatory payment processor hook.
 *
 * Empty because for Clik2pay there is no payment information to save.
 */
function savePaymentInformation() {
    return;
}

module.exports = {
    processForm: processForm,
    savePaymentInformation: savePaymentInformation
};
