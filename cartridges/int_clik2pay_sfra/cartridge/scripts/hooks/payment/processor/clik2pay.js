'use strict';

/*
 * See module processorHelpers for more information on the checkout flow.
 */


/**
 * Remove all current payment instruments from the basket and
 * then create a Clik2pay payment instrument.
 *
 * @param {dw.order.Basket} basket Basket
 * @param {Object} paymentInformation Data from payment method input form (not used with Clik2pay)
 * @param {string} paymentMethodID Payment method ID
 * @returns {Object} Success or error flag
 */
function handle(basket, paymentInformation, paymentMethodID) {
    var processorHelpers = require('*/cartridge/scripts/helpers/clik2pay/processorHelpers');

    var result = processorHelpers.handle(basket, paymentMethodID);
    return result;
}

/**
 * Perform a service call to Clik2pay which creates a new Payment Request.
 * Return the URL to the external payment page and update the order and payment transaction.
 *
 * @param {string} orderNo Order number
 * @param {dw.order.OrderPaymentInstrument} paymentInstrument The Clik2pay payment instrument
 * @param {dw.order.PaymentProcessor} paymentProcessor Payment processor
 * @param {dw.order.Order} order Order
 * @returns {processorAuthorizationResult} Error flag and redirect information
 */
function authorize(orderNo, paymentInstrument, paymentProcessor, order) {
    var processorHelpers = require('*/cartridge/scripts/helpers/clik2pay/processorHelpers');

    var result = processorHelpers.authorize(order, paymentInstrument);
    return result;
}

module.exports = {
    Handle: handle,
    Authorize: authorize
};
