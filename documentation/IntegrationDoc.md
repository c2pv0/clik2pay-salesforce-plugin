# int_clik2pay: Integration Documentation

### Cartridge version 1.0.0


# Table of Contents

1. [Summary](#markdown-header-summary)
1. [Overview](#markdown-header-overview)
    1. [Checkout Flow](#markdown-header-checkout-flow)
    1. [Functional Overview](#markdown-header-functional-overview)
    1. [Use Cases](#markdown-header-use-cases)
    1. [Limitations and Constraints](#markdown-header-limitations-and-constraints)
    1. [Compatibility](#markdown-header-compatibility)
    1. [Privacy, Payment](#markdown-header-privacy-payment)
1. [Checkout Flow: Details](#markdown-header-checkout-flow-details)
1. [Implementation Guide](#markdown-header-implementation-guide)
    1. [Setup](#markdown-header-setup)
    1. [Configuration](#markdown-header-configuration)
    1. [Custom Code](#markdown-header-custom-code)
    1. [External Interfaces](#markdown-header-external-interfaces)
1. [Testing](#markdown-header-testing)
    1. [Automated Tests](#markdown-header-automated-tests)
    1. [Manual Tests](#markdown-header-manual-tests)
1. [Operations and Maintenance](#markdown-header-operations-and-maintenance)
    1. [Data Storage](#markdown-header-data-storage)
    1. [Availability](#markdown-header-availability)
    1. [Support](#markdown-header-support)
1. [User Guide](#markdown-header-user-guide)
    1. [Roles and Responsibilities](#markdown-header-roles-and-responsibilities)
    1. [Business Manager](#markdown-header-business-manager)
    1. [Storefront Functionality](#markdown-header-storefront-functionality)
1. [Known Issues](#markdown-header-known-issues)
1. [Release History](#markdown-header-release-history)


# Summary

This cartridge allows merchants to utilize clik2pay to enable their customers with Canadian bank accounts to pay via Interac. Interac is a Canadian interbank network that serves as the Canadian debit card system.
Clik2pay uses the Interac system to offer fast, easy and secure payments directly from customer's bank accounts without the need for the customers to sign-up at clik2pay.

This cartridge implements the service communication with clik2pay to create payment requests via the clik2pay API and to display an external payment page in a popup window.

Because Interac doesn't offer an automatic redirect of the customer back to the merchant store, a change in the checkout flow is needed and documented below.

The merchant needs a contract with clik2pay to use this cartridge.


# Overview

## Checkout Flow

When the customer selects the clik2pay payment method then the default checkout flow is changed to this:

Between the summary and the confirmation page, an additional step is inserted. This step shows the clik2pay button which opens the external payment page in a popup window. Once the payment is completed, the customer closes the popup window and is automatically redirected to the order confirmation page.


## Functional Overview

This cartridge implements

* a payment processor to provide the payment method clik2pay
* authentication of the service communication with clik2pay
* creation of a payment request for payments via Interac at clik2pay
* a webhook listener to receive status updates from clik2pay about pending payment requests
* a custom cache to hold the authorization tokens used for API calls to clik2pay
* a checkout flow customization for the place order process


## Use Cases

This cartridge covers the necessary processes to place an order using the Clik2pay via Interac payment method.

For a detailed use case see [Storefront Functionality](#markdown-header-storefront-functionality).


## Limitations and Constraints

### Split Payment

The integration cartridge does not support split payment. When clik2pay is selected then any existing payment instruments are removed from the basket.


### Payment Request Status service

The Payment Request Status service call is not part of the integration cartridge as it is intended for more exceptional use cases.

Status notifications are received using a webhook listener, implemented by the controller endpoint `Clik2pay-Notify`.


## Compatibility

The cartridge is compatible to

* Salesforce B2C Commerce API (version 22.6)
* SFRA (version 6.0.0) and
* SiteGenesis (version 105.2.0).

SiteGenesis with controllers is supported. Pipeline support requires custom code adjustments which are not covered by this documentation.


## Privacy, Payment

The customer's full name, email and phone number are shared with clik2pay.

Clik2pay shall comply with all laws and regulations relating to the protection and privacy of the personal information collected, and such information shall not be used for any marketing, preference tracking or other purposes not directly related to the performance of its obligations.


# Checkout Flow: Details

![Clik2pay Checkout Flow](./images/checkout-flow.png)


## Create order

When submitting the summary page, the SFCC server creates an order and triggers payment integrations.

The clik2pay integration performs a Create Payment Request service call and fetches the payment page URL.


## Display button

The endpoint `Clik2pay-ShowPaymentPage` displays a clik2pay button and an informational message that the customer needs to click the button to proceed with payment.


## Popup and polling

Clicking the clik2pay button opens the external payment page in a popup and starts polling the endpoint `Clik2pay-TryContinuePlaceOrder` for updates on the payment status.

Also, the displayed informational message is updated. It now offers the customer a link to click in case the popup did not open.


## Webhook: receive payment status

The endpoint `Clik2pay-Notify` receives notification events from clik2pay. If payment was successful then the SFCC payment transaction is marked as paid.


## Place or fail order

On the payment page, the endpoint `Clik2pay-TryContinuePlaceOrder` is polled to check the payment status:

If the SFCC payment transaction is marked as paid then the order is placed and the customer is redirected to the order confirmation page.

If payment failed or polling timed out then the order is failed and an error message is displayed.


# Implementation Guide

The core logic of the clik2pay integration resides in cartridge `int_clik2pay`. Depending on the system you want to integrate the payment cartridge into, you'll find the customization for SFRA in the `int_clik2pay_sfra` cartridge and customizations for SiteGenesis within the `int_clik2pay_sg` cartridge.

Sections without an "SFRA:" or "SiteGenesis:" prefix are common steps that should be applied in both cases.

The steps that are specific to either integrating with SFRA or SiteGenesis are documented in theses sections:

* Configuration: [SFRA](#markdown-header-configuration-sfra) and [SiteGenesis](#markdown-header-configuration-sitegenesis)
* Custom Code: [SFRA](#markdown-header-custom-code-sfra) and [SiteGenesis](#markdown-header-custom-code-sitegenesis)

## Setup

### Create Payment Processor

Before you can start importing the metadata you have to create the payment processor for clik2pay:

1. In Business Manager, navigate to *SiteID > Merchant Tools > Ordering > Payment Processors*
2. Click on the *New* button to create a new Payment Processor
3. Enter as ID: `CLIK2PAY` and as Description: `Clik2pay via Interac`
4. Click the Apply button

See also the example import file `metadata/example/payment-processors.xml`.


### Import Metadata

Find the `metadata` folder within this repository and import these files using Business Manager:

* Import `metadata/payment-methods.xml` under *SiteID > Merchant Tools > Ordering > Import & Export*.
* Import `metadata/services.xml` under *Administration > Operations > Import & Export*.
* Import `metadata/system-objecttype-extensions.xml` under *Administration > Site Development > Import & Export*.


## Configuration

### Configure Payment Method

Activate the payment method: In Business Manager under *SiteID > Merchant Tools > Ordering > Payment Methods* select *C2P_VIA_INTERAC* and set the *Enabled* flag to *true*.


### Configure Site Preferences

Under *SiteID > Merchant Tools > Site Preferences > Custom Preferences* select the site preference group *Clik2pay* and configure these site preferences if needed (see descriptions below this table):

Attribute ID                           |  Name                                           | Type
-------------------------------------- | ----------------------------------------------- | ------
c2pMerchantID                          | Merchant ID                                     | String
c2pCustomerNotificationEmailTemplateID | Customer notification: Email template ID        | String
c2pPaymentRequestDisplayMessage        | Payment request: Display message                | String
c2pPaymentStatusPollingInterval        | Payment status polling interval (milliseconds)  | Integer
c2pPaymentStatusPollingTimeout         | Payment status polling timeout (milliseconds)   | Integer


#### Merchant ID

The unique identifier of the merchant for the payment request. Will default to the merchant the user caller is associated with. Required when the caller is associated with more than one merchant.


#### Display message

The network message displayed to the payer, within the popup as they review the payment request. If not set, a default message will be displayed. (This default message is set by clik2Pay.)


#### Payment status polling interval (milliseconds)

Once the external payment page is displayed, the client-side code starts polling the endpoint `Clik2pay-TryContinuePlaceOrder`. This preference configures the wait time between polling requests. (This defaults to 1,000 milliseconds.)


#### Payment status polling timeout (milliseconds)

Once the external payment page is displayed, the client-side code starts polling the endpoint
`Clik2pay-TryContinuePlaceOrder`. This preference configures the wait time after which the payment request is assumed to have timed out and the order is failed. (This defaults to 60,000 milliseconds.)


### Pipeline URLs

The clik2pay backend UI does not support certain dots in URLs (specifically the dot in `demandware.store`).
As a workaround you need to configure an alias for the webhook listener endpoint:

In Business Manager under *SiteID > Merchant Tools > SEO > URL Rules > Pipeline URLs* add the alias `c2pnotify` for endpoint `Clik2pay-Notify`.

The resulting webhook URL is then something like this:

`https://realm-000.sandbox.us01.dx.commercecloud.salesforce.com/s/RefArch/c2pnotify`

On production the vanity hostname, e.g. `www.example.com`, must be used. This is because Salesforce is restricting external sources from accessing a POD using the `demandware.net` hostname without going through the eCDN.

An example import file can be found in `metadata/example/url-rules.xml`.


### Configure Services

After importing the service definitions (see [Import Metadata](#markdown-header-import-metadata)) the services need to be configured.


#### Service: Get Authentication Token

Select the appropriate service profile (either sandbox or production).

Authentication service credentials are selected automatically at run-time, see [Configure Service Credentials](#markdown-header-configure-service-credentials).

Parameter                   | Value
--------------------------- | --------------------------
Name                        | `int_clik2pay.httpForm.clik2pay.get_auth_token`
Type                        | HTTP Form
Enabled                     | yes
Service Mode                | Live
Log Name Prefix             | `clik2pay`
Communication Log Enabled   | no
Profile                     | `clik2pay.authentication.sandbox` or `clik2pay.authentication.production`
Credentials                 | (empty, selected at run-time)

The authentication credentials are also used by the `Clik2pay-Notify` endpoint to authenticate incoming requests. The minimum length of both user name and password is 10 characters each. If this requirement is not met then the `Clik2pay-Notify` endpoint will not process the notification and log an error instead.


#### Service: Create Payment Request

Select the appropriate service profile and credentials (either sandbox or production).

Parameter                   | Value
--------------------------- | --------------------------
Name                        | `int_clik2pay.http.clik2pay.create_payment_request`
Type                        | HTTP
Enabled                     | yes
Service Mode                | Live
Log Name Prefix             | `clik2pay`
Communication Log Enabled   | *no*
Profile                     | `clik2pay.api.sandbox` or `clik2pay.api.production`
Credentials                 | `clik2pay.api.sandbox` or `clik2pay.api.production`


#### Configure Service Credentials

Authentication service credentials are selected automatically at run-time, based on the current instance type (Sandbox or Production) and the current site ID (e.g. `SiteGenesis` or `RefArch`).

The credential ID to lookup is constructed like this: `'clik2pay.authentication.' + instanceType + '.' + siteID`

E.g.: `clik2pay.authentication.production.SiteGenesis` or `clik2pay.authentication.sandbox.RefArch`.

Make sure that authentication credentials exist where the credential ID matches your instance type and site ID.


#### Credentials: Authentication

Enter the username and password provided by clik2pay:

Parameter    | Sandbox                                    | Production
------------ | ------------------------------------------ | ---------------------------------------
Name         | `clik2pay.authentication.sandbox.SiteID`   | `clik2pay.authentication.production.SiteID`
URL          | `https://api-auth.sandbox.clik2pay.com/oauth2/token` | `https://api-auth.clik2pay.com/oauth2/token`
User         | *API user name*                            | *API user name*
Password     | *API password*                             | *API password*


#### Credentials: API

Enter the API key provided by clik2pay:

Parameter    | Sandbox                                    | Production
------------ | ------------------------------------------ | ---------------------------------------
Name         | `clik2pay.api.sandbox`                     | `clik2pay.api.production`
URL          | `https://api.sandbox.clik2pay.com/open/v1` | `https://api.clik2pay.com/open/v1`
User         | *(empty)*                                  | *(empty)*
Password     | *API key*                                  | *API key*


### Country, Locale, and Currency

The Clik2pay payment method only supports the currency CAD.
Follow the steps below to enable that currency, set up Canada as an order address country, and configure a suitable locale.

#### Order Address Country

Go to *Administration > Site Development > System Object Definitions > OrderAddress > Attribute Definitions* and select the attribute `countryCode`. Under *Search Attribute Value Definitions* add a new entry with value `CA` and display value `Canada`.


#### Locale

Enable Canadian locale for your site: Go to *SiteID > Merchant Tools > Site Preferences > Locales* and enable `en_CA`.


#### Allowed Site Currency

To enable CAD currency open Business Manager and follow these steps:

* Go to *SiteID > Merchant Tools > Site Preferences > Currencies*.
* Click the *Add* button, select `CAD Canadian Dollar` and click *Add*.


#### Shipping method for CAD

In Business Manager go to *SiteID > Merchant Tools > Ordering > Shipping Methods*.

* Create a new shipping method for *Currency* `Canadian Dollar (CAD)`
* Use a suitable *Tax Class*
* Use a suitable *Shipment Cost*


### Configuration: SFRA

#### Cartridge Path

In Business Manager adjust the cartridge path under *Administration > Sites > Manage Sites > SiteID > Settings > Cartridges* to include `int_clik2pay_sfra:int_clik2pay`, e.g. like this:

`int_clik2pay_sfra:int_clik2pay:app_storefront_base`


#### Price Books

The payment method *Clik2pay via Interac* only supports the currency CAD. To be able to use this payment method price books with CAD currency are required. The following steps use the existing USD price books from SFRA to create and import CAD price books.


##### Export USD price books

* In Business Manager go to *SiteID > Merchant Tools > Products and Catalogs > Import & Export*.
* In the *Price Books* section click on the *Export* button.
* Select the price books `usd-m-list-prices` and `usd-m-sale-prices` and click on the *Next* button.
* Select *Export Selected Only* and enter an export file name, e.g. `usd-m-price-books`, then click the *Export* button.
* Go back to *SiteID > Merchant Tools > Products and Catalogs > Import & Export* and wait for the export to complete.
* When exporting is completed then download the export file.


##### Create CAD price books

* Open the downloaded file `usd-m-price-books.xml` in a text editor.
* Replace all occurrences of the text `usd-m-list-prices` with `cad-m-list-prices`.
* Replace all occurrences of the text `usd-m-sale-prices` with `cad-m-sale-prices`.
* Replace all occurrences of the text `<currency>USD</currency>` with `<currency>CAD</currency>`.
* Save the file as `cad-m-price-books.xml`.


##### Import CAD price books

* In Business Manager go to *SiteID > Merchant Tools > Products and Catalogs > Import & Export*.
* Click the *Upload* button, browse for and select `cad-m-price-books.xml`, then click on *Upload*.
* When the upload is finished, click the *Back* link and import the uploaded file.


##### Assign CAD price books to the site

* In Business Manager go to *SiteID > Merchant Tools > Products and Catalogs > Price Books*
* In the list of price books click on `cad-m-list-prices`.
* Select tab *Site Assignments*, check *RefArch*, then click on *Apply*.
* Click on *Back to list*.
* In the list of price books click on `cad-m-sale-prices`.
* Select tab *Site Assignments*, check *RefArch*, then click on *Apply*.


#### SFRA Custom Preferences

* In Business Manager go to *SiteID > Merchant Tools > Site Preferences > Custom Preferences*
* Select the group *Storefront Configs*
* Set *Default List Price Book ID* to `cad-m-list-prices`
* Set *Default Country Code* to `CA`
* Click *Save*


### Configuration: SiteGenesis

#### Cartridge Path

In Business Manager adjust the cartridge path under *Administration > Sites > Manage Sites > SiteID > Settings > Cartridges* to include `int_clik2pay_sg:int_clik2pay`, e.g. like this:

`int_clik2pay_sg:int_clik2pay:app_storefront_controllers:app_storefront_core`


#### Price Books

The payment method *Clik2pay via Interac* only supports the currency CAD. To be able to use this payment method price books with CAD currency are required. The following steps use the existing USD price books from SiteGenesis to create and import CAD price books.


##### Export USD price books

* In Business Manager go to *SiteID > Merchant Tools > Products and Catalogs > Import & Export*.
* In the *Price Books* section click on the *Export* button.
* Select the price books `usd-list-prices` and `usd-sale-prices` and click on the *Next* button.
* Select *Export Selected Only* and enter an export file name, e.g. `usd-price-books`, then click the *Export* button.
* Go back to *SiteID > Merchant Tools > Products and Catalogs > Import & Export* and wait for the export to complete.
* When exporting is completed then download the export file.


##### Create CAD price books

* Open the downloaded file `usd-price-books.xml` in a text editor.
* Replace all occurrences of the text `usd-list-prices` with `cad-list-prices`.
* Replace all occurrences of the text `usd-sale-prices` with `cad-sale-prices`.
* Replace all occurrences of the text `<currency>USD</currency>` with `<currency>CAD</currency>`.
* Save the file as `cad-price-books.xml`.


##### Import CAD price books

* In Business Manager go to *SiteID > Merchant Tools > Products and Catalogs > Import & Export*.
* Click the *Upload* button, browse for and select `cad-price-books.xml`, then click on *Upload*.
* When the upload is finished, click the *Back* link and import the uploaded file.


##### Assign CAD price books to site

* In Business Manager go to *SiteID > Merchant Tools > Products and Catalogs > Price Books*
* In the list of price books click on `cad-list-prices`.
* Select tab *Site Assignments*, check *SiteGenesis*, then click on *Apply*.
* Click on *Back to list*.
* In the list of price books click on `cad-sale-prices`.
* Select tab *Site Assignments*, check *SiteGenesis*, then click on *Apply*.


#### SiteGenesis Custom Preferences

* In Business Manager go to *SiteID > Merchant Tools > Site Preferences > Custom Preferences*
* Select the group *Storefront Configs*
* Set *Default List Price Book ID* to `cad-list-prices`
* Set *Default Country Code* to `CA`
* Click *Save*


---

## Custom Code

### Adjust dw.json

Copy the file `dw.json.example` to `dw.json` and adjust it as necessary.

The cartridge array should mention all cartridges that you want to upload to the instance. E.g. if you want to upload both SFRA and SiteGenesis as well as the respective clik2pay cartridges then the `dw.json` file should look like this:

```json
{
    "hostname": "bla-001.sandbox.us01.dx.commercecloud.salesforce.com",
    "username": "user@example.com",
    "password": "1234",

    "cartridge": [
        "int_clik2pay",

        "modules",
        "app_storefront_base",
        "bm_app_storefront_base",
        "int_clik2pay_sfra",

        "app_storefront_controllers",
        "app_storefront_core",
        "int_clik2pay_sg"
    ],

    "code-version": "version1",
    "site-id": "RefArch"
}
```

Note that `site-id` is only used by the [Integration Tests](#markdown-header-integration-tests).


### Custom Code: SFRA

To support asynchronous payment the SFRA code needs to be adjusted. This integration **replaces** the endpoint `CheckoutServices-PlaceOrder` and some of the functions exported by the module `checkoutHelpers.js`.

If you have made modifications to those parts of the SFRA code, then you may want to keep your files and only integrate the relevant changes into your code base. The changes can be found by searching in these files for `clik2pay`:

* `int_clik2pay_sfra/cartridge/controllers/CheckoutServices.js`
* `int_clik2pay_sfra/cartridge/scripts/checkout/checkoutHelpers.js`


#### Country and Locale

##### Forms

To set up the forms you can use the example forms provided in the cartridge folders:
Copy the form definitions contained in `cartridges/int_clik2pay_sfra/example/forms/en_CA` to `app_storefront_base/cartridge/forms/en_CA`. This will set up the forms in such a way that e.g. for the locale `en_CA` the country input has one valid option value `CA`.

To display the Canadian country and state names you can use provided example resource messages:
Copy `int_clik2pay_sfra/example/templates/resources/forms_en_CA.properties` to `app_storefront_base/cartridge/templates/resources`.


##### Locale

To enable Canada in the storefront's locale selector you need to add the locale definition for Canada. This is done by adding a new top-level entry in `app_storefront_base/cartridge/config/countries.json`. You can replace the above file with the example file provided here: `int_clik2pay_sfra/example/countries.json`.


#### Display Clik2pay Logo

The client-side JavaScript code of SFRA only handles credit cards when displaying payment instrument information. To display the clik2pay payment method name and logo the following needs to be adjusted.


##### billing.js

In `app_storefront_base/cartridge/client/default/js/checkout/billing.js` change function `updatePaymentInformation` to this:

```js
function updatePaymentInformation(order) {
    // update payment details
    var $paymentSummary = $('.payment-details');
    var htmlToAppend = '';

    if (order.billing.payment && order.billing.payment.selectedPaymentInstruments
        && order.billing.payment.selectedPaymentInstruments.length > 0) {
        var paymentInstrument = order.billing.payment.selectedPaymentInstruments[0]; // clik2pay
        if (paymentInstrument.paymentMethod === 'CREDIT_CARD') { // clik2pay
            htmlToAppend += '<span>' + order.resources.cardType + ' '
                + order.billing.payment.selectedPaymentInstruments[0].type
                + '</span><div>'
                + order.billing.payment.selectedPaymentInstruments[0].maskedCreditCardNumber
                + '</div><div><span>'
                + order.resources.cardEnding + ' '
                + order.billing.payment.selectedPaymentInstruments[0].expirationMonth
                + '/' + order.billing.payment.selectedPaymentInstruments[0].expirationYear
                + '</span></div>';
        } else { // clik2pay
            htmlToAppend += '<img src="' + paymentInstrument.imageUrl + '" style="max-width: 200px;"/>'; // clik2pay
        }
    }

    $paymentSummary.empty().append(htmlToAppend);
}
```

An example of this can be found in `int_clik2pay_sfra/example/client/default/js/checkout/billing.js`.


##### payment.js

In `app_storefront_base/cartridge/models/payment.js` change function `getSelectedPaymentInstruments` to this:

```js
function getSelectedPaymentInstruments(selectedPaymentInstruments) {
    return collections.map(selectedPaymentInstruments, function (paymentInstrument) {
        var results = {
            paymentMethod: paymentInstrument.paymentMethod,
            amount: paymentInstrument.paymentTransaction.amount.value
        };
        if (paymentInstrument.paymentMethod === 'C2P_VIA_INTERAC') { // clik2pay
            var method = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod); // clik2pay
            results.descriptionMarkup = method && method.description ? method.description.markup : ''; // clik2pay
            results.imageUrl = require('dw/web/URLUtils').staticURL('/images/clik2pay.svg').toString(); // clik2pay
        } else if (paymentInstrument.paymentMethod === 'CREDIT_CARD') {
            results.lastFour = paymentInstrument.creditCardNumberLastDigits;
            results.owner = paymentInstrument.creditCardHolder;
            results.expirationYear = paymentInstrument.creditCardExpirationYear;
            results.type = paymentInstrument.creditCardType;
            results.maskedCreditCardNumber = paymentInstrument.maskedCreditCardNumber;
            results.expirationMonth = paymentInstrument.creditCardExpirationMonth;
        } else if (paymentInstrument.paymentMethod === 'GIFT_CERTIFICATE') {
            results.giftCertificateCode = paymentInstrument.giftCertificateCode;
            results.maskedGiftCertificateCode = paymentInstrument.maskedGiftCertificateCode;
        }

        return results;
    });
}
```

An example of this can be found in `int_clik2pay_sfra/example/models/payment.js`.


### Custom Code: SiteGenesis

#### Controller Cartridge

In `cartridges/int_clik2pay_sg/package.json` configure the value of `controllerCartridge` so it matches the name of your controller cartridge, e.g. set it to `app_storefront_controllers`.

This is required so the integration can access the `app.js` module.

Example:

```json
{
    "controllerCartridge": "app_storefront_controllers",
    "hooks": "./hooks.json"
}
```


#### Country and Locale

##### Forms

To set up the forms you can use the example forms provided in the cartridge folders:
Copy the folders contained in `cartridges/int_clik2pay_sg/example/forms/en_CA` to `app_storefront_core/cartridge/forms`. This will setup the forms in such a way that e.g. for the locale `en_CA` the country input has one valid option value `CA`.

Also make sure to copy the resource messages file `cartridges/int_clik2pay_sg/example/templates/resources/forms_en_CA.properties` into `app_storefront_core/cartridge/templates/resources`.


##### Locale

To enable Canada in the storefront's locale selector you need to add the locale definition Canada. This is done by adding new top-level entries in `app_storefront_core/cartridge/countries.json`. You can replace the above file with the example file provided here: `cartridges/int_clik2pay_sg/example/countries.json`.


#### Template paymentmethods.isml

The template `app_storefront_core/cartridge/templates/default/checkout/billing/paymentmethods.isml` needs to be adjusted so it can display the clik2pay payment method.

Replace this code

```html
			<isloop items="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.options}" var="paymentMethodType">

				<iscomment>Ignore GIFT_CERTIFICATE method, GCs are handled separately before other payment methods.</iscomment>
				<isif condition="${paymentMethodType.value.equals(dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE)}"><iscontinue/></isif>

				<div class="form-row label-inline">
					<isset name="radioID" value="${paymentMethodType.value}" scope="page"/>
					<div class="field-wrapper">
						<input id="is-${radioID}" type="radio" class="input-radio" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" value="${paymentMethodType.htmlValue}" <isif condition="${paymentMethodType.value == pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlValue}">checked="checked"</isif> />
					</div>
					<label for="is-${radioID}"><isprint value="${Resource.msg(paymentMethodType.label,'forms',null)}"/></label>
				</div>

			</isloop>
```

with this:

```html
			<isloop items="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.options}" var="paymentMethodType">

				<iscomment>Ignore GIFT_CERTIFICATE method, GCs are handled separately before other payment methods.</iscomment>
				<isif condition="${paymentMethodType.value.equals(dw.order.PaymentInstrument.METHOD_GIFT_CERTIFICATE)}"><iscontinue/></isif>

				<iscomment>
					Clik2pay:
				</iscomment>
				<isif condition="${paymentMethodType.value === 'C2P_VIA_INTERAC'}">
					<isinclude template="checkout/billing/clik2paymethod"/>
				<iselse/>
					<div class="form-row label-inline">
						<isset name="radioID" value="${paymentMethodType.value}" scope="page"/>
						<div class="field-wrapper">
							<input id="is-${radioID}" type="radio" class="input-radio" name="${pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlName}" value="${paymentMethodType.htmlValue}" <isif condition="${paymentMethodType.value == pdict.CurrentForms.billing.paymentMethods.selectedPaymentMethodID.htmlValue}">checked="checked"</isif> />
						</div>
						<label for="is-${radioID}"><isprint value="${Resource.msg(paymentMethodType.label,'forms',null)}"/>
						</label>
					</div>
				</isif>

			</isloop>
```

Near the end of the file remove the payment processor placeholder. Remove this code:

```html
		<iscomment>
			Custom processor
			--------------------------------------------------------------
		</iscomment>

		<div class="payment-method <isif condition="${!empty(pdict.selectedPaymentID) && pdict.selectedPaymentID=='PayPal'}">payment-method-expanded</isif>" data-method="Custom">
			<!-- Your custom payment method implementation goes here. -->
			${Resource.msg('billing.custompaymentmethod','checkout',null)}
		</div>
```

An example of this can be found in `int_clik2pay_sg/example/templates/default/checkout/billing/paymentmethods.isml`.


#### Controller COPlaceOrder.js

To support redirecting to the page that displays the clik2pay button the controller COPlaceOrder needs to be adjusted.

In `app_storefront_controllers/cartridge/controllers/COPlaceOrder.js` change function `handlePayments` to this:

```js
function handlePayments(order) {

    if (order.getTotalNetPrice().value !== 0.00) {

        var paymentInstruments = order.getPaymentInstruments();

        if (paymentInstruments.length === 0) {
            return {
                missingPaymentInfo: true
            };
        }
        /**
         * Sets the transaction ID for the payment instrument.
         */
        var handlePaymentTransaction = function () {
            paymentInstrument.getPaymentTransaction().setTransactionID(order.getOrderNo());
        };

        for (var i = 0; i < paymentInstruments.length; i++) {
            var paymentInstrument = paymentInstruments[i];

            if (PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor() === null) {

                Transaction.wrap(handlePaymentTransaction);

            } else {

                var authorizationResult = PaymentProcessor.authorize(order, paymentInstrument);

                if (authorizationResult.not_supported || authorizationResult.error) {
                    return {
                        error: true
                    };
                }

                // Clik2pay: added redirect handling
                if (authorizationResult.redirectRequired) {
                    return {
                        redirectRequired: true,
                        redirectUrl: authorizationResult.redirectUrl
                    };
                }
            }
        }
    }

    return {};
}
```

In the same file find function `start` and change this block

```js
    } else if (handlePaymentsResult.missingPaymentInfo) {
        return Transaction.wrap(function () {
            OrderMgr.failOrder(order);
            return {
                error: true,
                PlaceOrderError: new Status(Status.ERROR, 'confirm.error.technical')
            };
        });
    }

    var orderPlacementStatus = Order.submit(order);
    if (!orderPlacementStatus.error) {
        clearForms();
    }
    return orderPlacementStatus;
}
```

to this:

```js
    } else if (handlePaymentsResult.missingPaymentInfo) {
        return Transaction.wrap(function () {
            OrderMgr.failOrder(order);
            return {
                error: true,
                PlaceOrderError: new Status(Status.ERROR, 'confirm.error.technical')
            };
        });
    } else if (handlePaymentsResult.redirectRequired) { // clik2pay
        response.redirect(handlePaymentsResult.redirectUrl); // clik2pay
        return {}; // clik2pay
    }

    var orderPlacementStatus = Order.submit(order);
    if (!orderPlacementStatus.error) {
        clearForms();
    }
    return orderPlacementStatus;
}
```

An example of this can be found in `int_clik2pay_sg/example/controllers/COPlaceOrder.js`.


---

## External Interfaces

This cartridge connects to the external clik2pay service endpoints. The implementation uses the `LocalServiceRegistry` as it is currently the best practice.

The clik2pay API is described in the [clik2pay API Documentation](https://docs.clik2pay.com/docs).


# Testing

## Automated Tests

The cartridge provides coverage with automatic unit and integration tests which can be found in the `test` folder. These tests are run using [mocha](#https://mochajs.org/), [chai](#https://www.chaijs.com/), [sinon](#https://sinonjs.org/) and [sinon-chai](#https://www.chaijs.com/plugins/sinon-chai/).

To set up automated tests and make uploading from VS Code work you need to copy the `dw.json.example` file to `dw.json`. Then configure your credentials, site ID (used for integration tests only), and the cartridges you want to upload.

The automated tests can be run using these commands:

* `npm run lint:js`
* `npm run test:unit`
* `npm run cover`
* `npm run test:integration`


### Unit Tests

The unit tests can be run using this command: `npm run test:unit`

The unit tests can be found in the `test/unit/int_clik2pay` folder of this repository.


### Integration Tests

The integration tests can be run using this command: `npm run test:integration`

Make sure to set the correct `site-id` in your `dw.json` file. This determines which site is used to execute the integration tests.

The integration tests can be found in the `test/integration/clik2pay` folder of this repository.


## Manual Tests

### Simulating a positive result

To simulate a positive result, use `depositSubmit` as the first name in the billing address.

In this case, the API request attribute `payer.name` is set to `depositSubmit`.

This results in the endpoint `Clik2pay-Notify` receiving a `PAYMENT-COMPLETED` event.

On Production `payer.name` is never modified.


### Test Case: Successful Payment

1. Add a product to cart.
1. Start the checkout.
1. Enter the shipping address and select a shipping method. Use the customer first name `depositSubmit` (case sensitive) to simulate successful payment and submit the shipping information.
1. Select the payment method Clik2pay and submit the billing information.
1. On the summary page submit the order.
1. Verify that the Clik2pay button and informational message are displayed correctly.
1. Click the Clik2pay button.
1. Verify that the external payment page is opened in a popup.
1. Submit the payment information and close the popup.
1. Verify that
    1. the customer is redirected to the order confirmation page and
    1. the order was placed successfully (the order status in Business Manager is `NEW` or `OPEN`
    and `PaymentTransaction.custom.c2pPaymentRequestStatus` is `PAID`).


### Test Case: Open Popup Manually

1. Start the checkout using the Clik2pay payment method and proceed to the order summary page.
1. Click the Clik2pay button.
1. Close the popup.
1. Click the payment link in the informational message next to the button.
1. Verify that the external payment page is opened in a popup.


### Test Case: Failed Payment

1. Start the checkout using the Clik2pay payment method and proceed to the order summary page.
1. Use a customer first name **other than** `depositSubmit`.
1. Click the Clik2pay button.
1. Verify that the external payment page is opened in a popup.
1. Submit the payment information and close the popup.
1. Verify that
    1. an error message is displayed and
    1. the order failed (the order status in Business Manager is `FAILED`
    and `PaymentTransaction.custom.c2pPaymentRequestStatus` is **not** `PAID`).


# Operations and Maintenance

## Data Storage

No custom objects are created by this cartridge. The cartridge uses logging and the order change history in the Business Manager to provide information about successful and failed payments.

Furthermore, multiple custom attributes are used (see descriptions below this table):


### PaymentTransaction custom attributes

Attribute ID                 |  Name                         | Type
---------------------------- | ----------------------------- | ------
c2pPaymentRequestID          | Payment request ID            | String
c2pPaymentRequestStatus      | Payment request status        | String
c2pPaymentRequestShortCode   | Payment request: Short code   | String
c2pPaymentRequestPaymentLink | Payment request: Payment link | String
c2pPaymentRequestQrCodeLink  | Payment Request: QR code link | String
c2pPollingStartDateTime      | Polling Start Date Time       | DateTime

All these attributes can be found in the attribute group *Clik2pay*.


#### Payment request ID

The ID of the payment request as returned by the Create Payment Request service call.


#### Payment request status

This attribute is updated with the attribute `resource.status` from the status notification, handled by the endpoint `Clik2Pay-Notify`.

The value of `PAID` indicates that the payment was successful.


#### Payment request: Short code

A short code that can be used by the payer to complete the payment from the clik2pay website.


#### Payment request: Payment link

The direct URL the customer can use to complete the payment.

This is the URL to the external payment page which is opened in a popup.


#### Payment Request: QR code link

The link to a QR code image which when scanned would direct a customer to the payment link.


#### Polling Start Date Time

Stores the date and time when the checkout summary page started polling `Clik2pay-TryContinuePlaceOrder`.

This is used to determine if polling has timed out. See site preference `c2pPaymentStatusPollingTimeout` (Payment status polling timeout (milliseconds)).


### Order custom attributes

Attribute ID                  |  Name                            | Type
----------------------------- | -------------------------------- | ------
c2pPaymentSuccessAfterTimeout | Payment successful after timeout | Boolean

This attribute can be found in the attribute group *Clik2pay*.


#### Payment successful after timeout

This is true if `Clik2pay-Notify` received a payment success notification but the order was not in status `CREATED` anymore. For example, after client-side polling has already timed out and the order failed. The custom order attribute allows customer service to search for these orders in Business Manager.


### Order Export

An example order export XML file can be found here: `documentation/example-order-export.xml`

## Availability

During your integration, your clik2pay Integration Manager will ask you to provide an email address to contact in the case of outages and disturbances to our system.


## Support

Should you have any questions relating to your integration, please contact our Support Team.


# User Guide

## Roles and Responsibilities

No recurring tasks need to be performed to run the integration.


## Storefront Functionality

This cartridge enables the payment method clik2pay via Interac. On the billing page, the clik2pay method can be chosen. Because Interac does not offer an automatic redirect of customers to the shop an alternative way is used:

The customer submits the billing step and is seeing the summary page. When the customer submits the summary step then a custom place order process takes place.

A payment request link is created and the customer is redirected to the clik2pay payment page which renders the *clik2pay via Interac* button and an informational message.

When the button is clicked a popup opens that allows the customer to use Interac to pay for the order. When successful, clik2pay sends a notification to the storefront's webhook listener, while the customer's browser uses polling to wait for an updated payment transaction status.

In case of success, the customer closes the payment page popup and is automatically redirected to the order confirmation page. Otherwise, an error message is displayed.


# Known Issues

N/A


# Release History

| Version | Date          | Changes         |
|---------|---------------|-----------------|
| 1.0.0   | 2022 MAY      | Initial Release |
