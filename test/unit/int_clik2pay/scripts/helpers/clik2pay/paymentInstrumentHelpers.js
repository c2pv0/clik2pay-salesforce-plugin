'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var chai = require('chai');
chai.use(require('sinon-chai'));
var expect = chai.expect;

var sandbox = sinon.createSandbox();

var mocks = {
    Logger: {
        getLogger: function () { return this; },
        error: sandbox.spy()
    },
    Order: function () {},
    constants: require('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/constants')
};

var paymentInstrumentHelpers = proxyquire('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/paymentInstrumentHelpers.js', {
    'dw/system/Logger': mocks.Logger,
    'dw/order/Order': mocks.Order,
    '*/cartridge/scripts/helpers/clik2pay/constants': mocks.constants
});

describe('paymentInstrumentHelpers', function () {
    beforeEach(function () {
        sandbox.resetHistory();
        mocks.Logger.error.resetHistory();
    });

    describe('findClik2payPaymentInstrument', function () {
        function mockLineItemContainer(instrumentMethodIDs) {
            var lineItemContainer = new mocks.Order();
            lineItemContainer.getPaymentInstruments = function (methodIDToLookFor) {
                var filteredIDs = instrumentMethodIDs.filter(function (instrumentMethodID) {
                    return instrumentMethodID === methodIDToLookFor;
                });
                return {
                    toArray: function () { return filteredIDs; }
                };
            };
            return lineItemContainer;
        }

        it('returns null if the container has no payment instrument', function () {
            var lineItemContainer = mockLineItemContainer([]);

            var result = paymentInstrumentHelpers.findClik2payPaymentInstrument(lineItemContainer);

            expect(result).to.be.null;
            expect(mocks.Logger.error).to.not.have.been.called;
        });

        it('returns null if the container has no Clik2pay payment instrument', function () {
            var lineItemContainer = mockLineItemContainer(['CREDIT_CARD', 'BRIBE_BUDDY']);

            var result = paymentInstrumentHelpers.findClik2payPaymentInstrument(lineItemContainer);

            expect(result).to.be.null;
            expect(mocks.Logger.error).to.not.have.been.called;
        });

        it('returns the container\'s Clik2pay payment instrument', function () {
            var lineItemContainer = mockLineItemContainer(['CREDIT_CARD', 'C2P_VIA_INTERAC']);

            var result = paymentInstrumentHelpers.findClik2payPaymentInstrument(lineItemContainer);

            expect(result).to.be.not.null;
            expect(mocks.Logger.error).to.not.have.been.called;
        });

        it('logs an error if the container has more than one Clik2pay payment instrument', function () {
            var lineItemContainer = mockLineItemContainer(['C2P_VIA_INTERAC', 'C2P_VIA_INTERAC']);

            var result = paymentInstrumentHelpers.findClik2payPaymentInstrument(lineItemContainer);

            expect(result).to.be.not.null;
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });
    });
});
