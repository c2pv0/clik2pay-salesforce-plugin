'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var chai = require('chai');
chai.use(require('sinon-chai'));
var expect = chai.expect;

var sandbox = sinon.createSandbox();

var mocks = {
    Calendar: function () {},
    Logger: {
        getLogger: function () { return this; },
        error: sandbox.spy()
    },
    Order: {
        ORDER_STATUS_NEW: 'NEW',
        ORDER_STATUS_OPEN: 'OPEN',
        ORDER_STATUS_CREATED: 'CREATED',
        ORDER_STATUS_FAILED: 'FAILED'
    },
    OrderMgr: {
        getOrder: sandbox.stub(),
        failOrder: sandbox.spy()
    },
    Resource: {
        msg: function () { return 'resource-message'; }
    },
    Site: {
        current: {
            getCustomPreferenceValue: function () {}
        }
    },
    Transaction: {
        wrap: function (callback) {
            return callback();
        }
    },
    constants: require('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/constants'),
    paymentInstrumentHelpers: {
        findClik2payPaymentInstrument: sandbox.stub()
    },
    checkoutHelperWrappers: {
        continuePlaceOrder: sandbox.stub(),
        getOrderConfirmationUrl: sandbox.stub()
    }
};

var placeOrderHelpers = proxyquire('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/placeOrderHelpers.js', {
    'dw/util/Calendar': mocks.Calendar,
    'dw/system/Logger': mocks.Logger,
    'dw/order/Order': mocks.Order,
    'dw/order/OrderMgr': mocks.OrderMgr,
    'dw/web/Resource': mocks.Resource,
    'dw/system/Site': mocks.Site,
    'dw/system/Transaction': mocks.Transaction,
    '*/cartridge/scripts/helpers/clik2pay/constants': mocks.constants,
    '*/cartridge/scripts/helpers/clik2pay/paymentInstrumentHelpers': mocks.paymentInstrumentHelpers,
    '*/cartridge/scripts/helpers/clik2pay/checkoutHelperWrappers': mocks.checkoutHelperWrappers
});

describe('placeOrderHelpers', function () {
    beforeEach(function () {
        mocks.Calendar.prototype.add = function () {};
        mocks.Calendar.prototype.getTime = function () {};
    });

    afterEach(function () {
        sandbox.resetHistory();
        mocks.Logger.error.resetHistory();
        mocks.OrderMgr.failOrder.resetHistory();
    });

    describe('checkPollingTimeout', function () {
        it('indicates continue on first attempt', function () {
            var paymentTransaction = { custom: { c2pPollingStartDateTime: null } };

            var result = placeOrderHelpers.checkPollingTimeout(paymentTransaction);

            expect(result.isTimedOut).to.be.false;
        });

        it('indicates continue if polling end time is in the future', function () {
            var now = new Date();

            var nowMinus30Sec = new Date();
            nowMinus30Sec.setSeconds(now.getSeconds() - 30);

            var nowPlus30Sec = new Date();
            nowPlus30Sec.setSeconds(now.getSeconds() + 30);

            var paymentTransaction = { custom: { c2pPollingStartDateTime: nowMinus30Sec } }; // polling start
            mocks.Calendar.prototype.getTime = sandbox.stub().returns(nowPlus30Sec); // polling end: in the future

            var result = placeOrderHelpers.checkPollingTimeout(paymentTransaction);

            expect(result.isTimedOut).to.be.false;
        });

        it('indicates timeout if polling end time is reached', function () {
            var now = new Date();

            var nowMinus30Sec = new Date();
            nowMinus30Sec.setSeconds(now.getSeconds() - 30);

            var paymentTransaction = { custom: { c2pPollingStartDateTime: nowMinus30Sec } }; // polling start
            mocks.Calendar.prototype.getTime = sandbox.stub().returns(nowMinus30Sec); // polling end: in the past

            var result = placeOrderHelpers.checkPollingTimeout(paymentTransaction);

            expect(result.isTimedOut).to.be.true;
        });
    });

    describe('determineOrderAction', function () {
        var order;
        var paymentTransaction;

        beforeEach(function () {
            order = {
                status: {
                    value: null,
                    displayValue: null
                }
            };
            paymentTransaction = {
                custom: {
                    c2pPaymentRequestStatus: null
                }
            };
            sandbox.stub(placeOrderHelpers, 'checkPollingTimeout');
        });

        afterEach(function () {
            placeOrderHelpers.checkPollingTimeout.restore();
        });

        it('returns ShowConfirmation if the order was already placed (NEW)', function () {
            order.status.value = mocks.Order.ORDER_STATUS_NEW;

            var result = placeOrderHelpers.determineOrderAction(order, paymentTransaction);

            expect(result.action).to.equal(placeOrderHelpers.OrderAction.ShowConfirmation);
        });

        it('returns ShowConfirmation if the order was already placed (OPEN)', function () {
            order.status.value = mocks.Order.ORDER_STATUS_OPEN;

            var result = placeOrderHelpers.determineOrderAction(order, paymentTransaction);

            expect(result.action).to.equal(placeOrderHelpers.OrderAction.ShowConfirmation);
        });

        it('returns FailOrder if the order is not NEW/OPEN/CREATED', function () {
            order.status.value = mocks.Order.ORDER_STATUS_FAILED;

            var result = placeOrderHelpers.determineOrderAction(order, paymentTransaction);

            expect(result.action).to.equal(placeOrderHelpers.OrderAction.FailOrder);
        });

        it('returns PlaceOrder if the payment request has status PAID', function () {
            order.status.value = mocks.Order.ORDER_STATUS_CREATED;
            paymentTransaction.custom.c2pPaymentRequestStatus = mocks.constants.PaymentRequestStatus.PAID;

            var result = placeOrderHelpers.determineOrderAction(order, paymentTransaction);

            expect(result.action).to.equal(placeOrderHelpers.OrderAction.PlaceOrder);
        });

        it('returns FailOrder if the payment request has status FAILED', function () {
            order.status.value = mocks.Order.ORDER_STATUS_CREATED;
            paymentTransaction.custom.c2pPaymentRequestStatus = mocks.constants.PaymentRequestStatus.FAILED;

            var result = placeOrderHelpers.determineOrderAction(order, paymentTransaction);

            expect(result.action).to.equal(placeOrderHelpers.OrderAction.FailOrder);
        });

        it('returns FailOrder if polling has timed out', function () {
            order.status.value = mocks.Order.ORDER_STATUS_CREATED;
            placeOrderHelpers.checkPollingTimeout.returns({ isTimedOut: true });

            var result = placeOrderHelpers.determineOrderAction(order, paymentTransaction);

            expect(result.action).to.equal(placeOrderHelpers.OrderAction.FailOrder);
        });

        it('returns WaitAndRetry if polling has not timed out', function () {
            order.status.value = mocks.Order.ORDER_STATUS_CREATED;
            placeOrderHelpers.checkPollingTimeout.returns({ isTimedOut: false });

            var result = placeOrderHelpers.determineOrderAction(order, paymentTransaction);

            expect(result.action).to.equal(placeOrderHelpers.OrderAction.WaitAndRetry);
        });
    });

    describe('failOrder', function () {
        var order;

        beforeEach(function () {
            order = {
                trackOrderChange: sandbox.spy()
            };
        });

        it('fails an order', function () {
            placeOrderHelpers.failOrder(order, null);

            expect(mocks.OrderMgr.failOrder).to.have.been.calledWith(order);
            expect(order.trackOrderChange).to.not.have.been.called;
        });

        it('fails an order', function () {
            var orderChangeNote = 'orderChangeNote';

            placeOrderHelpers.failOrder(order, orderChangeNote);

            expect(mocks.OrderMgr.failOrder).to.have.been.calledWith(order);
            expect(order.trackOrderChange).to.have.been.calledWith(orderChangeNote);
        });
    });

    describe('tryContinuePlaceOrder', function () {
        beforeEach(function () {
            sandbox.stub(placeOrderHelpers, 'determineOrderAction');
            sandbox.stub(placeOrderHelpers, 'failOrder');
        });

        afterEach(function () {
            placeOrderHelpers.determineOrderAction.restore();
            placeOrderHelpers.failOrder.restore();
        });

        it('returns error if the order is not found', function () {
            mocks.OrderMgr.getOrder.returns(null);

            var result = placeOrderHelpers.tryContinuePlaceOrder('orderNo', 'orderToken');

            expect(result.isSuccess).to.be.false;
        });

        it('returns error if the Clik2pay payment instrument is not found', function () {
            mocks.OrderMgr.getOrder.returns({});
            mocks.paymentInstrumentHelpers.findClik2payPaymentInstrument.returns(null);

            var result = placeOrderHelpers.tryContinuePlaceOrder('orderNo', 'orderToken');

            expect(result.isSuccess).to.be.false;
        });

        it('returns error if the payment transaction is null', function () {
            mocks.OrderMgr.getOrder.returns({});
            mocks.paymentInstrumentHelpers.findClik2payPaymentInstrument.returns({ paymentTransaction: null });

            var result = placeOrderHelpers.tryContinuePlaceOrder('orderNo', 'orderToken');

            expect(result.isSuccess).to.be.false;
        });

        it('continues to place the order', function () {
            mocks.OrderMgr.getOrder.returns({});
            mocks.paymentInstrumentHelpers.findClik2payPaymentInstrument.returns({ paymentTransaction: {} });
            placeOrderHelpers.determineOrderAction.returns({ action: placeOrderHelpers.OrderAction.PlaceOrder });
            mocks.checkoutHelperWrappers.continuePlaceOrder.returns({ isSuccess: true });

            var result = placeOrderHelpers.tryContinuePlaceOrder('orderNo', 'orderToken');

            expect(result.isSuccess).to.be.true;
            expect(mocks.checkoutHelperWrappers.continuePlaceOrder).to.have.been.calledOnce;
            expect(mocks.checkoutHelperWrappers.getOrderConfirmationUrl).to.not.have.been.called;
        });

        it('indicates to keep polling', function () {
            mocks.OrderMgr.getOrder.returns({});
            mocks.paymentInstrumentHelpers.findClik2payPaymentInstrument.returns({ paymentTransaction: {} });
            placeOrderHelpers.determineOrderAction.returns({ action: placeOrderHelpers.OrderAction.WaitAndRetry });

            var result = placeOrderHelpers.tryContinuePlaceOrder('orderNo', 'orderToken');

            expect(result.isSuccess).to.be.true;
            expect(result.keepPolling).to.be.true;
            expect(mocks.checkoutHelperWrappers.continuePlaceOrder).to.not.have.been.called;
            expect(mocks.checkoutHelperWrappers.getOrderConfirmationUrl).to.not.have.been.called;
        });

        it('returns the order confirmation page url', function () {
            mocks.OrderMgr.getOrder.returns({});
            mocks.paymentInstrumentHelpers.findClik2payPaymentInstrument.returns({ paymentTransaction: {} });
            placeOrderHelpers.determineOrderAction.returns({ action: placeOrderHelpers.OrderAction.ShowConfirmation });
            mocks.checkoutHelperWrappers.getOrderConfirmationUrl.returns({ isSuccess: true });

            var result = placeOrderHelpers.tryContinuePlaceOrder('orderNo', 'orderToken');

            expect(result.isSuccess).to.be.true;
            expect(mocks.checkoutHelperWrappers.continuePlaceOrder).to.not.have.been.called;
            expect(mocks.checkoutHelperWrappers.getOrderConfirmationUrl).to.have.been.calledOnce;
        });

        it('fails the order', function () {
            mocks.OrderMgr.getOrder.returns({});
            mocks.paymentInstrumentHelpers.findClik2payPaymentInstrument.returns({ paymentTransaction: {} });
            placeOrderHelpers.determineOrderAction.returns({ action: placeOrderHelpers.OrderAction.FailOrder, failOrderReason: 'reason' });

            var result = placeOrderHelpers.tryContinuePlaceOrder('orderNo', 'orderToken');

            expect(result.isSuccess).to.be.false;
            expect(placeOrderHelpers.failOrder).to.have.been.calledOnce;
            expect(mocks.checkoutHelperWrappers.continuePlaceOrder).to.not.have.been.called;
            expect(mocks.checkoutHelperWrappers.getOrderConfirmationUrl).to.not.have.been.called;
        });
    });
});
