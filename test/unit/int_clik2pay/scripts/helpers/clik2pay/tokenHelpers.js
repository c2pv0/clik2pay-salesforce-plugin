'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var chai = require('chai');
chai.use(require('sinon-chai'));
var expect = chai.expect;

var sandbox = sinon.createSandbox();

var mocks = {
    Cache: {
        get: sandbox.stub(),
        put: sandbox.spy(),
        invalidate: sandbox.spy()
    },
    CacheMgr: {
        getCache: sandbox.stub()
    },
    Logger: {
        getLogger: function () { return this; },
        error: sandbox.spy()
    },
    Site: {
        current: {
            ID: 'SiteID'
        }
    },
    constants: require('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/constants'),
    credentialHelpers: {
        getAuthenticationCredentialID: function () {
            return 'CredentialID';
        }
    },
    service: {
        getCredentialID: sandbox.stub(),
        configuration: {
            profile: {
                timeoutMillis: 20000
            }
        }
    },
    expectedServiceCredentialID: 'int_clik2pay.httpForm.clik2pay.get_auth_token',
    token: 'an absolutely perfect bearer token',
    milliSecondsBeforeExpiration: 3600000,
    expiredCacheEntry: {

    }
};

var tokenHelpers = proxyquire('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/tokenHelpers.js', {
    'dw/system/CacheMgr': mocks.CacheMgr,
    'dw/system/Logger': mocks.Logger,
    'dw/system/Site': mocks.Site,
    '*/cartridge/scripts/helpers/clik2pay/constants': mocks.constants,
    '*/cartridge/scripts/helpers/clik2pay/credentialHelpers': mocks.credentialHelpers
});

describe('tokenHelpers', function () {
    beforeEach(function () {
        sandbox.resetBehavior();
        sandbox.resetHistory();

        mocks.Cache.get.resetHistory();
        mocks.Cache.invalidate.resetHistory();

        mocks.Cache.get.returns(null);
        mocks.CacheMgr.getCache.returns(mocks.Cache);
        mocks.service.getCredentialID.returns(mocks.expectedServiceCredentialID);
    });

    describe('getTokenCache', function () {
        it('returns null if CacheMgr throws an exception', function () {
            mocks.CacheMgr.getCache.throws('no cache found - configuration is missing or incorrect');

            var cache = tokenHelpers.getTokenCache();

            expect(cache).to.be.null;
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns the custom cache', function () {
            var expectedCache = 'dummy';
            mocks.CacheMgr.getCache.returns(expectedCache);

            var actualCache = tokenHelpers.getTokenCache();

            expect(actualCache).to.equal(expectedCache);
            expect(mocks.Logger.error).to.not.have.been.called;
        });
    });

    describe('storeToken', function () {
        beforeEach(function () {
            sandbox.stub(tokenHelpers, 'getTokenCache');
        });

        afterEach(function () {
            tokenHelpers.getTokenCache.restore();
        });

        it('logs an error if the cache configuration is not correct', function () {
            tokenHelpers.getTokenCache.returns(null);

            tokenHelpers.storeToken(mocks.service, mocks.token, mocks.milliSecondsBeforeExpiration);

            expect(mocks.Cache.put).to.not.have.been.called;
        });

        it('stores the correct data in the correct cache', function () {
            var now = new Date();
            var validUntil = new Date(now.getTime() + mocks.milliSecondsBeforeExpiration);
            tokenHelpers.getTokenCache.returns(mocks.Cache);

            tokenHelpers.storeToken(mocks.service, mocks.token, validUntil);

            // expect the data to be put into the correct cache
            expect(mocks.Cache.put).to.have.been.called;
            expect(mocks.Cache.put.args[0]).to.not.be.empty;
            expect(mocks.Cache.put.args[0]).to.have.lengthOf(2);

            var cachedTokenData = mocks.Cache.put.args[0][1];
            expect(cachedTokenData.token).to.equal(mocks.token);
            var cachedValidityDate = new Date(cachedTokenData.validUntil);
            expect(cachedValidityDate).to.be.greaterThanOrEqual(validUntil);
        });
    });

    describe('retrieveToken', function () {
        beforeEach(function () {
            sandbox.stub(tokenHelpers, 'getTokenCache');
        });

        afterEach(function () {
            tokenHelpers.getTokenCache.restore();
        });

        it('returns null if there is no cache configured', function () {
            tokenHelpers.getTokenCache.returns(null);

            var result = tokenHelpers.retrieveToken(mocks.service);

            expect(result).to.be.null;
        });

        it('returns null if there is no entry stored in the cache', function () {
            tokenHelpers.getTokenCache.returns(mocks.Cache);

            var result = tokenHelpers.retrieveToken(mocks.service);

            expect(result).to.be.null;
        });

        it('returns null and invalidates the cache if the stored token is no longer valid', function () {
            tokenHelpers.getTokenCache.returns(mocks.Cache);
            // A token has to be valid at least until now, plus the timeout of the service
            // to be fresh enough to be safe for use. Otherwise the shop has to get a new one
            var cacheEntryWithExpiredToken = {
                token: 'cachedToken',
                validUntil: (new Date()).toISOString()
            };
            mocks.Cache.get.returns(cacheEntryWithExpiredToken);

            var result = tokenHelpers.retrieveToken(mocks.service);

            expect(mocks.Cache.invalidate).to.have.been.called;
            expect(result).to.be.null;
        });

        it('returns a token if there is a valid entry', function () {
            tokenHelpers.getTokenCache.returns(mocks.Cache);
            var expectedToken = 'a totally valid well retrieved token';
            var now = new Date();
            var sufficientlyValidUntil = now.getTime() + (2 * mocks.service.configuration.profile.timeoutMillis);
            var cacheEntryWithValidToken = {
                token: expectedToken,
                validUntil: (new Date(sufficientlyValidUntil)).toISOString()
            };
            mocks.Cache.get.returns(cacheEntryWithValidToken);

            var result = tokenHelpers.retrieveToken(mocks.service);

            expect(mocks.Cache.invalidate).to.not.have.been.called; // the token stays valid, so it remains cached
            expect(result).to.equal(expectedToken);
        });
    });
});
