'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var chai = require('chai');
chai.use(require('sinon-chai'));
var expect = chai.expect;

var sandbox = sinon.createSandbox();

var mocks = {
    Logger: {
        getLogger: function () { return this; },
        warn: sandbox.spy(),
        error: sandbox.spy()
    },
    preparedProblemReporter: 'uninitialized',
    preparedProblemReporterID: 'ReporterID: No worries mate',
    stringUtils: {
        format: function () {
            return [].slice.call(arguments).join('::');
        }
    },
    defaultFormat: {
        error: 'definitively not a usual error format',
        warning: 'definitively not a usual warning format'
    }
};

var ProblemReporter = proxyquire('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/problemReporter.js', {
    'dw/system/Logger': mocks.Logger,
    'dw/util/StringUtils': mocks.stringUtils
});

describe('problemReporter', function () {
    beforeEach(function () {
        sandbox.resetHistory();
        sandbox.resetBehavior();

        // prepare a fresh default ProblemReporter
        mocks.preparedProblemReporter = new ProblemReporter(mocks.preparedProblemReporterID, mocks.log, mocks.defaultFormat.error, mocks.defaultFormat.warning);
    });

    describe('addErrorMessage', function () {
        it('adds error messages', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.errorMessages).to.be.empty;

            preparedProblemReporter.addErrorMessage('an error has occurred... well not really... this is a test error');
            expect(preparedProblemReporter.errorMessages).to.have.lengthOf(1);

            preparedProblemReporter.addErrorMessage('another error has occurred');
            expect(preparedProblemReporter.errorMessages).to.have.lengthOf(2);
        });

        it('adds every error message only once (duplicates are ignored)', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.errorMessages).to.be.empty;

            preparedProblemReporter.addErrorMessage('this is the same error');
            preparedProblemReporter.addErrorMessage('this is the same error');

            expect(preparedProblemReporter.errorMessages).to.have.lengthOf(1);
        });
    });

    describe('addWarningMessage', function () {
        it('adds warning messages', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.warningMessages).to.be.empty;

            preparedProblemReporter.addWarningMessage('you can ignore this but... you have been *warned*');
            expect(preparedProblemReporter.warningMessages).to.have.lengthOf(1);

            preparedProblemReporter.addWarningMessage('this is your second warning');
            expect(preparedProblemReporter.warningMessages).to.have.lengthOf(2);
        });

        it('adds every warning message only once (duplicates are ignored)', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.warningMessages).to.be.empty;

            preparedProblemReporter.addWarningMessage('this is the same warning');
            preparedProblemReporter.addWarningMessage('this is the same warning');

            expect(preparedProblemReporter.warningMessages).to.have.lengthOf(1);
        });
    });

    describe('hasErrors', function () {
        it('knows if it has errors to report', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.errorMessages).to.be.empty;
            expect(preparedProblemReporter.hasErrors()).to.be.false;

            preparedProblemReporter.errorMessages.push('now there is an error message');
            expect(preparedProblemReporter.hasErrors()).to.be.true;

            preparedProblemReporter.errorMessages.push('now there is another error message');
            expect(preparedProblemReporter.hasErrors()).to.be.true;

            // Not exactly a mandatory feature (yet) for the problemReporter but still an interesting point if...
            // ... we should decide to flush all current messages at some point (e.g. after each logStatus call);
            preparedProblemReporter.errorMessages = []; // Now there are no error messages anymore;
            expect(preparedProblemReporter.hasErrors()).to.be.false;
        });
    });

    describe('hasWarnings', function () {
        it('knows if it has warnings to report', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.warningMessages).to.be.empty;
            expect(preparedProblemReporter.hasWarnings()).to.be.false;

            preparedProblemReporter.warningMessages.push('now there is a warning message');
            expect(preparedProblemReporter.hasWarnings()).to.be.true;

            preparedProblemReporter.warningMessages.push('now there is another warning message');
            expect(preparedProblemReporter.hasWarnings()).to.be.true;

            // Not exactly a mandatory feature (yet) for the problemReporter but still an interesting point if...
            // ... we should decide to flush all current messages at some point (e.g. after each logStatus call);
            preparedProblemReporter.warningMessages = []; // Now there are no warning messages anymore;
            expect(preparedProblemReporter.hasWarnings()).to.be.false;
        });
    });

    describe('getErrorReport', function () {
        it('returns an error report if errors are present', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.errorMessages).to.be.empty;

            var firstErrorMessage = 'This is the most marvelous error ever reported... in theory';
            var secondErrorMessage = 'This is the second most marvelous error ever reported... in theory';
            preparedProblemReporter.addErrorMessage(firstErrorMessage);
            var firstErrorReport = preparedProblemReporter.getErrorReport();
            expect(firstErrorReport).to.not.be.empty;
            expect(firstErrorReport).to.include(firstErrorMessage);

            preparedProblemReporter.addErrorMessage(secondErrorMessage);
            var secondErrorReport = preparedProblemReporter.getErrorReport();

            expect(secondErrorReport).to.include(firstErrorMessage);
            expect(secondErrorReport).to.include(secondErrorMessage);
        });

        it('returns an empty error report if no errors are present', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.errorMessages).to.be.empty;

            var errorReport = preparedProblemReporter.getErrorReport();
            expect(errorReport).to.be.empty;
        });
    });

    describe('getWarningReport', function () {
        it('returns a warning report if warnings are present', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.warningMessages).to.be.empty;

            var firstWarning = 'Warning: Yoru orhatopgrhy is scwreed';
            var secondWarning = 'Warning: Yoru orgrathophy is still scwreed';

            preparedProblemReporter.addWarningMessage(firstWarning);
            var firstWarningReport = preparedProblemReporter.getWarningReport();
            expect(firstWarningReport).to.not.be.empty;
            expect(firstWarningReport).to.include(firstWarning);

            preparedProblemReporter.addWarningMessage(secondWarning);
            var secondWarningReport = preparedProblemReporter.getWarningReport();
            expect(secondWarningReport).to.not.be.empty;
            expect(secondWarningReport).to.include(firstWarning);
            expect(secondWarningReport).to.include(secondWarning);
        });

        it('returns an empty warning report if no warnings are present', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.warningMessages).to.be.empty;

            var warningReport = preparedProblemReporter.getWarningReport();
            expect(warningReport).to.be.empty;
        });
    });

    describe('getStatusReport', function () {
        it('returns an error report if errors are present but no warnings', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.warningMessages).to.be.empty;
            expect(preparedProblemReporter.errorMessages).to.be.empty;

            var firstError = 'The very first error... a bit unexpected but there it is';
            var secondError = 'Another? Oh golly';

            preparedProblemReporter.addErrorMessage(firstError);
            var firstStatusReport = preparedProblemReporter.getStatusReport();
            expect(firstStatusReport).to.not.be.empty;
            expect(firstStatusReport).to.include(firstError);

            preparedProblemReporter.addErrorMessage(secondError);
            var secondStatusReport = preparedProblemReporter.getStatusReport();
            expect(secondStatusReport).to.not.be.empty;
            expect(secondStatusReport).to.include(firstError);
            expect(secondStatusReport).to.include(secondError);
        });

        it('returns a warning report if warnings are present but no errors', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.warningMessages).to.be.empty;
            expect(preparedProblemReporter.errorMessages).to.be.empty;

            var firstWarning = 'Warning: There will be a Warning shortly';
            var secondWarning = 'This is the warning you were warned about';

            preparedProblemReporter.addWarningMessage(firstWarning);
            var firstStatusReport = preparedProblemReporter.getStatusReport();
            expect(firstStatusReport).to.not.be.empty;
            expect(firstStatusReport).to.include(firstWarning);

            preparedProblemReporter.addWarningMessage(secondWarning);
            var secondStatusReport = preparedProblemReporter.getStatusReport();
            expect(secondStatusReport).to.not.be.empty;
            expect(secondStatusReport).to.include(firstWarning);
            expect(secondStatusReport).to.include(secondWarning);
        });

        it('returns a combined error and warning report if both are present', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.warningMessages).to.be.empty;
            expect(preparedProblemReporter.errorMessages).to.be.empty;

            var firstWarning = 'Warning: There will be a Warning shortly';
            var secondWarning = 'This is the warning you were warned about';
            var firstError = 'The very first error... a bit unexpected but there it is';
            var secondError = 'Another? Oh golly';

            // prepare the FIRST REPORT and check our expectations
            preparedProblemReporter.addWarningMessage(firstWarning);
            preparedProblemReporter.addErrorMessage(firstError);
            var firstStatusReport = preparedProblemReporter.getStatusReport();

            expect(firstStatusReport).to.not.be.empty;
            expect(firstStatusReport).to.include(firstWarning);
            expect(firstStatusReport).to.include(firstError);

            // prepare a SECOND REPORT and check our expectations
            preparedProblemReporter.addWarningMessage(secondWarning);
            preparedProblemReporter.addErrorMessage(secondError);
            var secondStatusReport = preparedProblemReporter.getStatusReport();

            expect(secondStatusReport).to.not.be.empty;
            expect(secondStatusReport).to.include(firstWarning);
            expect(secondStatusReport).to.include(secondWarning);
            expect(secondStatusReport).to.include(firstError);
            expect(secondStatusReport).to.include(secondError);
        });

        it('returns an empty status report if no errors and no warnings are present', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.errorMessages).to.be.empty;
            expect(preparedProblemReporter.warningMessages).to.be.empty;
            var statusReport = preparedProblemReporter.getStatusReport();
            expect(statusReport).to.be.empty;
        });
    });

    describe('logStatus', function () {
        var firstWarning;
        var secondWarning;
        var firstError;
        var secondError;

        var warnLogger;
        var errorLogger;

        beforeEach(function () {
            firstWarning = 'Warning: nothing to warn about';
            secondWarning = 'I was wrong, this is a warning!';
            firstError = 'Error! Error! Report this error!';
            secondError = 'Error: Don\'t panic! Keep Calm! Carry On!... but also let\'s fix this';

            warnLogger = mocks.Logger.warn;
            errorLogger = mocks.Logger.error;
        });

        function buildInclusionMatcher(message) {
            return function (array) {
                var includedMessage = message;
                return array.some(function (element) {
                    return element.includes(includedMessage);
                });
            };
        }

        it('logs error messages on error level', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            preparedProblemReporter.errorMessages = [firstError, secondError];
            expect(preparedProblemReporter.warningMessages).to.be.empty;
            expect(errorLogger).to.not.have.been.called;

            preparedProblemReporter.logStatus();
            expect(warnLogger).to.not.have.been.called;
            expect(errorLogger).to.have.been.calledOnce;

            // Note: we expect all errors and warnings to be logged as a compiled report, not individually
            // so we only have to check the last call to this logging function
            expect(errorLogger.lastCall.args).to.satisfy(buildInclusionMatcher(firstError));
            expect(errorLogger.lastCall.args).to.satisfy(buildInclusionMatcher(secondError));
        });

        it('logs warning messages on warning level', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            preparedProblemReporter.warningMessages = [firstWarning, secondWarning];
            expect(preparedProblemReporter.errorMessages).to.be.empty;

            preparedProblemReporter.logStatus();
            expect(errorLogger).to.not.have.been.called;
            expect(warnLogger).to.have.been.calledOnce;

            // Note: we expect all errors and warnings to be logged as a compiled report, not individually
            // so we only have to check the last call to this logging function
            expect(warnLogger.lastCall.args).to.satisfy(buildInclusionMatcher(firstWarning));
            expect(warnLogger.lastCall.args).to.satisfy(buildInclusionMatcher(secondWarning));
        });

        it('logs errors on error level and warnings on warning message for combined reports', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            preparedProblemReporter.warningMessages = [firstWarning, secondWarning];
            preparedProblemReporter.errorMessages = [firstError, secondError];

            expect(errorLogger).to.not.have.been.called;
            expect(warnLogger).to.not.have.been.called;

            preparedProblemReporter.logStatus();
            expect(errorLogger).to.have.been.calledOnce;
            expect(warnLogger).to.have.been.calledOnce;

            // Note: we expect all errors and warnings to be logged as a compiled report, not individually
            // so we only have to check the last call to these logging functions
            expect(warnLogger.lastCall.args).to.satisfy(buildInclusionMatcher(firstWarning));
            expect(warnLogger.lastCall.args).to.satisfy(buildInclusionMatcher(secondWarning));
            expect(errorLogger.lastCall.args).to.satisfy(buildInclusionMatcher(firstError));
            expect(errorLogger.lastCall.args).to.satisfy(buildInclusionMatcher(secondError));
        });

        it('does not log if there is nothing to log', function () {
            var preparedProblemReporter = mocks.preparedProblemReporter;
            expect(preparedProblemReporter.errorMessages).to.be.empty;
            expect(preparedProblemReporter.warningMessages).to.be.empty;

            preparedProblemReporter.logStatus();

            expect(errorLogger).to.not.have.been.called;
            expect(warnLogger).to.not.have.been.called;
        });
    });
});
