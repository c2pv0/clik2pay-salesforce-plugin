'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var chai = require('chai');
chai.use(require('sinon-chai'));
var expect = chai.expect;

var sandbox = sinon.createSandbox();

var mocks = {
    StringUtils: {
        format: sandbox.stub()
    },
    problemReporter: {
        addErrorMessage: sandbox.spy(),
        addWarningMessage: sandbox.spy(),
        logStatus: sandbox.spy(),
        getStatusReport: function () {
            if (this.addErrorMessage.callCount > 0) {
                return 'there are errors reported';
            }
            return '';
        }
    }
};

/**
 * Mocks the ProblemReporter to keep this a unit test for validationHelper
 */
function ProblemReporter() {
    this.addErrorMessage = mocks.problemReporter.addErrorMessage;
    this.addWarningMessage = mocks.problemReporter.addWarningMessage;
    this.logStatus = mocks.problemReporter.logStatus;
    this.getStatusReport = mocks.problemReporter.getStatusReport;
}


var validationHelper = proxyquire('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/validationHelpers.js', {
    'dw/util/StringUtils': mocks.StringUtils,
    '*/cartridge/scripts/helpers/clik2pay/problemReporter': ProblemReporter
});

describe('validationHelper', function () {
    beforeEach(function () {
        sandbox.resetHistory();
        sandbox.resetBehavior();
        mocks.StringUtils.format.returnsArg(1); // needed for tests of validateCreatePaymentRequestResult
    });

    describe('validateCreatePaymentRequestParameters', function () {
        it('checks and reports missing parameters', function () {
            var validationReporter = validationHelper.validateCreatePaymentRequestParameters();

            expect(validationReporter).to.exist;
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledOnce;
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWithMatch('parameters');
        });

        it('acknowledges it when all is valid and good', function () {
            var validRequestParameters = {
                body: {
                    amount: 1.0,
                    deliveryMode: 'NONE',
                    payer: {
                        name: 'a customerName'
                    }
                },
                apiKey: 'great api key',
                token: 'some fantastic token'
            };

            var validationReporter = validationHelper.validateCreatePaymentRequestParameters(validRequestParameters);

            expect(validationReporter).to.exist;

            // Expected problems: none
            expect(mocks.problemReporter.addErrorMessage).to.not.have.been.called;
            expect(mocks.problemReporter.addWarningMessage).to.not.have.been.called;
        });

        it('identifies multiple problems with token, apiKey, an invalid amount and the email delivery mode', function () {
            // Note: This also tests the ability to report multiple problems at once
            var requestParametersFullOfProblems = {
                body: {
                    amount: 0,
                    payer: {},
                    deliveryMode: 'EMAIL'
                }
            };

            var validationReporter = validationHelper.validateCreatePaymentRequestParameters(requestParametersFullOfProblems);

            expect(validationReporter).to.exist;

            // Expected problems:
            // 1. authentication Token
            // 2. api key
            // 3. invalid Amount
            // 4. no customer name
            // 5. no customer mail although needed for deliveryMode
            expect(mocks.problemReporter.addErrorMessage).to.have.callCount(5); // five problems
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWithMatch('customer name');
            expect(mocks.problemReporter.addErrorMessage).to.not.have.been.calledWithMatch('payer data');

            // expect results to have been logged
            expect(mocks.problemReporter.logStatus).to.have.been.calledOnce;
        });

        it('identifies a missing payer object and reports it', function () {
            var requestParametersWithoutPayer = {
                body: {
                    amount: 1.0,
                    deliveryMode: 'NONE'
                },
                apiKey: 'great api key',
                token: 'some fantastic token'
            };

            var validationReporter = validationHelper.validateCreatePaymentRequestParameters(requestParametersWithoutPayer);

            expect(validationReporter).to.exist;

            // Expected problems:
            // missing payer object
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledOnce;
            expect(mocks.problemReporter.addErrorMessage).to.not.have.been.calledWithMatch('customer name');
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWithMatch('payer data');

            // expect results to have been logged
            expect(mocks.problemReporter.logStatus).to.have.been.calledOnce;
        });

        it('identifies a missing phone number in case deliveryMode is SMS', function () {
            var requestParametersWithoutPhoneNumber = {
                body: {
                    amount: 1.0,
                    deliveryMode: 'SMS',
                    payer: {
                        name: 'a customerName'
                    }
                },
                apiKey: 'great api key',
                token: 'some fantastic token'
            };

            var validationReporter = validationHelper.validateCreatePaymentRequestParameters(requestParametersWithoutPhoneNumber);

            expect(validationReporter).to.exist;

            // Expected problems:
            // missing phone number
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledOnce;
            expect(mocks.problemReporter.addErrorMessage).to.not.have.been.calledWithMatch('customer name');
            expect(mocks.problemReporter.addErrorMessage).to.not.have.been.calledWithMatch('payer data');
            // we expect the message to include the words SMS and phone number
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWithMatch('SMS');
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWithMatch('phone number');

            // expect results to have been logged
            expect(mocks.problemReporter.logStatus).to.have.been.calledOnce;
        });
    });

    describe('validateCreatePaymentRequestResult', function () {
        it('checks and reports missing parameters', function () {
            var validationReport = validationHelper.validateCreatePaymentRequestResult();
            expect(validationReport).to.not.be.empty;
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledTwice;
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWithMatch('parameters');
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWithMatch('no service call result');

            // expect results to have been logged
            expect(mocks.problemReporter.logStatus).to.have.been.calledOnce;
        });

        it('acknowledges it when all is valid and good', function () {
            var parameters = {
                body: {
                    amount: 20.22,
                    type: 'ECOMM',
                    payer: {
                        name: 'Jane Doe'
                    },
                    deliveryMode: 'NONE',
                    invoiceNumber: 'invoice11',
                    merchantTransactionId: 'orderNo24'
                }
            };
            var createPaymentRequestResponse = {
                amount: 20.22,
                type: 'ECOMM',
                payer: {
                    name: 'Jane Doe'
                },
                paymentLink: 'https://click2pay-payment-link',
                deliveryMode: 'NONE',
                invoiceNumber: 'invoice11',
                merchantTransactionId: 'orderNo24'
            };

            var validationReport = validationHelper.validateCreatePaymentRequestResult(parameters, createPaymentRequestResponse);

            expect(validationReport).to.be.empty;
            expect(mocks.problemReporter.addErrorMessage).to.have.not.been.called;
            expect(mocks.problemReporter.addWarningMessage).to.have.not.been.called;
        });

        it('identifies invalid responses', function () {
            var parameters = {
                body: {
                    amount: 20.22,
                    deliveryMode: 'NONE',
                    invoiceNumber: 'invoice11',
                    merchantTransactionId: 'orderNo24',
                    payer: {
                        name: 'Jane Doe'
                    },
                    type: 'ECOMM'
                }
            };
            var createPaymentRequestResponse = { payer: {} };

            var validationReport = validationHelper.validateCreatePaymentRequestResult(parameters, createPaymentRequestResponse);

            // we expect the validationReporters messages to include the names of fields that dont match
            expect(validationReport).to.not.be.empty;
            expect(mocks.problemReporter.addErrorMessage).to.have.callCount(7);
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWith('amount');
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWith('deliveryMode');
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWith('invoiceNumber');
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWith('merchantTransactionId');
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWith('payer.name');
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWith('type');
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWithMatch('link');
        });
    });
});
