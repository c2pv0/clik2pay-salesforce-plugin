'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var chai = require('chai');
chai.use(require('sinon-chai'));
var expect = chai.expect;

var sandbox = sinon.createSandbox();

var mocks = {
    Logger: {
        getLogger: function () { return this; },
        debug: sandbox.spy(),
        error: sandbox.spy()
    },
    Order: {
        ORDER_STATUS_CREATED: 'CREATED'
    },
    StringUtils: {
        format: function (x) { return x; }
    },
    Transaction: {
        wrap: function (callback) {
            callback();
        }
    },
    URLUtils: {
        url: function () { return { toString: function () { return 'url'; } }; }
    },
    constants: require('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/constants'),
    problemReporter: {
        addErrorMessage: sandbox.spy(),
        addWarningMessage: sandbox.spy(),
        logStatus: sandbox.spy(),
        getStatusReport: function () {
            if (this.addErrorMessage.callCount > 0) {
                return 'there are errors reported';
            }
            return '';
        },
        hasErrors: sandbox.stub()
    },
    serviceAPI: {
        createPaymentRequest: sandbox.stub()
    }
};

/**
 * Mocks the money class
 *
 * @param {number} amount the amount of money represented by this instance
 * @param {string} currencyCode the three digit code of the currency this amount is representative of
 */

function Money(amount, currencyCode) {
    this.value = amount;
    this.currencyCode = currencyCode;
    this.toFormattedString = function () {
        return this.currencyCode + this.amount;
    };
    this.equals = function (otherMoney) {
        return (this.value === otherMoney.value && this.currencyCode === otherMoney.currencyCode);
    };
}
/**
 * Mocks the ProblemReporter to keep this a unit test for validationHelper
 */
function ProblemReporter() {
    this.addErrorMessage = mocks.problemReporter.addErrorMessage;
    this.addWarningMessage = mocks.problemReporter.addWarningMessage;
    this.logStatus = mocks.problemReporter.logStatus;
    this.getStatusReport = mocks.problemReporter.getStatusReport;
    this.hasErrors = function () {
        return this.addErrorMessage.callCount > 0;
    };
}

var processorHelpers = proxyquire('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/processorHelpers.js', {
    'dw/system/Logger': mocks.Logger,
    'dw/util/StringUtils': mocks.StringUtils,
    'dw/value/Money': Money,
    'dw/order/Order': mocks.Order,
    'dw/system/Transaction': mocks.Transaction,
    'dw/web/URLUtils': mocks.URLUtils,
    '*/cartridge/scripts/helpers/clik2pay/constants': mocks.constants,
    '*/cartridge/scripts/helpers/clik2pay/problemReporter': ProblemReporter,
    '*/cartridge/scripts/payment/clik2pay/serviceAPI': mocks.serviceAPI
});

describe('clik2pay payment processor', function () {
    beforeEach(function () {
        sandbox.resetHistory();
        sandbox.resetBehavior();
    });

    describe('handle', function () {
        it('returns an error in case the basket is missing', function () {
            expect(processorHelpers.handle()).to.not.throw;
            expect(mocks.Logger.error).to.have.been.called;
        });

        it('removes other payment instruments and adds a clik2pay instrument', function () {
            var basket = {
                removeAllPaymentInstruments: sandbox.spy(),
                createPaymentInstrument: sandbox.stub(),
                totalGrossPrice: 123.45
            };
            processorHelpers.handle(basket, mocks.constants.PaymentMethodClik2payInterac);

            expect(basket.removeAllPaymentInstruments).to.have.been.called;
            expect(basket.createPaymentInstrument).to.have.been.calledOnce;
            expect(basket.createPaymentInstrument).to.have.been.calledWith(mocks.constants.PaymentMethodClik2payInterac, basket.totalGrossPrice);
        });
    });

    describe('authorize', function () {
        var order;

        beforeEach(function () {
            // Please note: order is the currently mocked order instance.
            // But mocks.Order (with a capital O) is the mocked SFCC class dw.order.Order

            // init order
            order = {
                status: {
                    value: mocks.Order.ORDER_STATUS_CREATED,
                    displayValue: 'display_value'
                },
                orderNo: 'expectedOrderNo',
                totalGrossPrice: {
                    currencyCode: 'CAD'
                },
                trackOrderChange: sandbox.spy()
            };

            // init payment instrument and transaction
            mocks.paymentInstrument = {
                getPaymentTransaction: sandbox.stub()
            };
            mocks.paymentTransaction = {
                getAmount: sandbox.stub(),
                setAmount: sandbox.spy(),
                setTransactionID: sandbox.stub(),
                custom: {
                    c2pPaymentRequestPaymentLink: null,
                    c2pPaymentRequestQrCodeLink: null,
                    c2pPaymentRequestShortCode: null
                }
            };
            mocks.paymentInstrument.getPaymentTransaction.returns(mocks.paymentTransaction);

            // init serviceAPI and call results
            mocks.confirmedAmount = 123.45;

            mocks.serviceAPI.callResult = {
                success: true,
                clik2payResourceLink: 'https://url.to.resource.at.click2pay',
                confirmedAmount: mocks.confirmedAmount,
                id: 'expected ID',
                merchantTransactionId: 'expected transaction ID',
                status: 'created',
                paymentRequestLink: 'https://expected.url',
                qrCodeLink: '"https://pay.clik2pay.com/qr/ABCDEFG',
                shortCode: 'ABCDEFG'
            };
            mocks.serviceAPI.createPaymentRequest.returns(mocks.serviceAPI.callResult);

            mocks.paymentTransaction.getAmount.returns(new Money(mocks.confirmedAmount, order.totalGrossPrice.currencyCode));
        });

        it('reports errors in case there is no order', function () {
            var authorizationResult = processorHelpers.authorize(null, null);

            expect(authorizationResult.error).to.be.true;
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledTwice;
            expect(mocks.problemReporter.logStatus).to.have.been.calledOnce;
            expect(mocks.serviceAPI.createPaymentRequest).to.not.have.been.called;
        });

        it('reports an error if there is no payment instrument', function () {
            var authorizationResult = processorHelpers.authorize(order, null);

            expect(authorizationResult.error).to.be.true;
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledOnce;
            expect(mocks.problemReporter.logStatus).to.have.been.calledOnce;
            expect(mocks.serviceAPI.createPaymentRequest).to.not.have.been.called;
        });

        it('reports an error in case the order has the wrong status', function () {
            order.status.value = 'wrong status';
            var authorizationResult = processorHelpers.authorize(order, mocks.paymentInstrument);

            expect(authorizationResult.error).to.be.true;
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledOnce;
            expect(mocks.problemReporter.logStatus).to.have.been.calledOnce;
            expect(mocks.serviceAPI.createPaymentRequest).to.not.have.been.called;
        });

        it('reports an error in case there is a problem with calling the create payment request service', function () {
            var serviceResult = mocks.serviceAPI.callResult;
            serviceResult.problemsToReport = 'the problems here are critical ... but not serious';
            serviceResult.success = false;

            var authorizationResult = processorHelpers.authorize(order, mocks.paymentInstrument);

            expect(authorizationResult.error).to.be.true;
            expect(mocks.serviceAPI.createPaymentRequest).to.have.been.called;
            expect(mocks.problemReporter.addErrorMessage).to.have.been.calledWith(serviceResult.problemsToReport);
            expect(mocks.problemReporter.logStatus).to.have.been.called;
            expect(order.trackOrderChange).to.have.been.called;
        });

        it('reports an error in case the confirmed amount does not equal the requested amount', function () {
            mocks.serviceAPI.callResult.confirmedAmount -= 10;

            var authorizationResult = processorHelpers.authorize(order, mocks.paymentInstrument);
            expect(authorizationResult.error).to.be.true;
            expect(authorizationResult.problemsToReport).to.not.be.empty;
            expect(mocks.problemReporter.logStatus).to.have.been.called;

            var paymentTransaction = mocks.paymentTransaction;

            // Check the setters
            expect(paymentTransaction.setTransactionID).to.have.been.calledOnce;
            expect(paymentTransaction.setAmount).to.not.have.been.called; // The amount is set when the payment Instrument is created and should not be manipulated afterwards

            // check the orderNote
            expect(order.trackOrderChange).to.have.been.calledOnce;
            var orderChangeArguments = order.trackOrderChange.lastCall.args;
            expect(orderChangeArguments[0]).to.not.be.empty;
        });

        it('updates the order, adds an order note and returns success if all went well', function () {
            var authorizationResult = processorHelpers.authorize(order, mocks.paymentInstrument);
            expect(authorizationResult.error).to.be.false;

            var serviceCallResult = mocks.serviceAPI.callResult;
            var paymentTransaction = mocks.paymentTransaction;

            // Check the setters
            expect(paymentTransaction.setTransactionID).to.have.been.calledOnce;
            expect(paymentTransaction.setTransactionID).to.have.been.calledWith(serviceCallResult.merchantTransactionId);
            expect(paymentTransaction.setAmount).to.not.have.been.called; // The amount is set when the payment Instrument is created and should not be manipulated afterwards

            // check the rest of the payment transaction
            expect(paymentTransaction.custom.c2pPaymentRequestID).to.equal(serviceCallResult.id);
            expect(paymentTransaction.custom.c2pPaymentRequestPaymentLink).to.equal(serviceCallResult.paymentRequestLink);
            expect(paymentTransaction.custom.c2pPaymentRequestQrCodeLink).to.equal(serviceCallResult.qrCodeLink);
            expect(paymentTransaction.custom.c2pPaymentRequestShortCode).to.equal(serviceCallResult.shortCode);

            // check the orderNote
            expect(order.trackOrderChange).to.have.been.calledOnce;
            var orderChangeArguments = order.trackOrderChange.lastCall.args;
            expect(orderChangeArguments[0]).to.not.be.empty;
        });
    });
});
