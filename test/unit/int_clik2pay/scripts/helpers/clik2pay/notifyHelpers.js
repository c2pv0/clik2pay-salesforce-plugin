'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var chai = require('chai');
chai.use(require('sinon-chai'));
var expect = chai.expect;

var sandbox = sinon.createSandbox();

var mocks = {
    Logger: {
        getLogger: function () { return this; },
        debug: sandbox.spy(),
        error: sandbox.spy()
    },
    Order: {
        ORDER_STATUS_CREATED: 0
    },
    OrderMgr: {
        getOrder: sandbox.stub()
    },
    Site: {
        current: {
            ID: 'siteID'
        }
    },
    StringUtils: {
        decodeBase64: function (value) {
            return Buffer.from(value, 'base64').toString('utf8');
        }
    },
    System: {
        PRODUCTION_SYSTEM: 'PRODUCTION_SYSTEM',
        instanceType: null
    },
    Transaction: {
        wrap: function (callback) {
            return callback();
        }
    },
    constants: require('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/constants'),
    credentialHelpers: {
        getNotifyEndpointCredentialString: function () {}
    },
    paymentInstrumentHelpers: {
        findClik2payPaymentInstrument: function () {}
    }
};

var notifyHelpers = proxyquire('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/notifyHelpers.js', {
    'dw/system/Logger': mocks.Logger,
    'dw/order/Order': mocks.Order,
    'dw/order/OrderMgr': mocks.OrderMgr,
    'dw/system/Site': mocks.Site,
    'dw/util/StringUtils': mocks.StringUtils,
    'dw/system/System': mocks.System,
    'dw/system/Transaction': mocks.Transaction,
    '*/cartridge/scripts/helpers/clik2pay/constants': mocks.constants,
    '*/cartridge/scripts/helpers/clik2pay/credentialHelpers': mocks.credentialHelpers,
    '*/cartridge/scripts/helpers/clik2pay/paymentInstrumentHelpers': mocks.paymentInstrumentHelpers
});

describe('notifyHelpers', function () {
    beforeEach(function () {
        sandbox.resetHistory();
        mocks.Logger.debug.resetHistory();
        mocks.Logger.error.resetHistory();
    });

    afterEach(function () {
        mocks.System.instanceType = null;
    });

    describe('authenticateRequest', function () {
        var getNotifyEndpointCredentialStringMock;
        beforeEach(function () {
            getNotifyEndpointCredentialStringMock = sandbox.stub(mocks.credentialHelpers, 'getNotifyEndpointCredentialString');
        });

        afterEach(function () {
            getNotifyEndpointCredentialStringMock.restore();
        });

        it('returns an error if no valid credentials were not found', function () {
            getNotifyEndpointCredentialStringMock.returns(null);
            var authorizationHeader;

            var result = notifyHelpers.authenticateRequest(authorizationHeader);

            expect(result.errorMessage).to.not.be.undefined;
            expect(result.isSuccess).to.be.false;
            expect(result.errorStatusCode).to.equal(401);
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns an error if the authorization header is empty', function () {
            getNotifyEndpointCredentialStringMock.returns('username:password');
            var authorizationHeader = null;

            var result = notifyHelpers.authenticateRequest(authorizationHeader);

            expect(result.errorMessage).to.not.be.undefined;
            expect(result.isSuccess).to.be.false;
            expect(result.errorStatusCode).to.equal(401);
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns an error if the authorization header is invalid', function () {
            getNotifyEndpointCredentialStringMock.returns('username:password');
            var authorizationHeader = 'invalid authorization header';

            var result = notifyHelpers.authenticateRequest(authorizationHeader);

            expect(result.errorMessage).to.not.be.undefined;
            expect(result.isSuccess).to.be.false;
            expect(result.errorStatusCode).to.equal(401);
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns success if the submitted credentials are correct', function () {
            var storedCredentials = 'username:password';
            getNotifyEndpointCredentialStringMock.returns(storedCredentials);
            var submittedCredentials = Buffer.from(storedCredentials).toString('base64');
            var authorizationHeader = 'Basic ' + submittedCredentials;

            var result = notifyHelpers.authenticateRequest(authorizationHeader);

            expect(result.errorMessage, result.errorMessage).to.be.undefined;
            expect(result.isSuccess).to.be.true;
            expect(result.errorStatusCode).to.be.undefined;
            expect(mocks.Logger.error).to.not.have.been.called;
        });
    });

    describe('parseNotificationRequest', function () {
        it('returns an error if the request body is empty', function () {
            var requestBody = 'null';

            var result = notifyHelpers.parseNotificationRequest(requestBody);

            expect(result.errorMessage).to.not.be.undefined;
            expect(result.isSuccess).to.be.false;
            expect(result.errorStatusCode).to.equal(400);
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns an error if an exception occurs', function () {
            var requestBody = '{ invalid json: ]';

            var result = notifyHelpers.parseNotificationRequest(requestBody);

            expect(result.errorMessage).to.not.be.undefined;
            expect(result.isSuccess).to.be.false;
            expect(result.errorStatusCode).to.equal(400);
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns an error if the order number is empty', function () {
            var requestData = {
                resource: {
                    invoiceNumber: null,
                    businessUnit: 'orderToken'
                }
            };
            var requestBody = JSON.stringify(requestData);

            var result = notifyHelpers.parseNotificationRequest(requestBody);

            expect(result.errorMessage).to.not.be.undefined;
            expect(result.isSuccess).to.be.false;
            expect(result.errorStatusCode).to.equal(400);
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns the parsed order number and token', function () {
            var requestData = {
                resource: {
                    invoiceNumber: 'orderNo',
                    businessUnit: 'orderToken'
                }
            };
            var requestBody = JSON.stringify(requestData);

            var result = notifyHelpers.parseNotificationRequest(requestBody);

            expect(result.errorMessage, result.errorMessage).to.be.undefined;
            expect(result.isSuccess).to.be.true;
            expect(result.errorStatusCode).to.be.undefined;
            expect(mocks.Logger.error).to.not.have.been.called;
        });
    });

    describe('updatePaymentTransactionStatus', function () {
        var order;
        var paymentTransaction;

        beforeEach(function () {
            order = {
                custom: { c2pPaymentSuccessAfterTimeout: null },
                status: { value: mocks.Order.ORDER_STATUS_CREATED },
                trackOrderChange: sandbox.spy()
            };
            paymentTransaction = {
                custom: { c2pPaymentRequestStatus: null }
            };
        });

        it('returns an error if a notification is received and the order status is not CREATED anymore', function () {
            var newTransactionStatus = mocks.constants.PaymentRequestStatus.PAID;
            order.status.value = -1;

            var result = notifyHelpers.updatePaymentTransactionStatus(order, paymentTransaction, newTransactionStatus);

            expect(result.errorMessage).to.not.be.undefined;
            expect(result.isSuccess).to.be.false;
            expect(mocks.Logger.error).to.have.been.calledOnce;
            expect(order.custom.c2pPaymentSuccessAfterTimeout).to.be.true;
            expect(order.trackOrderChange).to.have.been.calledOnce;
        });


        it('returns success if the order status is CREATED', function () {
            var newTransactionStatus = mocks.constants.PaymentRequestStatus.FAILED;

            var result = notifyHelpers.updatePaymentTransactionStatus(order, paymentTransaction, newTransactionStatus);

            expect(result.errorMessage, result.errorMessage).to.be.undefined;
            expect(result.isSuccess).to.be.true;
            expect(mocks.Logger.error).to.not.have.been.called;
            expect(order.custom.c2pPaymentSuccessAfterTimeout).to.be.not.true;
            expect(order.trackOrderChange).to.have.been.calledOnce;
        });
    });

    describe('updateOrder', function () {
        var order;
        var notification;

        beforeEach(function () {
            sandbox.stub(mocks.paymentInstrumentHelpers, 'findClik2payPaymentInstrument');
            sandbox.stub(notifyHelpers, 'updatePaymentTransactionStatus');
            order = {
                orderNo: 'orderNo'
            };
            notification = {
                resource: {
                    status: null
                }
            };
        });

        afterEach(function () {
            mocks.paymentInstrumentHelpers.findClik2payPaymentInstrument.restore();
            notifyHelpers.updatePaymentTransactionStatus.restore();
        });

        function mockPaymentInstrument(paymentRequestStatus) {
            var paymentInstrument = {
                paymentTransaction: {
                    custom: {
                        c2pPaymentRequestStatus: paymentRequestStatus
                    }
                }
            };
            mocks.paymentInstrumentHelpers.findClik2payPaymentInstrument.returns(paymentInstrument);
        }

        // test a notification with the given values for payment request status
        function testNotification(currentStatus, newStatus) {
            mockPaymentInstrument(currentStatus);
            notification.resource.status = newStatus;

            var result = notifyHelpers.updateOrder(order, notification);
            return result;
        }

        it('returns an error if no Clik2pay payment instrument is found', function () {
            mocks.paymentInstrumentHelpers.findClik2payPaymentInstrument.returns(null);
            notification = undefined;

            var result = notifyHelpers.updateOrder(order, notification);

            expect(result.errorMessage).to.not.be.undefined;
            expect(result.isSuccess).to.be.false;
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns an error if no Clik2pay payment transaction is found', function () {
            mocks.paymentInstrumentHelpers.findClik2payPaymentInstrument.returns({ paymentTransaction: null });
            notification = undefined;

            var result = notifyHelpers.updateOrder(order, notification);

            expect(result.errorMessage).to.not.be.undefined;
            expect(result.isSuccess).to.be.false;
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns success and ignores the notification if payment transaction status is PAID', function () {
            var currentStatus = mocks.constants.PaymentRequestStatus.PAID;

            var result = testNotification(currentStatus, mocks.constants.PaymentRequestStatus.SETTLED);

            expect(result.errorMessage, result.errorMessage).to.be.undefined;
            expect(result.isSuccess).to.be.true;
            expect(result.paymentTransactionStatus).to.equal(currentStatus);
            expect(mocks.Logger.debug).to.have.been.calledOnce;
            expect(mocks.Logger.error).to.not.have.been.called;
        });

        it('returns success and ignores the notification if payment transaction status is FAILED', function () {
            var currentStatus = mocks.constants.PaymentRequestStatus.FAILED;

            var result = testNotification(currentStatus, mocks.constants.PaymentRequestStatus.EMAIL_SENT);

            expect(result.errorMessage, result.errorMessage).to.be.undefined;
            expect(result.isSuccess).to.be.true;
            expect(result.paymentTransactionStatus).to.equal(currentStatus);
            expect(mocks.Logger.debug).to.have.been.calledOnce;
            expect(mocks.Logger.error).to.not.have.been.called;
        });

        it('returns success if updating the payment transaction status is successful', function () {
            notifyHelpers.updatePaymentTransactionStatus.returns({ isSuccess: true });
            var newStatus = mocks.constants.PaymentRequestStatus.EMAIL_SENT;

            var result = testNotification(mocks.constants.PaymentRequestStatus.CREATED, newStatus);

            expect(result.errorMessage, result.errorMessage).to.be.undefined;
            expect(result.isSuccess).to.be.true;
            expect(result.paymentTransactionStatus).to.equal(newStatus);
            expect(mocks.Logger.error).to.not.have.been.called;
        });

        it('returns an error if updating the payment transaction status fails', function () {
            notifyHelpers.updatePaymentTransactionStatus.returns({ isSuccess: false, errorMessage: 'errorMessage' });

            var result = testNotification(null, mocks.constants.PaymentRequestStatus.EMAIL_SENT);

            expect(result.errorMessage).to.not.be.undefined;
            expect(result.isSuccess).to.be.false;
        });
    });

    describe('handleNotificationRequest', function () {
        beforeEach(function () {
            sandbox.stub(notifyHelpers, 'authenticateRequest');
            sandbox.stub(notifyHelpers, 'parseNotificationRequest');
            sandbox.stub(notifyHelpers, 'updateOrder');
        });

        afterEach(function () {
            notifyHelpers.authenticateRequest.restore();
            notifyHelpers.parseNotificationRequest.restore();
            notifyHelpers.updateOrder.restore();
        });

        it('returns an error if request authentication fails', function () {
            mocks.System.instanceType = mocks.System.PRODUCTION_SYSTEM;
            notifyHelpers.authenticateRequest.returns({ isSuccess: false, errorMessage: 'errorMessage' });

            var result = notifyHelpers.handleNotificationRequest();

            expect(result.errorMessage).to.be.null; // hide authentication error messages on production
            expect(result.isSuccess).to.be.false;
        });

        it('returns an error if request parsing fails', function () {
            mocks.System.instanceType = mocks.System.PRODUCTION_SYSTEM;
            notifyHelpers.authenticateRequest.returns({ isSuccess: true });
            notifyHelpers.parseNotificationRequest.returns({ isSuccess: false, errorMessage: 'errorMessage' });

            var result = notifyHelpers.handleNotificationRequest();

            expect(result.errorMessage).to.not.be.empty;
            expect(result.isSuccess).to.be.false;
        });

        it('returns an error if the order cannot be found', function () {
            mocks.System.instanceType = mocks.System.PRODUCTION_SYSTEM;
            notifyHelpers.authenticateRequest.returns({ isSuccess: true });
            notifyHelpers.parseNotificationRequest.returns({ isSuccess: true, orderNo: 'orderNo', orderToken: 'orderToken' });
            mocks.OrderMgr.getOrder.returns(null);

            var result = notifyHelpers.handleNotificationRequest();

            expect(result.errorMessage).to.not.be.empty;
            expect(result.isSuccess).to.be.false;
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns an error if updating the order fails', function () {
            mocks.System.instanceType = mocks.System.PRODUCTION_SYSTEM;
            notifyHelpers.authenticateRequest.returns({ isSuccess: true });
            notifyHelpers.parseNotificationRequest.returns({ isSuccess: true, orderNo: 'orderNo', orderToken: 'orderToken' });
            mocks.OrderMgr.getOrder.returns({ orderNo: 'orderNo' });
            notifyHelpers.updateOrder.returns({ isSuccess: false, errorMessage: 'errorMessage' });

            var result = notifyHelpers.handleNotificationRequest();

            expect(result.errorMessage).to.not.be.empty;
            expect(result.isSuccess).to.be.false;
        });

        it('returns success if no error occurred', function () {
            mocks.System.instanceType = mocks.System.PRODUCTION_SYSTEM;
            notifyHelpers.authenticateRequest.returns({ isSuccess: true });
            notifyHelpers.parseNotificationRequest.returns({ isSuccess: true, orderNo: 'orderNo', orderToken: 'orderToken' });
            mocks.OrderMgr.getOrder.returns({ orderNo: 'orderNo' });
            notifyHelpers.updateOrder.returns({ isSuccess: true, paymentTransactionStatus: 'paymentTransactionStatus' });

            var result = notifyHelpers.handleNotificationRequest();

            expect(result.paymentTransactionStatus).to.not.be.empty;
            expect(result.isSuccess).to.be.true;
        });
    });
});
