'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var chai = require('chai');
chai.use(require('sinon-chai'));
var expect = chai.expect;

var sandbox = sinon.createSandbox();

var mocks = {
    LocalServiceRegistry: {
        createService: function () {}
    },
    Logger: {
        getLogger: function () { return this; },
        debug: sandbox.spy(),
        error: sandbox.spy()
    },
    System: {
        DEVELOPMENT_SYSTEM: 'DEVELOPMENT_SYSTEM',
        PRODUCTION_SYSTEM: 'PRODUCTION_SYSTEM',
        instanceType: null
    },
    constants: require('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/constants')
};

var credentialHelpers = proxyquire('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/credentialHelpers.js', {
    'dw/svc/LocalServiceRegistry': mocks.LocalServiceRegistry,
    'dw/system/Logger': mocks.Logger,
    'dw/system/System': mocks.System,
    '*/cartridge/scripts/helpers/clik2pay/constants': mocks.constants
});

describe('credentialHelpers', function () {
    beforeEach(function () {
        sandbox.resetHistory();
        mocks.Logger.debug.resetHistory();
        mocks.Logger.error.resetHistory();
    });

    afterEach(function () {
        mocks.System.instanceType = null;
    });

    describe('getServiceCredential', function () {
        var createServiceMock;

        beforeEach(function () {
            createServiceMock = sandbox.stub(mocks.LocalServiceRegistry, 'createService');
        });

        afterEach(function () {
            createServiceMock.restore();
        });

        var serviceID = 'serviceID';
        var credentialID = 'credentialID';

        it('logs an error if the service was not found', function () {
            createServiceMock.throws();

            var result = credentialHelpers.getServiceCredential(serviceID, credentialID);

            expect(createServiceMock).to.have.been.calledWith(serviceID);
            expect(mocks.Logger.error).to.have.been.called;
            expect(result).to.be.null;
        });

        it('logs an error if the service configuration could not be retrieved', function () {
            createServiceMock.returns({
                getConfiguration: sandbox.stub().throws()
            });

            var result = credentialHelpers.getServiceCredential(serviceID, credentialID);

            expect(createServiceMock).to.have.been.calledWith(serviceID);
            expect(mocks.Logger.error).to.have.been.called;
            expect(result).to.be.null;
        });

        it('logs an error if the service configuration has no credentials assigned', function () {
            createServiceMock.returns({
                getConfiguration: sandbox.stub().returns({
                    getCredential: sandbox.stub().returns(null)
                })
            });

            var result = credentialHelpers.getServiceCredential(serviceID, credentialID);

            expect(createServiceMock).to.have.been.calledWith(serviceID);
            expect(mocks.Logger.error).to.have.been.called;
            expect(result).to.be.null;
        });

        it('returns the correct credentials', function () {
            var expectedCredentials = { ID: 'bla' };
            var setCredentialIDMock = sandbox.spy();
            createServiceMock.returns({
                getConfiguration: sandbox.stub().returns({
                    getCredential: sandbox.stub().returns(expectedCredentials)
                }),
                setCredentialID: setCredentialIDMock
            });

            var actualCredentials = credentialHelpers.getServiceCredential(serviceID, credentialID);

            expect(createServiceMock).to.have.been.calledWith(serviceID);
            expect(setCredentialIDMock).to.have.been.calledWith(credentialID);
            expect(mocks.Logger.error).to.not.have.been.called;
            expect(actualCredentials).to.equal(expectedCredentials);
        });
    });

    describe('getAuthenticationCredentialID', function () {
        it('returns production credential ID on production', function () {
            mocks.System.instanceType = mocks.System.PRODUCTION_SYSTEM;

            var credentialID = credentialHelpers.getAuthenticationCredentialID('siteID');

            expect(credentialID).to.contain('production');
        });

        it('does not return production credential ID on sandbox', function () {
            mocks.System.instanceType = mocks.System.DEVELOPMENT_SYSTEM;

            var credentialID = credentialHelpers.getAuthenticationCredentialID('siteID');

            expect(credentialID).not.to.contain('production');
        });
    });

    describe('getAuthenticationCredentials', function () {
        var getServiceCredentialMock;

        beforeEach(function () {
            getServiceCredentialMock = sandbox.stub(credentialHelpers, 'getServiceCredential');
            getServiceCredentialMock.returns(null);
            sandbox.stub(credentialHelpers, 'getAuthenticationCredentialID').returns('CredentialID');
        });

        afterEach(function () {
            credentialHelpers.getServiceCredential.restore();
            credentialHelpers.getAuthenticationCredentialID.restore();
        });

        it('returns null if user name is empty', function () {
            getServiceCredentialMock.returns({ user: '', password: '1234567890' });

            var credentials = credentialHelpers.getAuthenticationCredentials('siteID');

            expect(credentials).to.be.null;
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns null if password is empty', function () {
            getServiceCredentialMock.returns({ user: '1234567890', password: '' });

            var credentials = credentialHelpers.getAuthenticationCredentials('siteID');

            expect(credentials).to.be.null;
            expect(mocks.Logger.error).to.have.been.calledOnce;
        });

        it('returns valid credentials', function () {
            var expectedCredentials = { user: '1234567890', password: '1234567890' };
            getServiceCredentialMock.returns(expectedCredentials);

            var actualCredentials = credentialHelpers.getAuthenticationCredentials('siteID');

            expect(actualCredentials.user).to.equal(expectedCredentials.user);
            expect(actualCredentials.password).to.equal(expectedCredentials.password);
            expect(mocks.Logger.error).to.not.have.been.called;
        });
    });

    describe('getNotifyEndpointCredentialString', function () {
        var getAuthenticationCredentialsMock;

        beforeEach(function () {
            getAuthenticationCredentialsMock = sandbox.stub(credentialHelpers, 'getAuthenticationCredentials');
        });

        afterEach(function () {
            getAuthenticationCredentialsMock.restore();
        });

        it('returns null if no valid credentials were found', function () {
            getAuthenticationCredentialsMock.returns(null);

            var credentialString = credentialHelpers.getNotifyEndpointCredentialString('siteID');

            expect(credentialString).to.be.null;
        });

        it('returns concatenated user name and password', function () {
            var credentials = { user: '1234567890', password: '1234567890' };
            getAuthenticationCredentialsMock.returns(credentials);

            var credentialString = credentialHelpers.getNotifyEndpointCredentialString('siteID');

            expect(credentialString).to.equal(credentials.user + ':' + credentials.password);
        });
    });
});
