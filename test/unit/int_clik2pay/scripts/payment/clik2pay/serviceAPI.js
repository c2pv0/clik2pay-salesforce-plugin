'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var chai = require('chai');
chai.use(require('sinon-chai'));
var expect = chai.expect;

var sandbox = sinon.createSandbox();

var mocks = {
    Logger: {
        getLogger: function () { return this; },
        debug: sandbox.spy(),
        error: sandbox.spy()
    },
    Site: {
        current: {
            getCustomPreferenceValue: sandbox.stub()
        }
    },
    constants: require('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/constants'),
    tokenHelpers: {
        retrieveToken: sandbox.stub(),
        storeToken: sandbox.spy()
    },
    credentialHelpers: {
        getAuthenticationCredentials: sandbox.stub()
    },
    problemReporter: {
        addErrorMessage: sandbox.spy(),
        addWarningMessage: sandbox.spy(),
        getStatusReport: sandbox.stub(),
        logStatus: sandbox.spy()
    },
    validationHelper: {
        validateCreatePaymentRequestParameters: sandbox.stub(),
        validateCreatePaymentRequestResult: sandbox.stub()
    },
    serviceConfig: {
        createService: sandbox.stub()
    },
    customer: {
        email: 'not-a-real-Email@example.com',
        fullName: 'Francis Drake',
        testName: 'depositSubmit',
        localeID: 'fr_CA',
        no: '0074247',
        phone: {
            billing: '012345678900',
            shipping: '009876543210'
        }
    },
    service: {
        configuration: {
            credential: {
                user: 'a totally trustworthy user name',
                password: 'a completely safe password'
            }
        },
        call: sandbox.stub(),
        setCredentialID: sandbox.stub()
    }
};

/**
 * Mocks the ProblemReporter to keep this a unit test for validationHelper
 */
function ProblemReporter() {
    this.addErrorMessage = mocks.problemReporter.addErrorMessage;
    this.addWarningMessage = mocks.problemReporter.addWarningMessage;
    this.logStatus = mocks.problemReporter.logStatus;
    this.getStatusReport = mocks.problemReporter.getStatusReport;
}

var serviceAPI = proxyquire('../../../../../../cartridges/int_clik2pay/cartridge/scripts/payment/clik2pay/serviceAPI.js', {
    'dw/system/Logger': mocks.Logger,
    'dw/system/Site': mocks.Site,
    '*/cartridge/scripts/helpers/clik2pay/constants': mocks.constants,
    '*/cartridge/scripts/helpers/clik2pay/tokenHelpers': mocks.tokenHelpers,
    '*/cartridge/scripts/helpers/clik2pay/credentialHelpers': mocks.credentialHelpers,
    '*/cartridge/scripts/payment/clik2pay/serviceConfig': mocks.serviceConfig,
    '*/cartridge/scripts/helpers/clik2pay/validationHelpers': mocks.validationHelper,
    '*/cartridge/scripts/helpers/clik2pay/problemReporter': ProblemReporter
});

describe('serviceAPI', function () {
    beforeEach(function () {
        sandbox.resetHistory();
        sandbox.resetBehavior();
    });

    describe('authenticate', function () {
        beforeEach(function () {
            mocks.credentialHelpers.getAuthenticationCredentials.returns(null);
            mocks.serviceConfig.createService.returns(mocks.service);
            mocks.tokenHelpers.retrieveToken.returns(null);
        });

        it('handles problems with service creation', function () {
            mocks.serviceConfig.createService.returns(null);
            var expectedResult = {
                success: false
            };

            var result = serviceAPI.authenticate();
            expect(result).to.deep.equal(expectedResult);
            expect(mocks.Logger.error).to.have.been.calledOnce;
            expect(mocks.service.call).to.not.have.been.called;
        });

        it('returns a cached token if there is any', function () {
            var expectedToken = 'some token';
            mocks.tokenHelpers.retrieveToken.returns(expectedToken);

            var expectedResult = {
                success: true,
                token: expectedToken
            };

            var result = serviceAPI.authenticate();
            expect(result).to.deep.equal(expectedResult);
            expect(mocks.service.call).to.not.have.been.called;
        });

        it('calls the clik2pay service for a new token if there is no cached token', function () {
            var expectedToken = '';
            var serviceCallResult = {
                success: true,
                response: {
                    access_token: expectedToken,
                    expires_in: 3847
                }
            };
            mocks.credentialHelpers.getAuthenticationCredentials.returns(mocks.service.configuration.credential);
            mocks.service.call.returns({ ok: true, object: JSON.stringify(serviceCallResult) });

            var expectedResult = {
                success: true,
                token: expectedToken
            };

            var result = serviceAPI.authenticate();
            expect(mocks.service.call).to.have.been.calledOnce;
            expect(mocks.tokenHelpers.storeToken).to.have.been.calledOnce;
            expect(mocks.tokenHelpers.storeToken.lastCall.args).to.include(expectedToken);
            expect(result).to.deep.equal(expectedResult);
        });

        it('can handle problems while calling the clik2pay service for a new token', function () {
            var serviceCallResult = {
                success: false,
                response: {}
            };
            mocks.credentialHelpers.getAuthenticationCredentials.returns(mocks.service.configuration.credential);
            mocks.service.call.returns({ ok: false, object: JSON.stringify(serviceCallResult) });

            var expectedResult = {
                success: false
            };

            var result = serviceAPI.authenticate();
            expect(mocks.service.call).to.have.been.calledOnce;
            expect(result).to.deep.equal(expectedResult);
        });
    });

    describe('createPaymentRequest', function () {
        // "local globals" for this specific test suite
        var authenticationStub;
        var authenticationToken;
        var defaultOrder;

        var expectedDeliveryMode;
        var expectedDisplayMessage;
        var expectedEmailTemplate;
        var expectedMerchantID;
        var expectedOrderToken;
        var expectedStatusReport;
        var expectedTransactionType;

        beforeEach(function () {
            expectedStatusReport = 'exactly what we expected. Incredible!';
            mocks.problemReporter.getStatusReport.returns(expectedStatusReport);
            mocks.serviceConfig.createService.returns(mocks.service);

            authenticationToken = 'h6fgibhnöb7n3epow6a0tb7guböjbjlvdbjfkgldös4975zbkynsdvl7bnq0p9fjhvf9waepzv59ptruhjöRÖÄJGAÖ';
            authenticationStub = sandbox.stub(serviceAPI, 'authenticate');
            authenticationStub.returns({
                success: true,
                token: authenticationToken
            });

            // our expectations for the custom site preferences
            expectedDeliveryMode = 'NONE';
            expectedDisplayMessage = 'usually... none';
            expectedEmailTemplate = 'no template';
            expectedMerchantID = 'exactly the ID that we expected';
            expectedOrderToken = 'asdfghjklöäTokenertzuiopü';
            expectedTransactionType = 'ECOMM';

            // setup custom site preferences
            var getCustomPreferenceValueMock = mocks.Site.current.getCustomPreferenceValue;
            getCustomPreferenceValueMock.withArgs('c2pCustomerNotificationDeliveryMode').returns({ value: expectedDeliveryMode });
            getCustomPreferenceValueMock.withArgs('c2pCustomerNotificationEmailTemplateID').returns(expectedEmailTemplate);
            getCustomPreferenceValueMock.withArgs('c2pPaymentRequestDisplayMessage').returns(expectedDisplayMessage);
            getCustomPreferenceValueMock.withArgs('c2pMerchantID').returns(expectedMerchantID);

            // Construct a default order for the tests, which might be changed during tests, so rebuild them each time to be sure
            defaultOrder = {
                billingAddress: {
                    firstName: '',
                    fullName: mocks.customer.fullName,
                    phone: mocks.customer.phone.billing
                },
                defaultShipment: {
                    shippingAddress: {
                        phone: mocks.customer.phone.shipping
                    }
                },
                customerEmail: mocks.customer.email,
                customerNo: mocks.customer.no,
                customerLocaleID: mocks.customer.localeID,
                totalGrossPrice: {
                    value: '123.45'
                },
                orderToken: expectedOrderToken,
                orderNo: 'dev456789'
            };
        });

        afterEach(function () {
            authenticationStub.restore();
        });

        it('reports missing order address data', function () {
            var noOrder = null;
            var orderWithoutAddresses = {
                defaultShipment: {}
            };
            var orderWithoutShippingAddress = {
                billingAddress: {
                    fullName: 'totally normal name'
                },
                defaultShipment: {}
            };
            var orderWithoutAName = {
                billingAddress: {},
                defaultShipment: {
                    shippingAddress: {}
                }
            };

            function checkResultAndResetProblemReporter(order) {
                var result = serviceAPI.createPaymentRequest(order);
                expect(result.success).to.be.false;
                expect(result.problemsToReport).to.equal(expectedStatusReport); // Note: if this field wouldn't be updated it would have the value 'uninitialized'
                expect(mocks.problemReporter.addErrorMessage).to.have.been.calledOnce;
                // after we checked everything to meet our expectations, let's make the sandbox forget everything and check the other cases
                sandbox.resetHistory();
                mocks.problemReporter.addErrorMessage.resetHistory();
            }

            checkResultAndResetProblemReporter(noOrder);
            checkResultAndResetProblemReporter(orderWithoutAddresses);
            checkResultAndResetProblemReporter(orderWithoutShippingAddress);
            checkResultAndResetProblemReporter(orderWithoutAName);
        });

        it('reports inability to acquire a token', function () {
            // overwrite authentication stub to simulate inability to get a token
            authenticationStub.returns({
                success: false
            });

            var result = serviceAPI.createPaymentRequest(defaultOrder);
            expect(mocks.problemReporter.addErrorMessage).to.have.been.called;
            expect(result.success).to.be.false;
            expect(result.problemsToReport).to.equal(expectedStatusReport);
        });

        it('reports call parameters validation problems', function () {
            var expectedValidationReport = 'Oh my where shall I start?';
            mocks.validationHelper.validateCreatePaymentRequestParameters.returns({
                hasErrors: sandbox.stub().returns(true),
                getStatusReport: sandbox.stub().returns(expectedValidationReport)
            });

            var expectedPayer = 'perfectly good payer';
            var payerObjectBuilderStub = sandbox.stub(serviceAPI, 'buildPayerObject');
            payerObjectBuilderStub.returns(expectedPayer);

            // Regarding businessUnit: we wanted to transmit the orderNo and orderToken as transactionID but due to length constraints...
            // ... it was decided to use the business Unit for the Token instead
            var expectedParameters = {
                body: {
                    amount: defaultOrder.totalGrossPrice.value,
                    businessUnit: defaultOrder.orderToken,
                    deliveryMode: expectedDeliveryMode,
                    displayMessage: expectedDisplayMessage,
                    emailTemplate: expectedEmailTemplate,
                    invoiceNumber: defaultOrder.orderNo,
                    merchant: {
                        id: expectedMerchantID
                    },
                    merchantTransactionId: defaultOrder.orderNo,
                    payer: expectedPayer,
                    type: expectedTransactionType
                },
                apiKey: mocks.service.configuration.credential.password,
                token: authenticationToken
            };

            var result = serviceAPI.createPaymentRequest(defaultOrder);

            expect(result.success).to.be.false;
            expect(result.problemsToReport).to.equal(expectedValidationReport);
            expect(result.parameters).to.deep.equal(expectedParameters);

            // cleaning up the stubbing to not interfere with other tests
            payerObjectBuilderStub.restore();
        });

        it('reports problems with the service call', function () {
            var expectedErrorMessage = 'if this message is what you expected... is it then still an error?';
            mocks.service.call.returns({
                object: JSON.stringify({ success: false }),
                msg: expectedErrorMessage
            });

            var expectedValidationReport = 'Nothing to report... really. Everything is fine';
            mocks.validationHelper.validateCreatePaymentRequestParameters.returns({
                hasErrors: sandbox.stub().returns(false),
                getStatusReport: sandbox.stub().returns(expectedValidationReport)
            });

            var result = serviceAPI.createPaymentRequest(defaultOrder);
            expect(result.success).to.be.false;
            expect(result.problemsToReport).to.equal(expectedErrorMessage);
            expect(result.result).to.not.be.empty;
        });

        it('reports problems with the service call result validation', function () {
            var expectedErrorMessage = 'No message is expected here';
            mocks.service.call.returns({
                ok: true,
                object: JSON.stringify({ success: true }),
                msg: expectedErrorMessage
            });

            var expectedProblemReportFromValidationHelper = 'Validation Error! Validation Error! All Hands On Deck! We have validation Errors!';
            mocks.validationHelper.validateCreatePaymentRequestResult.returns(expectedProblemReportFromValidationHelper);

            var expectedValidationReport = 'Nothing to report... really. Everything is fine';
            mocks.validationHelper.validateCreatePaymentRequestParameters.returns({
                hasErrors: sandbox.stub().returns(false),
                getStatusReport: sandbox.stub().returns(expectedValidationReport)
            });

            var result = serviceAPI.createPaymentRequest(defaultOrder);

            expect(result.success).to.be.false;
            expect(result.problemsToReport).to.equal(expectedProblemReportFromValidationHelper);
            expect(result.result).to.not.be.empty;
        });

        it('returns a positive result if all went well', function () {
            var expectedPaymentLink = 'https://expected.payment.link';
            var expectedQrCodeLink = '"https://pay.clik2pay.com/qr/ABCDEFG';
            var expectedShortCode = 'ABCDEFG';
            var expectedStatus = 'CREATED';
            var expectedResourceLink = 'https://api.clik2pay.com/open/v1/payment-requests/e6a16402-fade-4fb2-8bfc-27194d3e3f87';
            var expectedAmount = 50.25;
            var expectedErrorMessage = 'if this message is what you expected... is it then still an error?';
            var expectedID = 'expected id';
            var expectedTransactionID = 'expected Transaction ID';

            mocks.service.call.returns({
                object: JSON.stringify({
                    success: true,
                    response: {
                        id: expectedID,
                        merchantTransactionId: expectedTransactionID,
                        paymentLink: expectedPaymentLink,
                        qrCodeLink: expectedQrCodeLink,
                        shortCode: expectedShortCode,
                        status: expectedStatus,
                        amount: expectedAmount,
                        links: [
                            {
                                rel: 'payment-request',
                                href: expectedResourceLink
                            }
                        ]
                    }
                }),
                msg: expectedErrorMessage
            });

            var expectedValidationReport = 'Nothing to report... really. Everything is fine';
            mocks.validationHelper.validateCreatePaymentRequestParameters.returns({
                hasErrors: sandbox.stub().returns(false),
                getStatusReport: sandbox.stub().returns(expectedValidationReport)
            });
            // in case there are no problems we expect this function to return an empty string
            mocks.validationHelper.validateCreatePaymentRequestResult.returns('');

            var expectedResult = {
                success: true,
                clik2payResourceLink: expectedResourceLink,
                confirmedAmount: expectedAmount,
                id: expectedID,
                merchantTransactionId: expectedTransactionID,
                paymentRequestLink: expectedPaymentLink,
                qrCodeLink: expectedQrCodeLink,
                shortCode: expectedShortCode,
                status: expectedStatus
            };

            var result = serviceAPI.createPaymentRequest(defaultOrder);
            expect(result).to.deep.equal(expectedResult);
            expect(result.problemsToReport).to.not.exist; // technically unnecessary check, only here to underline that we really don't expect this field
        });
    });

    describe('buildPayerObject', function () {
        /* The function buildPayerObject is only called from createPaymentRequest, which already validates the existence of
         * the necessary data. Error reporting is also outsourced to that function. So we can skip the validation testing
         * and concentrate on special case handling
         */
        var order;
        beforeEach(function () {
            order = {
                billingAddress: {
                    firstName: '',
                    fullName: mocks.customer.fullName,
                    phone: mocks.customer.phone.billing
                },
                defaultShipment: {
                    shippingAddress: {
                        phone: mocks.customer.phone.shipping
                    }
                },
                customerEmail: mocks.customer.email,
                customerNo: mocks.customer.no,
                customerLocaleID: mocks.customer.localeID
            };
        });

        it('builds a payer object from the order data if all is there', function () {
            var expectedPayerObject = {
                name: mocks.customer.fullName,
                email: mocks.customer.email,
                merchantAccountId: mocks.customer.no,
                mobileNumber: mocks.customer.phone.billing,
                preferredLanguage: 'fr'
            };

            var payerObject = serviceAPI.buildPayerObject(order);
            expect(payerObject).to.deep.equal(expectedPayerObject);
        });

        it('does not include the mobileNumber field if no phone number is available', function () {
            order.billingAddress.phone = null;
            order.defaultShipment.shippingAddress.phone = null;
            var expectedPayerObject = {
                name: mocks.customer.fullName,
                email: mocks.customer.email,
                merchantAccountId: mocks.customer.no,
                preferredLanguage: 'fr'
            };

            var payerObject = serviceAPI.buildPayerObject(order);
            expect(payerObject).to.deep.equal(expectedPayerObject);
            expect(payerObject).to.not.have.ownProperty('mobileNumber');
        });

        it('identifies clik2pay test and correctly ignores the last name', function () {
            order.billingAddress.firstName = mocks.customer.testName;

            var expectedPayerObject = {
                name: mocks.customer.testName,
                email: mocks.customer.email,
                merchantAccountId: mocks.customer.no,
                mobileNumber: mocks.customer.phone.billing,
                preferredLanguage: 'fr'
            };

            var payerObject = serviceAPI.buildPayerObject(order);
            expect(payerObject).to.deep.equal(expectedPayerObject);
        });

        it('correctly identifies the preferred language', function () {
            order.customerLocaleID = 'en_CA';

            var expectedPayerObject = {
                name: mocks.customer.fullName,
                email: mocks.customer.email,
                merchantAccountId: mocks.customer.no,
                mobileNumber: mocks.customer.phone.billing,
                preferredLanguage: 'en'
            };

            var payerObject = serviceAPI.buildPayerObject(order);
            expect(payerObject).to.deep.equal(expectedPayerObject);
        });
    });

    describe('getCustomerErrorMessage', function () {
        it('returns null if the response cannot be parsed', function () {
            expect(serviceAPI.getCustomerErrorMessage(null)).to.be.null;
            expect(serviceAPI.getCustomerErrorMessage('')).to.be.null;
            expect(serviceAPI.getCustomerErrorMessage('false')).to.be.null;
            expect(serviceAPI.getCustomerErrorMessage('{ not "json"]')).to.be.null;
        });

        it('returns null if the response contains no errors', function () {
            expect(serviceAPI.getCustomerErrorMessage('{}')).to.be.null;
            expect(serviceAPI.getCustomerErrorMessage('{ "errors": [] }')).to.be.null;
        });

        it('returns the error message', function () {
            var expectedErrorMessage = 'A valid phone number is required, with 10 digits and no spaces or other characters';
            var response = {
                errors: [
                    {
                        property: 'payer.mobileNumber',
                        error: expectedErrorMessage
                    }
                ]
            };
            var responseText = JSON.stringify(response);

            var errorMessage = serviceAPI.getCustomerErrorMessage(responseText);

            expect(errorMessage).to.equal(expectedErrorMessage);
        });
    });
});
