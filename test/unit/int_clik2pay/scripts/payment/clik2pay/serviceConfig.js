'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var chai = require('chai');
chai.use(require('sinon-chai'));
var expect = chai.expect;

var sandbox = sinon.createSandbox();

var mocks = {
    Bytes: sandbox.stub(),
    Encoding: {
        toBase64: sandbox.stub()
    },
    LocalServiceRegistry: {
        createService: sandbox.stub()
    },
    System: {
        PRODUCTION_SYSTEM: 'PRODUCTION_SYSTEM',
        instanceType: null
    },
    base64String: 'hard to believe this is no base64 string',
    httpClient: { },
    service: {
        addHeader: sandbox.spy()
    },
    constants: require('../../../../../../cartridges/int_clik2pay/cartridge/scripts/helpers/clik2pay/constants')
};

var serviceConfig = proxyquire('../../../../../../cartridges/int_clik2pay/cartridge/scripts/payment/clik2pay/serviceConfig.js', {
    'dw/util/Bytes': mocks.Bytes,
    'dw/crypto/Encoding': mocks.Encoding,
    'dw/svc/LocalServiceRegistry': mocks.LocalServiceRegistry,
    'dw/system/System': mocks.System,
    '*/cartridge/scripts/helpers/clik2pay/constants': mocks.constants
});


describe('serviceConfig', function () {
    beforeEach(function () {
        sandbox.resetBehavior();
        sandbox.resetHistory();

        // setup default behavior
        mocks.LocalServiceRegistry.createService.returnsArg(0);
        mocks.Encoding.toBase64.returns(mocks.base64String);

        mocks.httpClient = {
            statusCode: '200',
            text: '',
            errorText: ''
        };
    });

    describe('createPaymentRequestRequest', function () {
        var service;

        beforeEach(function () {
            service = {
                setAuthentication: function () {},
                addHeader: function () {},
                getURL: function () {},
                setURL: function () {}
            };
        });

        it('creates the request body', function () {
            var parameters = {
                body: 'dummy-body'
            };

            var requestBodyJson = serviceConfig.createPaymentRequestRequest(service, parameters);

            expect(requestBodyJson).to.contain(parameters.body);
        });
    });

    describe('createService', function () {
        it('creates a service for get_auth_Token', function () {
            var getAuthService = serviceConfig.createService('get_auth_token');

            expect(getAuthService).to.equal('int_clik2pay.httpForm.clik2pay.get_auth_token');
            expect(mocks.LocalServiceRegistry.createService).to.have.been.called;
            expect(mocks.LocalServiceRegistry.createService.args[0]).to.have.a.lengthOf(2);

            var getAuthCallbacks = mocks.LocalServiceRegistry.createService.args[0][1];
            expect(getAuthCallbacks).to.have.property('createRequest');
            expect(getAuthCallbacks).to.have.property('parseResponse');
        });

        it('creates a service for create_payment_request', function () {
            var createPaymentRequestService = serviceConfig.createService('create_payment_request');

            expect(createPaymentRequestService).to.equal('int_clik2pay.http.clik2pay.create_payment_request');
            expect(mocks.LocalServiceRegistry.createService).to.have.been.called;
            expect(mocks.LocalServiceRegistry.createService.args[0]).to.have.a.lengthOf(2);

            var createPaymentRequestCallbacks = mocks.LocalServiceRegistry.createService.args[0][1];
            expect(createPaymentRequestCallbacks).to.have.property('createRequest');
            expect(createPaymentRequestCallbacks).to.have.property('parseResponse');
        });
    });

    describe('createAuthenticationRequest', function () {
        it('sets the necessary header for an authentication request', function () {
            var parameters = {
                username: 'expected username',
                password: 'expected password'
            };

            var authenticationRequest = serviceConfig.createAuthenticationRequest(mocks.service, parameters);

            expect(mocks.service.addHeader).to.have.been.calledTwice;
            expect(mocks.service.addHeader).to.have.been.calledWith('Authorization', 'Basic ' + mocks.base64String);
            expect(mocks.service.addHeader).to.have.been.calledWith('Content-Type', 'application/x-www-form-urlencoded');
            expect(authenticationRequest).to.be.not.empty;
        });
    });

    describe('parseResponse', function () {
        it('parses responses for successful requests', function () {
            var expectedResultObject = {
                greatProperty: 'fantastic!',
                evenBetterProperty: 'you would not believe it if you would not read it here'
            };
            mocks.httpClient.text = JSON.stringify(expectedResultObject);
            // the parsed response will be converted back into a string before it is handled by the SFCC service. So we get it as a string and have to parse it back into an object
            var parsedResponse = JSON.parse(serviceConfig.parseResponse(mocks.service, mocks.httpClient));

            expect(parsedResponse.success).to.be.true;
            expect(parsedResponse.statusCode).to.equal(mocks.httpClient.statusCode);
            expect(parsedResponse.response).to.deep.equal(expectedResultObject);
        });

        it('parses responses for failed requests', function () {
            var expectedError = {
                message: 'Failed to parse resource',
                errors: [
                    {
                        property: 'email',
                        error: 'must be a well-formed email address'
                    }
                ]
            };
            mocks.httpClient.statusCode = '400';
            mocks.httpClient.errorText = JSON.stringify(expectedError);

            // the parsed response will be converted back into a string before it is handled by the SFCC service. So we get it as a string and have to parse it back into an object
            var parsedResponse = JSON.parse(serviceConfig.parseResponse(mocks.service, mocks.httpClient));
            expect(parsedResponse.success).to.be.false;
            expect(parsedResponse.statusCode).to.equal(mocks.httpClient.statusCode);
            expect(parsedResponse.response).to.deep.equal(expectedError);
        });

        it('handles empty responses', function () {
            // this could be any statusCode which is coming in with an empty response
            // it's 500 in this testcase just to make it different from the others
            mocks.httpClient.statusCode = '500';

            // the parsed response will be converted back into a string before it is handled by the SFCC service. So we get it as a string and have to parse it back into an object
            var parsedResponse = JSON.parse(serviceConfig.parseResponse(mocks.service, mocks.httpClient));
            expect(parsedResponse.success).to.be.false;
            expect(parsedResponse.statusCode).to.equal(mocks.httpClient.statusCode);
            expect(parsedResponse.response.fatalErrorMessage).to.not.be.empty;
        });

        it('handles responses which can not be parsed', function () {
            mocks.httpClient.statusCode = '301';
            mocks.httpClient.errorText = '<!DOCTYPE html><html><head><meta http-equiv="Refresh" content="0; url=https://de.w3docs.com" /></head></html>';

            // the parsed response will be converted back into a string before it is handled by the SFCC service. So we get it as a string and have to parse it back into an object
            var parsedResponse = JSON.parse(serviceConfig.parseResponse(mocks.service, mocks.httpClient));
            expect(parsedResponse.success).to.be.false;
            expect(parsedResponse.response.fatalErrorMessage).to.not.be.empty;
            expect(parsedResponse.response.serverResponse).to.equal(mocks.httpClient.errorText);
        });
    });
});
