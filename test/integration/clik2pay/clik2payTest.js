'use strict';

var expect = require('chai').expect;
var request = require('request-promise');
var config = require('../it.config');

var cwd = process.cwd();
var path = require('path');
var dwJson = require(path.join(cwd, 'dw.json'));
var storefrontHostname = dwJson.hostname;
var siteID = dwJson['site-id'] || 'RefArch';

var defaultRequestOptions = {
    method: 'GET',
    auth: config.auth,
    json: true,
    resolveWithFullResponse: true
};

describe('Clik2Pay', function () {
    it('Site ID: ' + siteID + ' (configure this in dw.json)', function () {}); // abusing this because console.log displays before the first describe

    describe('Authentication', function () {
        var requestOptions = Object.assign({}, defaultRequestOptions, { url: config.baseUrl + '/Clik2payTest-TestAuthentication' });

        it('returns a token', function () {
            return request(requestOptions)
                .then(function (response) {
                    var result = response.body;
                    expect(result.success).to.be.true;
                    expect(result.token).to.be.a.string;
                    expect(result.token).to.be.not.empty;
                });
        });

        it('returns the same (cached) token', function () {
            var firstToken = 'uninitialized';
            return request(requestOptions)
                .then(function (firstResponse) {
                    var result = firstResponse.body;
                    firstToken = result.token;
                    expect(result.success).to.be.true;
                    expect(firstToken).to.be.a.string;
                    expect(firstToken).to.be.not.empty;

                    return request(requestOptions);
                })
                .then(function (secondResponse) {
                    var result = secondResponse.body;
                    var secondToken = result.token;

                    // we expect this token to be the same as the first, because of caching
                    expect(result.success).to.be.true;
                    expect(secondToken).to.be.a.string;
                    expect(secondToken).to.be.not.empty;

                    expect(firstToken).to.equal(secondToken);
                });
        });
    });

    describe('createPaymentRequest', function () {
        var requestOptions = Object.assign({}, defaultRequestOptions, { url: config.baseUrl + '/Clik2payTest-TestCreatePaymentRequest' });

        it('creates a paymentRequest link', function () {
            return request(requestOptions)
                .then(function (response) {
                    var result = response.body;

                    expect(result.problemsToReport).to.be.undefined;
                    expect(result.success).to.be.true;
                    expect(result.paymentRequestLink).to.not.be.empty;
                });
        });
    });

    describe('Notify endpoint', function () {
        it('marks a payment transaction as paid', function () {
            // create a new test order
            var requestOptions = Object.assign({}, defaultRequestOptions, { url: config.baseUrl + '/Clik2payTest-CreateTestOrder' });

            return request(requestOptions)
                .then(function (createOrderResponse) {
                    var body = createOrderResponse.body;

                    expect(!!body).to.be.true;
                    expect(body.errorMessage, 'CreateTestOrder endpoint failed: ' + body.errorMessage).to.be.undefined;
                    expect(body.isSuccess).to.be.true;
                    expect(body.orderNo).to.not.be.empty;
                    expect(body.orderToken).to.not.be.empty;
                    expect(body.notifyCredentials).to.not.be.empty;

                    // send a Payment Request notification to the Notify endpoint
                    var requestBody = require('./paymentCompleted.json');
                    requestBody.resource.invoiceNumber = createOrderResponse.body.orderNo;
                    requestBody.resource.businessUnit = createOrderResponse.body.orderToken;
                    var url = 'https://' + storefrontHostname + '/s/' + siteID + '/c2pnotify?skipauth=true';
                    requestOptions = {
                        json: true,
                        resolveWithFullResponse: true,
                        simple: false, // enable custom error handling
                        method: 'POST',
                        url: url,
                        headers: {
                            Authorization: 'Basic ' + Buffer.from(createOrderResponse.body.notifyCredentials).toString('base64')
                        },
                        body: requestBody
                    };

                    return request(requestOptions)
                        .then(function (notifyResponse) {
                            var notifyBody = notifyResponse.body;
                            if (!notifyBody || !notifyBody.isSuccess || notifyResponse.statusCode !== 200) {
                                if (notifyResponse.statusCode !== 200) {
                                    // eslint-disable-next-line no-console
                                    console.log('Notify responded with: ' + notifyResponse.statusCode + ' ' + notifyResponse.statusMessage);
                                }
                                expect(false, (notifyBody && notifyBody.errorMessage) || 'Notify endpoint failed without error message').to.be.true;
                            }
                            expect(notifyBody.paymentTransactionStatus).to.equal('PAID');
                        });
                });
        });
    });
});
