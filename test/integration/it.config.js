'use strict';

var getConfig = require('@tridnguyen/config');
var cwd = process.cwd();
var path = require('path');
var dwJson = require(path.join(cwd, 'dw.json'));

var hostName = dwJson['storefront-hostname'] || dwJson.hostname;
var siteID = dwJson['site-id'] || 'RefArch';

var opts = Object.assign({}, getConfig({
    baseUrl: 'https://' + hostName + '/on/demandware.store/Sites-' + siteID + '-Site/default',
    suite: '*',
    reporter: 'spec',
    timeout: 60000,
    locale: 'x_default',
    auth: {
        user: dwJson['storefront-username'] || dwJson.username,
        pass: dwJson['storefront-password'] || dwJson.password
    }
}, './config.json'));

module.exports = opts;
